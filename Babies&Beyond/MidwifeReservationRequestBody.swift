//
//  MidwifeReservationRequestBody.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/9/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class MidwifeReservationRequestBody: Codable {
    public var midwife_id:Int?
    public var dates:[MidwifeTime]?
    
    
    init(_ midwifeId:Int, _ dates:[MidwifeTime]) {
        self.midwife_id = midwifeId
        self.dates = dates
    }
    
    init() {}
}
