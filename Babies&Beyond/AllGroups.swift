//
//  AllGroups.swift
//  BabiesChat
//
//  Created by M.I.Kamashany on 1/17/18.
//  Copyright © 2018 NtamTech. All rights reserved.
//

import UIKit

class AllGroups: Codable {
    var status:Bool?
    var message:String?
    var data:[Group]?
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
}
