//
//  ForgotPasswordViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/15/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import PKHUD

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var sendBtn:EMSpinnerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendBtn.cornerRadius = 5.0
        sendBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
     }
    override func viewWillAppear(_ animated: Bool) {
        // Hide the navigation bar for current view controller
        self.navigationController?.isNavigationBarHidden = true;
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendBtnTapped(_ sender: UIButton) {
        guard let email = emailTxt.text , !email.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard  Validations.isValidEmail(email: email) else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.InvalidEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        if InternetConnection.connected() {
            sendBtn.animate(animation: AnimationType.collapse)
            UserService.forgetPassword(email: email, completion: { (response, error) in
                self.sendBtn.animate(animation: AnimationType.expand)
                if let _ = error {
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    return
                }
                if let response = response as? CommentRateResponse {
                    if response.status! {
                        Helper.showAlert(title: "Password Reset Email Sent", message: "An email has been sent to your email address. Follow the directions in the email to reset your password.", controller: self, okBtnTitle: Constants.OKTItle)
                    }else{
                        Helper.showAlert(title: "", message: "The email doesn't have an account.", controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            })
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
}
