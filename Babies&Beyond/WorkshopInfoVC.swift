//
//  WorkshopInfoVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/15/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class WorkshopInfoVC: UIViewController {

    @IBOutlet weak var WorkshopNameLbl: UILabel!
    @IBOutlet weak var dateFromLbl: UILabel!
    @IBOutlet weak var timeFromLbl: UILabel!
    @IBOutlet weak var dateToLbl: UILabel!
    @IBOutlet weak var timeToLbl: UILabel!
    @IBOutlet weak var eventStatusLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var speakerName: UILabel!
    @IBOutlet weak var workshopStatusContainerView: UIView!
    @IBOutlet weak var speakerBio: UITextView!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var feeLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusContainerView: UIView!
    @IBOutlet weak var sendRequestContainerViewRightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var sendRequestOrCancelBtn: EMSpinnerButton!
    @IBOutlet weak var payBtn: EMSpinnerButton!
    var workshopDetails : WorkshopsDetails?
    var itemIndex:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Workshop Information"
        self.sendRequestOrCancelBtn.cornerRadius = 5.0
        self.sendRequestOrCancelBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.sendRequestOrCancelBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        
        self.payBtn.cornerRadius = 5.0
        self.payBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.payBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        
        setupView()
    }
    
    /* we added isChangedNow parameter cause buttons titles changes in viewDidLoad with setTitle method and
    after that with storedTitle method */
    fileprivate func checkWorkshopStatus(_ workshopDetailsObj: WorkshopsDetails, isChangedNow:Bool) {
        if let status = workshopDetailsObj.service_workshop_status_name{
            if status == "" {
                sendRequestContainerViewRightConstraint.constant = 0
                statusContainerView.isHidden = true
                feeLabelTopConstraint.constant = -50
                self.sendRequestOrCancelBtn.isUserInteractionEnabled = true
                if isChangedNow {
                    self.sendRequestOrCancelBtn.storedTitle = "Send Request"
                }else{
                    self.sendRequestOrCancelBtn.setTitle("Send Request", for: .normal)
                }
                self.payBtn.isHidden = true
            }else if status == "Cash On Delivery"{
                statusContainerView.isHidden = false
                feeLabelTopConstraint.constant = 8
                eventStatusLbl.text = status
                self.sendRequestOrCancelBtn.isHidden = true
                self.payBtn.isHidden = true
            }else if status == "Pending" {
                if isChangedNow {
                    self.sendRequestOrCancelBtn.storedTitle = "Cancel"
                }else{
                    self.sendRequestOrCancelBtn.setTitle("Cancel", for: .normal)
                }
                statusContainerView.isHidden = false
                feeLabelTopConstraint.constant = 8
                eventStatusLbl.text = "Pending"
                self.payBtn.isHidden = true
            }else{
                eventStatusLbl.text = "Ask for payment"
                statusContainerView.isHidden = false
                feeLabelTopConstraint.constant = 8
                if isChangedNow {
                    self.payBtn.storedTitle = "Pay"
                    self.sendRequestOrCancelBtn.storedTitle = "Cancel"
                }else{
                    self.payBtn.setTitle("Pay", for: .normal)
                    self.sendRequestOrCancelBtn.setTitle("Cancel", for: .normal)
                }
                sendRequestContainerViewRightConstraint.constant = self.view.bounds.width/2
            }
        }
    }
    
    func setupView() {
        if let workshopDetailsObj = workshopDetails{
            WorkshopNameLbl.text = workshopDetailsObj.name
            
            if let startDate = workshopDetailsObj.start_date{
                dateFromLbl.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
            }
            if let startTime = workshopDetailsObj.start_date{
                timeFromLbl.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
            }
            if let endDate = workshopDetailsObj.end_date{
                dateToLbl.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
            }
            if let endTime = workshopDetailsObj.end_date{
                timeToLbl.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
            }
            
            if let feeValue = workshopDetailsObj.price {
                feeLabel.text = "\(feeValue) $"
            }
            
            checkWorkshopStatus(workshopDetailsObj,isChangedNow: false)
            
            locationLbl.text = workshopDetailsObj.location
            speakerName.text = workshopDetailsObj.speaker_name
            speakerBio.text = workshopDetailsObj.speaker_bio
        }
    }
    
    @IBAction func payBtnTapped(_ sender: UIButton) {
        guard let workshopID = workshopDetails?.id else{
            return
        }
        let paymentViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectPaymentMethodViewController") as! SelectPaymentMethodViewController
        paymentViewController.selectedId = workshopID
        paymentViewController.isService = false
        paymentViewController.isMidwife = false
        self.navigationController?.pushViewController(paymentViewController, animated: true)
    }
    
    @IBAction func SendRequestOrCancelBtnTapped(_ sender: UIButton) {
        
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        guard let workshopID = workshopDetails?.id else{
            return
        }
        
        // in case of the workshop hasn't beeny∞ requested before.
        if workshopDetails?.service_workshop_status_name == "" {
            self.sendWorkshopRequest(userID : userID,workshopID:workshopID)
        }else {
            // in case of the workshop is requested and the workshop not moved to the confirm wih payment state so the user can cancel it.
            self.cancelWorkshopRequest(userID: userID, workshopID: workshopID)
        }
    }
    
    
    func sendWorkshopRequest(userID : Int,workshopID:String) {
        self.view.isUserInteractionEnabled = false
        self.sendRequestOrCancelBtn.animate(animation: .collapse)
        
        let parameters = ["user_id":userID, "workshop_id":workshopID]  as [String:Any]
        
        UserService.workshopRequest(Constants.WorkshopRequestApi, params: parameters, completion: { (response, error) in
            self.sendRequestOrCancelBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let workshopReqestResponseResult = response as? WorkshopReqestResponse {
                    if (workshopReqestResponseResult.data?.id != nil){
                        AccountManager.shared().allWorkshops![self.itemIndex!].service_workshop_status_name = "Pending"
                        self.workshopDetails?.service_workshop_status_name = "Pending"
                        self.checkWorkshopStatus(self.workshopDetails!,isChangedNow: true)
                        self.view.makeToast("you are going")
                    }else{
                        return
                    }
                }
            }
        })
    }
    
    
    func cancelWorkshopRequest(userID : Int,workshopID:String) {
        self.sendRequestOrCancelBtn.animate(animation: .collapse)
        UserService.cancelWorkshop(workshopId: workshopID, userId: userID, completion: { (response, error) in
            self.sendRequestOrCancelBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let response = response as? CommentRateResponse {
                    if response.status!{
                        AccountManager.shared().allWorkshops![self.itemIndex!].service_workshop_status_name = ""
                        self.workshopDetails?.service_workshop_status_name = ""
                        self.checkWorkshopStatus(self.workshopDetails!,isChangedNow: true)
                        self.view.makeToast("the workshop cancelled")
                    }else{
                        return
                    }
                }
            }
        })
    }
}

