//
//  ChatLogController.swift
//  ChatLibraryModule
//
//  Created by M.I.Kamashany on 11/27/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import FirebaseStorage
import MobileCoreServices
import AVFoundation
import SDWebImage

class ChatLogController: UICollectionViewController {
    
    let cellId = "cellId"
    var messages = [Message]()
    var startingFrame:CGRect?
    var blackBackgroundView:UIView?
    var selectedGroup:Group? {
        didSet {
            self.navigationItem.title = selectedGroup?.name
            observeMessages()
        }
    }
    var isUserFired:Bool = false
    
    func observeMessages() {
        guard let groupID = selectedGroup?.id else {
            return
        }
        
        let groupMessagesRef = Database.database().reference().child("groups").child(groupID)
        groupMessagesRef.observe(DataEventType.childAdded, with: { (snapshot) in
            guard let dic = snapshot.value as? [String:Any] else{
                return
            }
            
            self.messages.append(Message(dictionary: dic))
            self.collectionView?.reloadData()
            //scroll to the last item in collectionview
            let lastIndexPath = IndexPath(item: self.messages.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: lastIndexPath, at: UICollectionViewScrollPosition.bottom, animated: true)
        }, withCancel: nil)
    }
    
    
    lazy var inputContainerView:ChatInputContainerView = {
        let containerView = ChatInputContainerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        containerView.chatLogController = self
        return containerView
    }()
    
    @objc func handleUploadImageViewTapped() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        // this commented line if you want to select videos from the gallery.
        // imagePickerController.mediaTypes = [kUTTypeImage as String,kUTTypeMovie as String]
        present(imagePickerController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        //        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        collectionView?.backgroundColor = UIColor.white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.keyboardDismissMode = .interactive
//        self.tabBarController?.tabBar.isHidden = true
        setupKeyboardObserver()
        observeChanges()
    }
    
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(groupsChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Group.rawValue), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Group.rawValue), object: nil)
    }
    
    @objc func groupsChanged(_ notification:NSNotification){
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        guard let groupId = notification.userInfo?[Constants.Id] as? String else {
            return
        }
        if groupId == selectedGroup?.id {
            isUserFired = true
            switch action {
            case GroupNotificationAction.GroupDeleted.rawValue:
                Helper.dismissViewControllerByAlert(title: "Owner's decision", message: "Group Owner deleted the group, so you can't chat in it anymore.", controller: self, okBtnTitle: "Ok", completion: {
                     self.navigationController?.popViewController(animated: true)
                })
                break
            case GroupNotificationAction.MemberRemoved.rawValue:
                Helper.dismissViewControllerByAlert(title: "Owner's decision", message: "Group Owner removed your membership , so you can't chat in it anymore.", controller: self, okBtnTitle: "Ok", completion: {
                    self.navigationController?.popViewController(animated: true)
                })
                break
            default: break
                // no action will be taken
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Group.rawValue), object: nil)
    }
    
    override var inputAccessoryView: UIView? {
        get{
//            if isUserFired {
//                return nil
//            }
            return inputContainerView
        }
    }
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    
    @objc func handleSend()  {
        if InternetConnection.connected() {
            if let messageText = inputContainerView.messageTextField.text, !messageText.isEmpty {
                let properties:[String : Any] = ["message": messageText]
                sendMessageWithProperties(properties: properties)
            }
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func sendMessageWithImageUrl(imageUrl:String, image:UIImage){
        let properties:[String : Any] = ["imageURL":imageUrl,
                                         "imgWidth":image.size.width,
                                         "imgHeight":image.size.height]
        
        sendMessageWithProperties(properties: properties)
    }
    
    func sendMessageWithProperties(properties:[String:Any]){
        guard let groupID = selectedGroup?.id else {
            return
        }
        let groupMessagesRef = Database.database().reference().child("groups").child(groupID)
        let newMessageRef = groupMessagesRef.childByAutoId()
        let fromId = AccountManager.shared().userData?.id
        let timestamp = Int(NSDate().timeIntervalSince1970)
        var values = ["timeStamp":timestamp,
                      "userId":fromId!] as [String : Any]
        // append properties dictionary onto values somehow ??
        //key is $0 , value is $1
        properties.forEach({values[$0] = $1})
        
        newMessageRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            self.addUser()
            self.inputContainerView.messageTextField.text = nil
        }
    }
    
    // add my data in the database
    private func addUser(){
        guard let user = AccountManager.shared().userData else {
            return
        }
        let userRef = Database.database().reference().child("users").child("\(user.id!)")
        let values = ["name":user.name!,
                      "photo":user.photo!] as [String : Any]
        userRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
        }
    }
    //zooming in message image
    func performZoomInForStartingImageView(imageView: UIImageView)  {
        startingFrame = imageView.superview?.convert(imageView.frame, to: nil)
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.backgroundColor = UIColor.red
        zoomingImageView.image = imageView.image
        zoomingImageView.isUserInteractionEnabled = true
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        if let keywindow = UIApplication.shared.keyWindow {
            
            blackBackgroundView = UIView()
            blackBackgroundView?.frame = keywindow.frame
            blackBackgroundView?.backgroundColor = UIColor.black
            blackBackgroundView?.alpha = 0
            keywindow.addSubview(blackBackgroundView!)
            keywindow.addSubview(zoomingImageView)
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                self.inputContainerView.alpha = 0
                self.blackBackgroundView?.alpha = 1
                //math?
                // h2/w2 = h1/w1
                // h2 = h1/w1 * w2
                let height  = self.startingFrame!.height / self.startingFrame!.width * keywindow.frame.width
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keywindow.frame.width, height: height)
                zoomingImageView.center = keywindow.center
            }, completion: nil)
        }
    }
    
    @objc func handleZoomOut(tapGesture:UITapGestureRecognizer) {
        if let zoomOutImageView = tapGesture.view {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                zoomOutImageView.frame = self.startingFrame!
                zoomOutImageView.layer.cornerRadius = 16
                zoomOutImageView.layer.masksToBounds = true
                self.blackBackgroundView?.alpha = 0
                self.inputContainerView.alpha = 1
            }, completion: { (completed) in
                zoomOutImageView.removeFromSuperview()
            })
        }
    }
}

extension ChatLogController: UICollectionViewDelegateFlowLayout {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatMessageCell
        cell.chatLogController = self
        let message = messages[indexPath.item]
        cell.textview.text = message.message
        setupCell(cell: cell, message: message)
        if message.imageURL == nil {
            cell.bubbleWidthAnchor?.constant = Helper.estimateFromForText(text: message.message!, width: 200).width + 30
            cell.textview.isHidden = false
        }else {
            cell.bubbleWidthAnchor?.constant = 200
            cell.textview.isHidden = true
        }
        
        return cell
    }
    
    private func setupCell(cell:ChatMessageCell, message:Message){
        if let profileImageURL = AccountManager.shared().userData?.photo {
             cell.profileImageView.sd_setImage(with: URL(string: profileImageURL), placeholderImage: UIImage(named: "profilePic"), options: SDWebImageOptions.lowPriority, completed: nil)
        }
        
        if message.userId?.intValue == AccountManager.shared().userData?.id! {
            cell.bubbleview.backgroundColor = ChatMessageCell.blueColor
            cell.textview.textColor = UIColor.white
            cell.profileImageView.isHidden = true
            cell.bubbleLeftAnchor?.isActive = false
            cell.bubbleRightAnchor?.isActive = true
            cell.usernameLabelHeightAnchor?.constant = 0
        }else{
            cell.bubbleview.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
            cell.textview.textColor = UIColor.black
            cell.profileImageView.isHidden = false
            cell.bubbleLeftAnchor?.isActive = true
            cell.bubbleRightAnchor?.isActive = false
            cell.usernameLabelHeightAnchor?.constant = 13
            getReceiverInfo(withId: message.userId!.stringValue, cell: cell)
        }
        
        if message.imageURL != nil {
            if let messageImageURL = message.imageURL {
                // cell.messageImageView.loadImageFromCacheWithURLString(urlString: messageImageURL)
                cell.messageImageView.sd_setImage(with: URL(string: messageImageURL), placeholderImage: UIImage.from(color: UIColor.lightGray), options: SDWebImageOptions.lowPriority, completed: nil)
                cell.messageImageView.sd_setShowActivityIndicatorView(true)
                cell.messageImageView.sd_setIndicatorStyle(.white)
                cell.bubbleview.backgroundColor = UIColor.clear
                cell.messageImageView.isHidden = false
            }
        }else{
            cell.messageImageView.isHidden = true
        }
    }
    
    fileprivate func getReceiverInfo(withId:String, cell:ChatMessageCell){
        
        let userRef = Database.database().reference().child("users").child(withId)
        userRef.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            guard let dic = snapshot.value as? [String:Any] else{
                return
            }
            guard let profileImageURLString = dic["photo"] as? String else {
                return
            }
            
            guard let username = dic["name"] as? String else {
                return
            }
            
            cell.usernameLabel.text = username
            cell.profileImageView.sd_setImage(with: URL(string: profileImageURLString), placeholderImage: UIImage(named: "Personimage"), options: SDWebImageOptions.lowPriority, completed: nil)
        }, withCancel: nil)
    }
    
    func setupKeyboardObserver(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatLogController.handleKeyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    //scroll to the last item in collectionview
    @objc func handleKeyboardDidShow(notification: Notification){
        if messages.count > 0 {
            let lastIndexPath = IndexPath(item: self.messages.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: lastIndexPath, at: UICollectionViewScrollPosition.bottom, animated: true)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height:CGFloat = 80
        
        let message = messages[indexPath.item]
        if message.imageURL == nil {
            if let text = message.message {
                height = Helper.estimateFromForText(text: text, width: 200).height + 20
            }
        }else {
            if let imageWidth = message.imageWidth?.floatValue, let imageHeight = message.imageHeight?.floatValue {
                height = CGFloat(imageHeight/imageWidth * 200)
            }
        }
        
//        var result:CGFloat = 0
//        if message.userId?.intValue != AccountManager.shared().userData?.id! {
//           result = height + 13
//        }else{
//           result = height
//        }
        
        let width = UIScreen.main.bounds.width
        if message.userId?.intValue != AccountManager.shared().userData?.id! {
            return CGSize(width: width, height: height+13)
        }
        return CGSize(width: width, height: height)
    }
}


extension ChatLogController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        handleImageSelectedForInfo(info: info)
        dismiss(animated: true, completion: nil)
    }
    
    private func handleImageSelectedForInfo(info:[String:Any]) {
        var selectedImageFromPicker:UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            uploadToFirebaseStorageUsingImage(image: selectedImage, completion: { (imageUrl) in
                self.sendMessageWithImageUrl(imageUrl:imageUrl,image: selectedImage)
            })
        }
    }
    
    private func uploadToFirebaseStorageUsingImage(image:UIImage,completion: @escaping (_ imageUrl:String) -> ()){
        let imageName = NSUUID().uuidString
        let ref = Storage.storage().reference().child("message_images").child(imageName)
        if let uploadedData = UIImageJPEGRepresentation(image, 0.2) {
            ref.putData(uploadedData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    return
                }
                ref.downloadURL(completion: { (url, error) in
                    if let currentUrl = url {
                    completion(currentUrl.absoluteString)
                    }
                })
//                if let imageURL = metad metadata?.downloadURL()?.absoluteString {
//                }
            })
        }
    }
}
