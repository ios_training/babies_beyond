//
//  AllServicesResponse.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 1/26/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class AllServicesResponse: Codable {
    
    var status : Bool?
    var message : String?
    var data : Home_page?
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
}
