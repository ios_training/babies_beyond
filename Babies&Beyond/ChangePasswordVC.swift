//
//  ChangePasswordVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/24/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var oldPasswordTxt: UITextField!
    @IBOutlet weak var newPasswordTxt: UITextField!
    @IBOutlet weak var confirmNewPasswordTxt: UITextField!
    @IBOutlet weak var changePasswordButton: EMSpinnerButton!
    @IBOutlet weak var cancelButton: EMSpinnerButton!
    
    @IBOutlet weak var submitBtn: EMSpinnerButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        oldPasswordTxt.isSecureTextEntry = true
        newPasswordTxt.isSecureTextEntry = true
        confirmNewPasswordTxt.isSecureTextEntry = true
        self.cancelButton.cornerRadius = 5.0
        self.cancelButton.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.submitBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        self.submitBtn.cornerRadius = 5.0
        self.submitBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.submitBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
    }
    
    func changePasswordRequest(newPassword:String, oldPassword:String, userID:Int) {
        self.view.isUserInteractionEnabled = false
        self.submitBtn.animate(animation: .collapse)
        let parameters = ["user_id":userID,"password":newPassword,"old_password":oldPassword]  as [String:Any]
        
        UserService.changePassword(Constants.ChangePasswordApi, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            self.submitBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                // basic usage
//                self.view.makeToast(Constants.FailureAlert)
            }else{
                // response
                guard let updateProfileResponse = response as? CommentRateResponse else{
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    return
                }
                if updateProfileResponse.status! {
                    self.dismiss(animated: true, completion: {
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ChangePasswordSuccessAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    })
                }else{
                    ProgressHUD.showSuccess()
                    Helper.showAlert(title: Constants.errorAlertTitle, message: updateProfileResponse.message!, controller: self, okBtnTitle: Constants.OKTItle)
                }
            }
        })
    }
    
    // Mark: Button actions
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        guard let currentUser = AccountManager.shared().userData else{
            return
        }
        
        guard let oldPassword = oldPasswordTxt.text, !oldPassword.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyCurrentPassword, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let newPassword = newPasswordTxt.text, !newPassword.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyNewPassword, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let confirmNewPassword = confirmNewPasswordTxt.text, !confirmNewPassword.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.newPasswordAndConfirmation, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard confirmNewPassword == newPassword else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.newPasswordAndConfirmation, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard oldPassword != newPassword else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.oldPasswordSimilarNewPassword, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        if InternetConnection.connected() {
            changePasswordRequest(newPassword:newPassword, oldPassword: oldPassword, userID:currentUser.id!)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
