//
//  MapVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/16/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import GooglePlacePicker

class MapVC: UIViewController {
    @IBOutlet var viewContainer:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
         getPlacePickerView() 
    }
    func getPlacePickerView() {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
}

extension MapVC : GMSPlacePickerViewControllerDelegate
{
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.viewContainer.isHidden = false
       // self.indicatorView.isHidden = true
        
        viewController.dismiss(animated: true, completion: nil)
        
//        self.lblName.text = place.name
//        self.lblAddress.text = place.formattedAddress?.components(separatedBy: ", ")
//            .joined(separator: "\n")
//        self.lblLatitude.text = String(place.coordinate.latitude)
//        self.lblLongitude.text = String(place.coordinate.longitude)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        self.viewContainer.isHidden = true
       // self.indicatorView.isHidden = true
    }
}
