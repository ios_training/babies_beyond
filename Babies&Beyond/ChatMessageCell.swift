//  ChatMessageCell.swift
//  ChatLibraryModule
//
//  Created by M.I.Kamashany on 11/30/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

import UIKit
import AVFoundation

class ChatMessageCell: UICollectionViewCell {
    
    var message:Message?
    var chatLogController: ChatLogController?
    
    let activityIndicatorView: UIActivityIndicatorView = {
       let aiv = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()

    
    let textview:UITextView = {
        let tv = UITextView()
        tv.text = "Hello world"
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textColor = UIColor.white
        tv.isEditable = false
        return tv
    }()
    
    static let blueColor = UIColor(red: 0, green: 137/255, blue: 249/255, alpha: 1.0)
    
    let bubbleview : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = blueColor
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        return view
    }()
    
    let usernameLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.textColor = UIColor.lightGray
        label.font = UIFont(name: "MuseoSans-300", size: 12)
        return label
    }()
    
    var bubbleWidthAnchor: NSLayoutConstraint?
    var bubbleRightAnchor: NSLayoutConstraint?
    var bubbleLeftAnchor: NSLayoutConstraint?
    var usernameLabelHeightAnchor: NSLayoutConstraint?
    
    let profileImageView :UIImageView = {
        let imageview = UIImageView()
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.image = UIImage(named: "gameOfThrones")
        imageview.layer.cornerRadius = 14
        imageview.layer.masksToBounds = true
        return imageview
    }()
    
    lazy var messageImageView:UIImageView = {
       let imageview = UIImageView()
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.layer.cornerRadius = 16
        imageview.layer.masksToBounds = true
        imageview.isUserInteractionEnabled = true
        imageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))
        imageview.contentMode = .scaleAspectFill
        return imageview
    }()
    
    @objc func handleZoomTap(tapGesture: UITapGestureRecognizer)  {
        let tappedImageView = tapGesture.view as? UIImageView
        //tip: don't perform a lot of custm logic inside of a view class
        self.chatLogController?.performZoomInForStartingImageView(imageView: tappedImageView!)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(usernameLabel)
        addSubview(bubbleview)
        addSubview(textview)
        addSubview(profileImageView)
        bubbleview.addSubview(messageImageView)
        messageImageView.leftAnchor.constraint(equalTo: bubbleview.leftAnchor).isActive = true
        messageImageView.topAnchor.constraint(equalTo: bubbleview.topAnchor).isActive = true
        messageImageView.widthAnchor.constraint(equalTo: bubbleview.widthAnchor).isActive = true
        messageImageView.heightAnchor.constraint(equalTo: bubbleview.heightAnchor).isActive = true

        bubbleview.addSubview(activityIndicatorView)
        activityIndicatorView.centerXAnchor.constraint(equalTo: bubbleview.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: bubbleview.centerYAnchor).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalToConstant: 36).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalToConstant: 36).isActive = true

        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 28).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 28).isActive = true
        
        //constraints
        bubbleRightAnchor = bubbleview.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8)
        bubbleRightAnchor?.isActive = true
        bubbleLeftAnchor = bubbleview.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8)
        bubbleLeftAnchor?.isActive = false
        bubbleview.topAnchor.constraint(equalTo: self.usernameLabel.bottomAnchor).isActive = true
        bubbleview.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        bubbleWidthAnchor = bubbleview.widthAnchor.constraint(equalToConstant: 200)
        bubbleWidthAnchor?.isActive = true
        
        usernameLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        usernameLabel.leftAnchor.constraint(equalTo: self.bubbleview.leftAnchor).isActive = true
        usernameLabelHeightAnchor = usernameLabel.heightAnchor.constraint(equalToConstant: 13)
        usernameLabelHeightAnchor?.isActive = true
        usernameLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        
        textview.leftAnchor.constraint(equalTo: bubbleview.leftAnchor, constant: 8).isActive = true
        textview.topAnchor.constraint(equalTo: self.bubbleview.topAnchor).isActive = true
        textview.bottomAnchor.constraint(equalTo: self.bubbleview.bottomAnchor).isActive = true
        textview.rightAnchor.constraint(equalTo: bubbleview.rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
