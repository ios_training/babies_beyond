//
//  GetServiceQutationViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 5/29/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import GooglePlacePicker
import FormTextField

class GetServiceQutationViewController: UIViewController {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var chooseService: UITextField!    
    @IBOutlet weak var numberOfDays: FormTextField!
    @IBOutlet weak var numberOfHours: FormTextField!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var specificRequest: KMPlaceholderTextView!
    @IBOutlet weak var submitButton: EMSpinnerButton!
    
    var longitude:Double?
    var latitude:Double?
    var address:String?
    let cha:Int = 24
    var validation = Validation()
    var validation1 = Validation()

    
    var reasonsArr = ["Newborn Care","Child Development",".Professional Babysitting","Breastfeeding & postnatal","Sleep Support","Nanny Training"]
    let reasonsPicker = UIPickerView()
    let servicePlaceHolder = "Select Your Service"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        chooseService.inputView = reasonsPicker
        chooseService.placeholder = servicePlaceHolder
        reasonsPicker.delegate = self
        reasonsPicker.selectRow(0, inComponent:0, animated:true)
        self.submitButton.cornerRadius = 5.0
        self.submitButton.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        self.title = "Get Service Quotation"
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:  82/255.0, green: 87/255.0, blue: 106/255.0, alpha: 100.0/100.0)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        validation.minimumValue = 1
        validation.maximumValue = 24
        validation1.minimumValue = 1
        validation1.maximumValue = 31
        numberOfHours.inputValidator = InputValidator(validation: validation)
        numberOfDays.inputValidator = InputValidator(validation: validation1)


    }
    


    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(locationLblTapped))
        //locationLbl.isUserInteractionEnabled = true
        locationLbl.addGestureRecognizer(tap)
    }
    
    @objc func locationLblTapped(sender:UITapGestureRecognizer) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func GetServiceQuotionAction(_ sender: Any) {
        
        guard  let userID = AccountManager.shared().userData?.id else{
            return
        }

        guard let servicename = chooseService.text, !servicename.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyService, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let numberofDaysPerMonth = numberOfDays.text , !numberofDaysPerMonth.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyNumberOfDays, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let numberOfHoursADay = numberOfHours.text , !numberOfHoursADay.isEmpty

            else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyNumberOfHours, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let specificRequestOrConcerns = specificRequest.text , !specificRequestOrConcerns.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptySpecificRequest, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
//        guard let location = locationLbl.text , !location.isEmpty else{
//            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyLocation, controller: self, okBtnTitle: Constants.OKTItle)
//            return
//        }
        guard let location = address,let _ = longitude, let _ = latitude, !location.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyLocation, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        
        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.submitButton.animate(animation: .collapse)
            getQuotation(userID: userID, servicename: servicename, numberofDays: numberofDaysPerMonth, numberofHours: numberOfHoursADay, specificRequest: specificRequestOrConcerns, location: location)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    
    
    func getQuotation(userID:Int,servicename:String,numberofDays:String,numberofHours:String,specificRequest:String,location:String){
        
        let parameters = ["user_id":userID, "service_name":servicename, "no_of_days_required_per_month":numberofDays,"no_of_hours_aday": numberofHours,"specific_request":specificRequest ,"location":location] as [String:Any]
        UserService.getServiceQuotationUser(Constants.GetServiceQuotationApi, params: parameters , completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            if let error = error {
                self.submitButton.animate(animation: .expand)
                // show alert with error message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? GetServiceQuotationResponse {
                    if result.status!{
                        self.submitButton.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.submitButtonMessage, controller: self, okBtnTitle: Constants.OKTItle)
                            self.chooseService.text = ""
                            self.locationLbl.text = ""
                            self.numberOfDays.text = ""
                            self.numberOfHours.text = ""
                            self.specificRequest.text = ""
                        
                    }else{
                        self.submitButton.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    return
                }
            }
        })
    }
    
}



extension GetServiceQutationViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return reasonsArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return reasonsArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        chooseService.text = reasonsArr[row]
    }
}

extension GetServiceQutationViewController : GMSPlacePickerViewControllerDelegate {
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.viewContainer.isHidden = false
        // self.indicatorView.isHidden = true
        address = place.formattedAddress?.components(separatedBy: ", ").joined(separator: ",")
        longitude = place.coordinate.longitude
        latitude = place.coordinate.latitude
        self.locationLbl.text = address
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}
