//
//  containerViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/22/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import ISHPullUp

class containerViewController: ISHPullUpViewController {


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }

    private func commonInit() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let contentVC = storyBoard.instantiateViewController(withIdentifier: "content") as! CalenadarViewController
        let bottomVC = storyBoard.instantiateViewController(withIdentifier: "bottom") as! ScheduleViewController
        contentViewController = contentVC
        bottomViewController = bottomVC
        bottomVC.pullUpController = self
        contentDelegate = contentVC as? ISHPullUpContentDelegate
        sizingDelegate = bottomVC as? ISHPullUpSizingDelegate
        stateDelegate = bottomVC as? ISHPullUpStateDelegate
    }
    
}
