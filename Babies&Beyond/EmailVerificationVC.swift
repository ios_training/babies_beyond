//
//  VerificationCodeVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 2/26/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class EmailVerificationVC: UIViewController {

    
    @IBOutlet weak var VerificationCodeTxt: UITextField!
    @IBOutlet weak var doneBtn: EMSpinnerButton!
    @IBOutlet weak var resendVerificationCodeBtn: EMSpinnerButton!
    @IBOutlet weak var activityLoading: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.doneBtn.cornerRadius = 5.0
        self.doneBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        resendVerificationCodeBtn.backgroundColor = UIColor.clear
        self.activityLoading.stopAnimating()
    }
    
    @IBAction func sendTapped(_ sender: UIButton) {
        
        guard let userVerificationCode = VerificationCodeTxt.text, !userVerificationCode.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyEmailVerificationAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let verificationCode = AccountManager.shared().verificationCode else{
            return
        }
        guard let  confirmationCode = VerificationCodeTxt.text, Int(confirmationCode) == verificationCode else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.enteredCodeAndSentCode, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let userEmail = AccountManager.shared().userData?.email else {
            return
        }
        
        if InternetConnection.connected() {
            self.activateRequest( userEmail : userEmail , apiName: Constants.ActiveAccountApi)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
        
    }
    
    
    func activateRequest( userEmail : String, apiName: String) {
        if apiName == Constants.ActiveAccountApi {
            self.doneBtn.animate(animation: .expand)
        }
        let parameters = ["email":userEmail]  as [String:Any]
        UserService.activateAccountOrSendCode(apiName, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            self.activityLoading.stopAnimating()
            self.resendVerificationCodeBtn.isHidden = false
            if let _ = error {
                self.doneBtn.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result = response as? ActivateAccountResponse {
                    if result.status!{
                        if result.data == nil{
                            AccountManager.shared().userData?.isActivate = AccountActivated.ActiveAccount.rawValue
                            self.navigateToUserScreens()
                        }else{
                            AccountManager.shared().verificationCode = result.data
                            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.CodeSentSuccessfully, controller: self, okBtnTitle: Constants.OKTItle)
                        }
                    }else{
                        self.doneBtn.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                        return
                    }
                }
            }
        })
    }
    private func navigateToUserScreens(){
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func resendTapped(_ sender: UIButton) {
        
        if InternetConnection.connected() {
            activityLoading.startAnimating()
            resendVerificationCodeBtn.isHidden = true
            guard let userEmail = AccountManager.shared().userData?.email else {
                return
            }
            self.activateRequest( userEmail : userEmail , apiName: Constants.ResendVerificationCodeApi)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
       
    }
    
}
