//
//  DatePopupVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/25/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit

class DatePopupVC: UIViewController{
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var userSendRequestVC : UserSendRequestVC?
    var nurseSendRequestVC : RequestNurseServiceViewController?

    var isStartDate:Bool?
    var isEndDate:Bool?

    var submitStartDate:String?
    var submitEndDate:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        let currentDate: Date = Date()
        var components: DateComponents = DateComponents()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        components.calendar = calendar
        components.day = +1
        if isStartDate == true{
            let currentDate: Date = Date()
            var components: DateComponents = DateComponents()
            var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            components.calendar = calendar
            components.day = +1
            datePicker.minimumDate = calendar.date(byAdding: components, to: currentDate)
        }else{
            let minimumDate: Date = (userSendRequestVC?.minimumEndDate)!
            var components: DateComponents = DateComponents()
            var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            components.calendar = calendar
            components.hour = +1
            datePicker.minimumDate = calendar.date(byAdding: components, to: minimumDate)
        }
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
    }
    
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let selectDate = dateFormatter.string(from: sender.date)
        if isStartDate == true{
            userSendRequestVC?.minimumEndDate = sender.date
            userSendRequestVC?.selectedStartDateTime = "\(selectDate)"
            nurseSendRequestVC?.minimumEndDate = sender.date
            nurseSendRequestVC?.selectedStartDateTime = "\(selectDate)"

        }else{
            userSendRequestVC?.selectedEndDateTime = "\(selectDate)"
            nurseSendRequestVC?.selectedEndDateTime = "\(selectDate)"

        }
        
    }
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        datePickerValueChanged(sender: datePicker)
        dismiss(animated: true, completion: nil)
    }
}
