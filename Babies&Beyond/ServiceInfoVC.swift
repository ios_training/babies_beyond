//
//  ServiceInfoVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/4/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class ServiceInfoVC: UIViewController {
    
    @IBOutlet weak var serviceTypeLbl: UILabel!
    @IBOutlet weak var dateFromLbl: UILabel!
    @IBOutlet weak var timeFromLbl: UILabel!
    @IBOutlet weak var dateToLbl: UILabel!
    @IBOutlet weak var timeToLbl: UILabel!
    @IBOutlet weak var serviceStatusLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var feeLbl: UILabel!
    
    var serviceDetails : Services?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Service Information"
        setupView()
    }
    
    func setupView() {
        if let serviceDetailsObj = serviceDetails{
            serviceTypeLbl.text = serviceDetailsObj.service_type_name
  
            if let startDate = serviceDetailsObj.start_date{
                dateFromLbl.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
            }
            if let startTime = serviceDetailsObj.start_date{
                timeFromLbl.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
            }
            if let endDate = serviceDetailsObj.end_date{
                dateToLbl.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
            }
            if let endTime = serviceDetailsObj.end_date{
                timeToLbl.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
            }
            
            serviceStatusLbl.text = serviceDetailsObj.service_workshop_status_name
            locationLbl.text = serviceDetailsObj.location
            feeLbl.text = serviceDetailsObj.price
        }
    }



}
