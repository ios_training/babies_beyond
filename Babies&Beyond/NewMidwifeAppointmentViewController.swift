//
//  NewMidwifeAppointmentViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/26/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import JBDatePicker
import WWCalendarTimeSelector

class NewMidwifeAppointmentViewController: UIViewController {
    
    @IBOutlet weak var startDateBtnContainerView: UIView!
    @IBOutlet weak var endDateBtnContainerView: UIView!
    @IBOutlet weak var startDateBtn:UIButton!
    @IBOutlet weak var endDateBtn:UIButton!
    @IBOutlet weak var datePickerView: JBDatePickerView!
    @IBOutlet weak var addNewAppointmentBtn: EMSpinnerButton!
    @IBOutlet weak var monthDescriptionLabel: UILabel!
    @IBOutlet weak var monthDescriptionContainerView: UIView!
    
    
    // MARK: public variables
    public var selectedMidwife:Midwife! // impossible to be nil
    var appointmentsArr:[MidwifeTime]! // impossible to be nil
    
    // MARK: private variables
    fileprivate var newAppointment = MidwifeTime()
    fileprivate var isStartHour = true
    fileprivate var dateFormatter:DateFormatter = {
        let formatter = DateFormatter()
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialTimeForNewAppointment()
        startDateBtnContainerView.layer.borderColor = UIColor.lightGray.cgColor
        startDateBtnContainerView.layer.borderWidth = 0.5
        endDateBtnContainerView.layer.borderColor = UIColor.lightGray.cgColor
        endDateBtnContainerView.layer.borderWidth = 0.5
        datePickerView.delegate = self
        self.addNewAppointmentBtn.cornerRadius = 5
        self.addNewAppointmentBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        monthDescriptionContainerView.backgroundColor = UIColor(r: 226, g: 202, b: 193)
    }
    
    // default startHour and endHour will be the first timeSlot in availableTimes array of the selectedMidwife
    private func setInitialTimeForNewAppointment(){
        self.addNewAppointmentBtn.cornerRadius = addNewAppointmentBtn.bounds.size.height/2
        guard var fromHour = selectedMidwife?.availableTimes?.first?.from else{
            return
        }
    
        guard var toHour = selectedMidwife?.availableTimes?.first?.to else{
            return
        }
        
        let fromHourArr = fromHour.components(separatedBy: ":")
        fromHour = "\(fromHourArr[0]):\(fromHourArr[1])"
        
        let toHourArr = toHour.components(separatedBy: ":")
        toHour = "\(toHourArr[0]):\(toHourArr[1])"
        
        newAppointment.from = fromHour
        newAppointment.to = toHour
        
        // set the start hour of the first time slot to the startDateBtn title
        let fromHourStringIn12Format = Helper.to12DateFormat(dateIn24Format:fromHour)
        self.startDateBtn.setTitle(fromHourStringIn12Format, for: .normal)
        
        // set the end hour of the first timeslot to the endDateBtn title
        let toHourStringIn12Format = Helper.to12DateFormat(dateIn24Format:toHour)
        self.endDateBtn.setTitle(toHourStringIn12Format, for: .normal)
    }
    
    
    @IBAction func back(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectStartDate(_ sender:Any){
        isStartHour = true
        showCalendar()
    }
    
    @IBAction func selectEndDate(_ sender:Any){
        isStartHour = false
        showCalendar()
    }
    
    @IBAction func loadNextMonth(_ sender: UIBarButtonItem) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIBarButtonItem) {
        datePickerView.loadPreviousView()
    }
    
    func showCalendar() {
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        selector.optionTimeStep = .sixtyMinutes
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showTime(true)
        selector.optionStyles.showYear(false)
        selector.optionButtonShowCancel = true
        selector.optionLayoutHeight = (UIScreen.main.bounds.size.height/3)*2
        selector.optionLayoutTopPanelHeight = 50
        setSelectorCalenderTimeBeforeAppear(selector:selector,appointment: newAppointment)
        /*
         Any other options are to be set before presenting selector!
         */
        self.present(selector, animated: true, completion: nil)
    }
    
    private func setSelectorCalenderTimeBeforeAppear(selector:WWCalendarTimeSelector,appointment:MidwifeTime){
        if isStartHour {
            selector.optionTopPanelTitle = "From"
            if  let currentDateString = appointment.from {
                dateFormatter.dateFormat = "HH:mm"
                let currentDateObj = dateFormatter.date(from: currentDateString)
                if currentDateObj != nil {
                    selector.optionCurrentDate = currentDateObj!
                }else{
                    selector.optionCurrentDate = Date()
                }  
            }
        }else{
            selector.optionTopPanelTitle = "To"
            if  let currentDateString = appointment.to {
                dateFormatter.dateFormat = "HH:mm"
                let currentDateObj = dateFormatter.date(from: currentDateString)
                if currentDateObj != nil {
                    selector.optionCurrentDate = currentDateObj!
                }else{
                    selector.optionCurrentDate = Date()
                }
            }
        }
    }
    
    @IBAction func addNewAppointmentBtnTapped(_ sender:Any){
        guard let midwifeId = selectedMidwife?.id else {
            return
        }
        
        guard let fromHour = newAppointment.from else {
            return
        }
        
        guard let toHour = newAppointment.to else {
            return
        }
        
        guard let date = newAppointment.date else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyAppointmentDate, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        if InternetConnection.connected() {
            if isAppointmentOverlapped(fromHour:fromHour,toHour:toHour,date:date){
                checkMidwifeAvailabitity(midwifeId, fromHour, toHour, date)
            }else{
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.OverlappedAppointmentDate, controller: self, okBtnTitle: Constants.OKTItle)
            }
        }else{
            self.addNewAppointmentBtn.animate(animation: .expand)
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    private func isAppointmentOverlapped(fromHour: String,toHour: String, date:String) -> Bool {
        for appointment in appointmentsArr {
            if appointment.date == date {
                if (appointment.from! <= toHour && appointment.to! >= fromHour){
                   return false
                }
            }
        }
        return true
    }
    
    fileprivate func checkMidwifeAvailabitity(_ midwifeId: Int, _ fromHour: String, _ toHour: String, _ date: String) {
        self.addNewAppointmentBtn.animate(animation: .collapse)
        UserService.isMidwifeAppointmentAvailable(midwifeId: midwifeId, fromHour: fromHour, toHour: toHour, date: date, completion: { (response, error) in
            if let err = error {
                self.addNewAppointmentBtn.animate(animation: .expand)
                Helper.showAlert(title: Constants.errorAlertTitle, message: err, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                if let res = response as? CommentRateResponse {
                    if res.status! {
                        self.addNewAppointmentBtn.animate(animation: .expand)
                        let count = self.navigationController?.viewControllers.count
                        let previousViewController = self.navigationController?.viewControllers[count!-2] as! MidwifeAppointmentsViewController
                        previousViewController.appointmentsArr.append(self.newAppointment)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.addNewAppointmentBtn.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: res.message!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    self.addNewAppointmentBtn.animate(animation: .expand)
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    return
                }
            }
        })
    }
}

// MARK: - JBDatePickerViewDelegate
extension NewMidwifeAppointmentViewController: JBDatePickerViewDelegate {
    
    func didSelectDay(_ dayView: JBDatePickerDayView) {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        newAppointment.date = dateFormatter.string(from: dayView.date!)
        dateFormatter.dateFormat  = "EEEE"
        newAppointment.day = dateFormatter.string(from: dayView.date!)
    }
    
    /**
     Is called to check if any particular date is selectable by the picker
     - parameter date: the date to be checked on selectability
     */
    func shouldAllowSelectionOfDay(_ date: Date?) -> Bool {
        //this code example disables selection for dates older then today
        guard let date = date else {return true}
        let comparison = NSCalendar.current.compare(date, to: Date(), toGranularity: .day)
        
        if comparison == .orderedAscending || comparison == .orderedSame {
            return false
        }
        return true
    }
    
    var colorForWeekDaysViewBackground: UIColor {
        return UIColor(r: 226, g: 202, b: 193)
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        monthDescriptionLabel.text = datePickerView.presentedMonthView.monthDescription
    }
}

extension NewMidwifeAppointmentViewController: WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        if isStartHour {
            dateFormatter.dateFormat = "h:mm a"
            let startDateStringIn12Format = dateFormatter.string(from: date)
            self.startDateBtn.setTitle(startDateStringIn12Format, for: .normal)
            
            dateFormatter.dateFormat = "HH:mm"
            let startDateStringIn24Format = dateFormatter.string(from: date)
            self.newAppointment.from = startDateStringIn24Format
        }else{
            dateFormatter.dateFormat = "h:mm a"
            let endDateStringIn12Format = dateFormatter.string(from: date)
            self.endDateBtn.setTitle(endDateStringIn12Format, for: .normal)
            
            dateFormatter.dateFormat = "HH:mm"
            let endDateStringIn24Format = dateFormatter.string(from: date)
            self.newAppointment.to = endDateStringIn24Format
        }
    }
}
