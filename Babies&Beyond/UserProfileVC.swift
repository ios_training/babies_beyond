//
//  UserProfileVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/24/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import STPopup
import Alamofire
import MBProgressHUD
import SDWebImage
class UserProfileVC: UIViewController {

    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var BackOrCancelBtn: UIBarButtonItem!
    @IBOutlet weak var EditOrDoneBtn: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var UploadPhotoBtn: UIButton!
    
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var uploadProfileImageBtn: UIButton!
    
    var imagePicker = UIImagePickerController()
    //Default is false = not editable
    var isEditable = false
    //flag to detect if change photo or not
    var isPhotoChanged = false
    var base64String:String?
    var userData = AccountManager.shared().userData
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
 
    override func viewWillAppear(_ animated: Bool) {
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        loadUserImage()
    }
    func setUpView() {
        // customize the navigation bar
        self.navigationItem.hidesBackButton = true
        self.historyBtn.layer.cornerRadius = 3.0
        self.historyBtn.layer.masksToBounds = true
        self.changePasswordBtn.layer.cornerRadius = 3.0
        self.changePasswordBtn.layer.masksToBounds = true
        self.uploadProfileImageBtn.isHidden = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "LeftChevron"), style: UIBarButtonItemStyle.done, target: self, action: #selector(backOrCancelBtnTapped(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "editIcon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(editOrDoneBtnTapped(_:)))
        if let userAccountData = userData{
            nameTxt.text = userAccountData.name
            phoneTxt.text = userAccountData.phone ?? "None"
            if userAccountData.isLoggedFromSocial {
                changePasswordBtn.isHidden = true
            }
        }
    }
    func loadUserImage(){
        if let showUserData = userData{
            imageView.sd_setImage(with: URL(string: showUserData.photo!), placeholderImage: UIImage(named: "Personimage"))
        }
    }
    @IBAction func backOrCancelBtnTapped(_ sender: UIBarButtonItem) {
        if isEditable{
            //cancel case
            setUpView()
            moveToShowMood()
            loadUserImage()
            isEditable = false
        }else{
            //back case
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func editOrDoneBtnTapped(_ sender: UIBarButtonItem) {
        if isEditable{
            //save & move to show mood
            updateProfilRequest()
            moveToShowMood()
            isEditable = false
            isPhotoChanged = false
        }else{
            moveToEditMood()
            isEditable = true
        }
    }
    func updateProfilRequest(){
        if InternetConnection.connected() {
            //show hud
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
            spinnerActivity.isUserInteractionEnabled = false;
            
            updateProfil()
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func updateProfil() {
        
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        
        guard let name = nameTxt.text, !name.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyName, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let phone = phoneTxt.text, !phone.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyPhoneNumber, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        var parameters = ["user_id" : "\(userID)", "name": name, "phone":phone] as [String : Any]


        if isPhotoChanged{
            parameters["photo"] = base64String
        }
        
        
        UserService.updateProfile(Constants.UpdateProfileApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // show alert with success request.
                if let updateProfileResponse = response as? UpdateProfileResponse {
                    self.userData?.photo = updateProfileResponse.data
                    
                }
                self.userData?.name = self.nameTxt.text
                self.userData?.phone = self.phoneTxt.text
                //hide hud
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true);

                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ProfileUpdateSuccessAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }
        })
    }

    func moveToEditMood(){
        self.uploadProfileImageBtn.isHidden = false
        nameTxt.isUserInteractionEnabled = true
        phoneTxt.isUserInteractionEnabled = true
        UploadPhotoBtn.isUserInteractionEnabled = true
        self.navigationItem.leftBarButtonItem?.image = UIImage(named: "WrongIcon")
        self.navigationItem.rightBarButtonItem?.image = UIImage(named: "correctIcon")
    }
    func moveToShowMood(){
        nameTxt.isUserInteractionEnabled = false
        phoneTxt.isUserInteractionEnabled = false
        UploadPhotoBtn.isUserInteractionEnabled = false
        self.navigationItem.leftBarButtonItem?.image = UIImage(named: "backNav")
        self.navigationItem.rightBarButtonItem?.image = UIImage(named: "editIcon")

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadPhotoBtnTapped(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    @IBAction func changePasswordTapped(_ sender: UIButton) {
        let destination = self.storyboard!.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
        destination?.contentSizeInPopup = CGSize(width: self.view.bounds.width-40, height: 350)
        let popupController = STPopupController.init(rootViewController: destination!)
        //popupController.containerView.layer.cornerRadius = 5.0
        popupController.containerView.layer.cornerRadius = 5.0
        popupController.containerView.layer.masksToBounds = true
        popupController.navigationBarHidden = true
        popupController.present(in: self)
    }
    
}

extension UserProfileVC: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        var selectedImageFromPicker: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImageFromPicker = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        dismiss(animated: true, completion: nil)
        if let selectedImage = selectedImageFromPicker
        {
            imageView.image = selectedImage
            let imageData = UIImagePNGRepresentation(imageView.image!)
            base64String = imageData?.base64EncodedString(options: .endLineWithLineFeed)
            isPhotoChanged = true
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
}
