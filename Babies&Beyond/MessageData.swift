//
//  MessageData.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class MessageData: Codable {
    let id : Int?
    let from_id : Int?
    let to_id : Int?
    let message : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case from_id = "from_id"
        case to_id = "to_id"
        case message = "message"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
}
