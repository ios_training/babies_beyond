//
//  Group.swift
//  BabiesChat
//
//  Created by M.I.Kamashany on 1/15/18.
//  Copyright © 2018 NtamTech. All rights reserved.
//

import UIKit

class Group: Codable {
    
    var id:String?
    var name:String?
    var description:String?
    var photo:String?
    var status:String?
    var created_by:String?
    var approved_by:String?
    var created_at:String?
    var updated_at:String?
    var is_member:Bool?
    var created_by_name:String?
    var user_status:String?
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case photo
        case status
        case created_by
        case approved_by
        case created_at
        case updated_at
        case is_member
        case created_by_name
        case user_status
    }
}
