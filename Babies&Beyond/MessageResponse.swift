//
//  MessageResponse.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
class MessageResponse: Codable {
    let status : Bool?
    let message : String?
    let data : [MessageData]?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
}
