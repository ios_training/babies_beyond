//
//  MidwifeTimeSlotTableViewCell.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/25/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeTimeSlotTableViewCell: UITableViewCell {

    @IBOutlet weak var startTimeLabel:UILabel!
    @IBOutlet weak var endTimeLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
