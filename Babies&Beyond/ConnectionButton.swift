//
//  ConnectionButton.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/28/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit

class ConnectionButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setTitle("Check your Internet Connection!", for: .normal)
        self.setTitleColor(UIColor.gray, for: .normal)
        self.contentHorizontalAlignment = .center
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
