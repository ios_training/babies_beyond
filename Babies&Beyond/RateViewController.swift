//
//  RateViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/18/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

class RateViewController: UIViewController {
 
    @IBOutlet weak var starRatingView: SwiftyStarRatingView!
    @IBOutlet weak var submitBtn: EMSpinnerButton!
    @IBOutlet weak var commentTxt: UITextField!
    
    var serviceID : String?
    var rateValue :CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createRateStars()
        self.submitBtn.cornerRadius = 5.0
        self.submitBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.submitBtn.titleColor = UIColor(r: 82, g: 87, b: 106)

        print(serviceID)
    }
    @IBAction func starRatingValueChanged(_ sender: SwiftyStarRatingView) {
        starRatingViewValueChange()
    }
    func starRatingViewValueChange() {
        print("Value : \(starRatingView.value)")
        rateValue = starRatingView.value
    }
    func createRateStars()
    {
        starRatingView.tintColor = #colorLiteral(red: 0.3173025846, green: 0.3451647162, blue: 0.415482372, alpha: 1)
        starRatingView.allowsHalfStars = false 	//default is true
        starRatingView.accurateHalfStars = false //default is true
        starRatingView.continuous = true        //default is true
        starRatingView.emptyStarImage = UIImage(named: "empty.png")
        starRatingView.filledStarImage = UIImage(named: "filled.png")
        starRatingView.value = 0
    }

    
    @IBAction func submitBtnTapped(_ sender: UIButton){
        guard let userID = AccountManager.shared().staffData?.id else {//.nurseLoginResponse?.data?.user_data?.id else{
            return
        }
        
        guard let comment = commentTxt.text, !comment.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyComment, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let rate = rateValue, rate > 0 else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.RateValue, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }

        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.submitBtn.animate(animation: .collapse)
            rateRequest(userID: userID,serviceID:serviceID!, comment:comment, rate:rate)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
        
    }
    
    func rateRequest(userID: Int,serviceID:String, comment:String, rate:CGFloat) {
        let parameters = ["staff_id" : "\(userID)", "service_id": serviceID, "comment": comment, "rate":rate] as [String : Any]
        NurseService.rateCommentRequest(Constants.StaffRateCommentApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                // show alert with err message
                self.submitBtn.animate(animation: .expand)

                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // show alert with success request.
                if let requestResult =  response as? CommentRateResponse{
                    if requestResult.status!{
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.submitBtn.animate(animation: .expand)

                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
    }
    
    
}

