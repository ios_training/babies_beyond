//
//  SelectedHistoryItemViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/17/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import STPopup

class SelectedHistoryItemViewController: UIViewController {

    fileprivate let cellId = "timeSlotCell"
    
    @IBOutlet weak var commentLabel:UILabel!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var locationLabel:UILabel!
    @IBOutlet weak var rateBtn:UIButton!
    @IBOutlet weak var feesLabel:UILabel!
    @IBOutlet weak var dateTitleLabel:UILabel!
    @IBOutlet weak var fromDateLabel:UILabel!
    @IBOutlet weak var toDateLabel:UILabel!
    @IBOutlet weak var timeSlotsTableview:UITableView!
    
    fileprivate var days:[String:[MidwifeTime]]?
    var selectedService:FinishedService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if selectedService.type == 3 {
            dateTitleLabel.isHidden = true
            fromDateLabel.isHidden = true
            toDateLabel.isHidden = true
            timeSlotsTableview.isHidden = false
            timeSlotsTableview.register(UINib(nibName: "MidwifeTimeSlotTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
            timeSlotsTableview.separatorStyle = .none
            if let availabelTimeSlotsArr = selectedService.dates {
                if availabelTimeSlotsArr.count > 0 {
                    days = groupTimeSlotsPerDay(availableTimes: availabelTimeSlotsArr)
                }
            }
        }else{
            dateTitleLabel.isHidden = false
            timeSlotsTableview.isHidden = true
            if selectedService.type == 4 || selectedService.type == 5 {
                fromDateLabel.isHidden = false
                toDateLabel.isHidden = false
                fromDateLabel.text = "From: \((selectedService.dates?.first?.from)!)"
                toDateLabel.text = "To: \((selectedService.dates?.first?.to)!)"
            }else{
                fromDateLabel.isHidden = false
                toDateLabel.isHidden = true
                fromDateLabel.text = "\((selectedService.dates?.first?.from)!)"
            }
        }
        
        initViews()
    }
    
    private func initViews(){
        switch selectedService.type {
            case 0:
                self.title = "Event"
                break
            case 1:
                self.title = "Workshop"
                break
            case 3:
                self.title = "Midwife"
                break
            case 4:
                self.title = "Nurse"
                break
            case 5:
                self.title = "Babysitter"
                break
            default: break
        }
        
        nameLabel.text = selectedService?.name
        if let comment = selectedService.comment {
            if comment.isEmpty {
                commentLabel.text = "None"
            }else{
                commentLabel.text = selectedService.comment 
            }
        }else{
            commentLabel.text = "None"
        }
        
        locationLabel.text = selectedService?.location
//        feesLabel.text = selectedService.comment
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let userRate = selectedService.rate {
            setRateOfService(rate: userRate)
        }else{
            rateBtn.setTitle("Press to enter your rate", for: .normal)
            rateBtn.isUserInteractionEnabled = true
        }
    }
    
    private func setRateOfService(rate:String){
        switch rate {
            case "1":
                rateBtn.setTitle("Very Bad", for: .normal)
                rateBtn.isUserInteractionEnabled = false
                break
            case "2":
                rateBtn.setTitle("Bad", for: .normal)
                rateBtn.isUserInteractionEnabled = false
                break
            case "3":
                rateBtn.setTitle("Good", for: .normal)
                rateBtn.isUserInteractionEnabled = false
                break
            case "4":
                rateBtn.setTitle("Very Good", for: .normal)
                rateBtn.isUserInteractionEnabled = false
                break
            case "5":
                rateBtn.setTitle("Excellent", for: .normal)
                rateBtn.isUserInteractionEnabled = false
                break
            default:
                rateBtn.setTitle("Press to enter your rate", for: .normal)
                rateBtn.isUserInteractionEnabled = true
                break
        }
    }
    
    private func groupTimeSlotsPerDay(availableTimes : [MidwifeTime]) -> [String:[MidwifeTime]] {
        let groupedDays = Dictionary(grouping: availableTimes) { (availableTime) -> String in
           //return availableTime.day!
            return availableTime.date!

        }
        return groupedDays
    }
    
    @IBAction func rateBtnTapped(_ sender:Any){
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "RateSelectedHistoyItemViewController") as! RateSelectedHistoyItemViewController
        destination.selectedService = selectedService
        destination.contentSizeInPopup = CGSize(width: UIScreen.main.bounds.size.width - 50, height: 260)
        let popupController = STPopupController.init(rootViewController: destination)
        popupController.navigationBarHidden = true
        popupController.containerView.layer.cornerRadius = 5.0
        popupController.present(in: self)
    }
}

extension SelectedHistoryItemViewController:UITableViewDelegate,UITableViewDataSource {
        
    func numberOfSections(in tableView: UITableView) -> Int {
        if let allDays = days {
            return allDays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allDays = days {
            let dayNameKey = allDays.keys.sorted()[section]
            if let midwifeTimes = allDays[dayNameKey] {
                return midwifeTimes.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MidwifeTimeSlotTableViewCell
        let dayNameKey = days!.keys.sorted()[indexPath.section]
        if let midwifeTimes = days![dayNameKey] {
            let endHourAsStringArr = midwifeTimes[indexPath.row].to!.components(separatedBy: ":")
            var endHourString = "\(endHourAsStringArr[0]):\(endHourAsStringArr[1])"
            
            let startHourAsStringArr = midwifeTimes[indexPath.row].from!.components(separatedBy: ":")
            var startHourString = "\(startHourAsStringArr[0]):\(startHourAsStringArr[1])"
            
            //convert startHour from 24 format to 12 format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            if let fromHourIn24Format = dateFormatter.date(from: startHourString) {
                dateFormatter.dateFormat = "h:mm a"
                startHourString = dateFormatter.string(from: fromHourIn24Format)
                cell.startTimeLabel.text = "From \(startHourString)"
            }else{
                cell.startTimeLabel.text = "From \(startHourString)"
            }
            
            //convert endHour from 24 format to 12 format
            dateFormatter.dateFormat = "HH:mm"
            if let endHourIn24Format = dateFormatter.date(from: endHourString) {
                dateFormatter.dateFormat = "h:mm a"
                endHourString = dateFormatter.string(from: endHourIn24Format)
                cell.endTimeLabel.text = "To \(endHourString)"
            }else{
                cell.endTimeLabel.text = " To \(endHourString)"
            }
            
        }
        cell.selectionStyle = .none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let allDays = days {
            return allDays.keys.sorted()[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let screenWidth = UIScreen.main.bounds.size.width
        let headerFrame = CGRect(x: 0, y: 0, width: screenWidth, height: 40)
        let headerView = UIView(frame: headerFrame)
        headerView.backgroundColor = UIColor(r: 82, g: 87, b: 106)
        
        let dayNameFrame = CGRect(x: 10, y: 0, width: screenWidth-10, height: 40)
        let dayNameLabel = UILabel(frame: dayNameFrame)
        let font = UIFont(name: "MuseoSans-300", size: 17)
        dayNameLabel.font = font
        dayNameLabel.textColor = UIColor.white
        
        headerView.addSubview(dayNameLabel)
        if let allDays = days {
            dayNameLabel.text = allDays.keys.sorted()[section]
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
