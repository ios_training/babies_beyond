//
//  MidwifeTableViewCell.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/24/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
