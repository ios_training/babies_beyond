//
//  MessagesTableViewCell.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MessagesTableViewCell: UITableViewCell {

    @IBOutlet weak var messageFromMe: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var theMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
