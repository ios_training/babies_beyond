//
//  NurseService.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//


import UIKit
import Alamofire
//import ReachabilitySwift
import Reachability
class NurseService: NSObject {


    static let NurseUserToken = "Bearer \((AccountManager.shared().staffData?.user_token)!)"
    //"Bearer \((AccountManager.shared().nurseLoginResponse?.data?.user_data?.user_token)!)"
    static let notificationToken = defaults.string(forKey: "notificationToken")
    
    // login method used with user and nurse
    static func login(_ apiName: String,  params:[String : Any]? ,completion:((Any?,String? ,Error?) -> ())?){
        
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
                case .success:
                    let jsonDecoder = JSONDecoder()
                    do {
                        if let JSON = response.result.value as? NSDictionary{
                            if let status = JSON["status"] as? Bool {
                                if status {
                                    if let obj = JSON["data"] as? NSDictionary{
                                        if let userData = obj["user_data"] as? NSDictionary{
                                            let typeIDI = userData["user_type_id"] as? Int
                                            let typeID = String(typeIDI ?? 0)
                                            if typeID == Constants.UserTypeID{
                                                let loginResponse = try jsonDecoder.decode(LoginResponse.self, from: response.data!)
                                                completion?(loginResponse,typeID, nil)
                                                debugPrint("Alamofire Response . resposne :- ", response)
                                            }else{
                                                let nurseloginResponse = try jsonDecoder.decode(NurseLoginResponse.self, from: response.data!)
                                                completion?(nurseloginResponse,typeID, nil)
                                                debugPrint("Alamofire Response . resposne :- ", response)
                                            }
                                        }
                                    }
                                }else{
                                    let loginResponse = try jsonDecoder.decode(LoginResponse.self, from: response.data!)
                                    loginResponse.message = Constants.NoUser
                                    completion?(loginResponse,nil,nil)
                                }
                            }
                        }
                    } catch let jsonErr {
                        debugPrint("Error serializing json:", jsonErr)
                        completion?(nil,nil, response.error)
                    }
                
                case .failure:
                    completion?(nil,nil, response.error)
                    debugPrint("Alamofire Failure :- ",response.error?.localizedDescription)
                }
            }.responseString(){ res in
                print(res)
        }
    }

    static func rateCommentRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":NurseUserToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let commentRateResponse = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    debugPrint("Alamofire Response . resposne :- ", response)
                    completion?(commentRateResponse, nil)
                } catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
                debugPrint("Alamofire Failure :- ",response.error)
                
            }
        }
    }

    static func logoutRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let logoutResponse = try jsonDecoder.decode(LogoutResponse.self, from: response.data!)
                    completion?(logoutResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    
    
    static func GetStaffInboxMessagesRequest(_ apiName: String,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":NurseUserToken]
        
        Alamofire.request(url!, method:.get,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let StaffMessageResponse = try jsonDecoder.decode(MessageResponse.self, from: response.data!)
                    debugPrint("Alamofire Response . resposne :- ", response)
                    completion?(StaffMessageResponse, nil)
                } catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
                debugPrint("Alamofire Failure :- ",response.error)
                
            }
        }
    }
    
    
    static func SendInboxMessagesRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":NurseUserToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let MessageResponse = try jsonDecoder.decode(SendMessageResponse.self, from: response.data!)
                    debugPrint("Alamofire Response . resposne :- ", response)
                    completion?(MessageResponse, nil)
                } catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
                debugPrint("Alamofire Failure :- ",response.error)
                
            }
        }
    }
    
    static func getSchedule(staffID:Int, completion:((Any?, String?) -> ())?){
        let urlString = Constants.BASE_URL + Constants.StaffScheduleApi;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":NurseUserToken]
        let params = ["staff_id":staffID] as [String:Any]
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let staffScheduleObj = try jsonDecoder.decode(StaffScheduleResponse.self, from: response.data!)
                    if let status = staffScheduleObj.status {
                        if status {
                            completion?(staffScheduleObj, nil)
                        }else {
                            if let message = staffScheduleObj.message {
                                completion?(nil,message)
                            }else{
                                completion?(nil,Constants.FailureAlert)
                            }
                        }
                    }else{
                        if let message = staffScheduleObj.message {
                            completion?(nil,message)
                        }else{
                            completion?(nil,Constants.FailureAlert)
                        }
                    }
                } catch let jsonErr {
                    completion?(nil,jsonErr.localizedDescription)
                }
            case .failure:
                completion?(nil, response.error?.localizedDescription)
            }
        }
    }
    static func loadAllStaffTasks(completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + Constants.StaffPaidServicesApi;
        let url = URL(string: urlString);
        let headers = ["Authorization":NurseUserToken]
        
        Alamofire.request(url!, method:.post, parameters: nil,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let StaffPaidServicesResponse = try jsonDecoder.decode(StaffPaidServices.self, from: response.data!)
                    completion?(StaffPaidServicesResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
}
