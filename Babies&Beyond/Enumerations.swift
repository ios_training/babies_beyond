//
//  Enumerations.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 1/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation


enum NotificationType:String {
    case Service = "0"
    case Workshop = "1"
    case Event = "2"
    case Group = "3"
    case Midwife = "4"
}

enum WorkshopNotificationType:String {
    case Cancelled = "0"
    case ConfirmationWithoutPayment = "1"
}

enum EventNotificationType:String {
    case Deleted = "0"
}

enum ServiceNotificationType:String {
    case Cancelled = "0"
    case ConfirmationWithoutPayment = "1"
}

enum MidwifeNotificationType:String {
    case Cancelled = "3"
    case ConfirmationWithoutPayment = "2"
}

enum GroupNotificationAction:String {
    case GroupApproved = "0"
    case GroupRejected = "1"
    case MemberJoined = "2"
    case MemberRejected = "3"
    case MemberRemoved = "4"
    case GroupDeleted = "5"
}


enum GroupStatus: String {
    case pending = "0"
    case approved = "1"
}


enum ServiceType {
    case nurse
    case babySitter
    case midWife
}


enum AccountTypeID:String {
    case User = "1"
    case Nurse = "3"
    case BabySitter = "4"
    case MideWife = "5"
}

enum GroupUserStatus : String {
    case Pending = "Pending"
    case InGroup = "In Group"
    case NotInGroup = "Not in Group"
}

enum ESRefreshType {
    case wechat
}
enum AccountActivated:Int {
    case DeactiveAccount = 0
    case ActiveAccount = 1
}
enum LoginType:String {
    case gmail = "gmail"
}

enum HistoryServiceType: Int {
    case Event = 0
    case Workshop = 1
    case Midwife = 3
    case Nurse = 4
    case Babysitter = 5
}
