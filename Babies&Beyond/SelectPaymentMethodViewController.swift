//
//  SelectPaymentMethodViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/19/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import Alamofire

class SelectPaymentMethodViewController: UIViewController {
    // by default cash on delivery is the selected payment method unless the use change it to 1 values
    var selectedPaymentIndex = 0
    let cellId = "paymentCell"
    
    var isService:Bool?
    var selectedId:String?
    var isMidwife:Bool?
    var price = 5
    
    @IBOutlet weak var submitBtn:EMSpinnerButton!
    @IBOutlet weak var tableview:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select Payment Method"
        self.submitBtn.cornerRadius = 5.0
        self.submitBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        tableview.register(UINib(nibName: "PaymentMethodTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       self.tabBarController?.tabBar.isHidden = false
    }
}


extension SelectPaymentMethodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! PaymentMethodTableViewCell
        cell.paymentImageView.image = Constants.paymentsImages[indexPath.row]
        if indexPath.row == 0 {
            cell.paymentTitleLabel.text = Constants.cachOnDeliveryTitle
        }else{
            cell.paymentTitleLabel.text = Constants.onlinePaymentTitle
        }
        
        if selectedPaymentIndex == indexPath.row {
            cell.selectionStatusImageView.image = Constants.selectedPaymentIcon
        }else{
            cell.selectionStatusImageView.image = Constants.notSelectedPAymentIcon
        }
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPaymentIndex = indexPath.row
        self.tableview.reloadData()
    }
    
    @IBAction func submit(_ sender:Any){
        if InternetConnection.connected() {
            pay()
        }else{
             Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    private func pay(){
        guard let userId = AccountManager.shared().userData?.id else {
            return
        }
        if selectedPaymentIndex == 0 {
            self.submitBtn.animate(animation: .collapse)
            UserService.pay(isMidwife:isMidwife! ,isService: isService!, id: selectedId!, userId: userId, paymentMethod: 1, completion: { (response, err) in
                if let _ = err {
                    // show alert with err message
                    self.submitBtn.animate(animation: .expand)
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    // response
                    if let response = response as? CommentRateResponse {
                        if response.status!{
                            if self.isService! {
                                var count = 0
                                for service in AccountManager.shared().allServices! {
                                    if let midwifeServiceObj = service as? MidwifeService, midwifeServiceObj.id == self.selectedId {
                                        midwifeServiceObj.serviceStatus = "Cash On Delivery"
                                        AccountManager.shared().allServices![count] = midwifeServiceObj
                                    }else if let anotherService = service as? NurseOrBabysitterService, anotherService.id == self.selectedId {
                                        anotherService.service_workshop_status_name = "Cash On Delivery"
                                        AccountManager.shared().allServices![count] = anotherService
                                    }
                                    count += 1
                                }
                            }else{
                                if let i = AccountManager.shared().allWorkshops?.index(where:{$0.id == self.selectedId!}) {
                                    AccountManager.shared().allWorkshops![i].service_workshop_status_name = "Cash On Delivery"
                                }
                            }
                            self.tabBarController?.tabBar.isHidden = false
                            self.navigationController?.popToRootViewController(animated: true)
                        }else{
                            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                        }
                    }
                }
            })
        }else {
            self.submitBtn.animate(animation: .collapse)
            proceedPayfort()
        }
    }
    
    private func payedOnlide(){
        guard let userId = AccountManager.shared().userData?.id else {
            return
        }
        UserService.pay(isMidwife:isMidwife! ,isService: isService!, id: selectedId!, userId: userId, paymentMethod: 2, completion: { (response, err) in
            if let _ = err {
                // show alert with err message
                self.submitBtn.animate(animation: .expand)
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let response = response as? CommentRateResponse {
                    if response.status!{
                        if self.isService! {
                            var count = 0
                            for service in AccountManager.shared().allServices! {
                                if let midwifeServiceObj = service as? MidwifeService, midwifeServiceObj.id == self.selectedId {
                                    midwifeServiceObj.serviceStatus = "Payment Online"
                                    AccountManager.shared().allServices![count] = midwifeServiceObj
                                }else if let anotherService = service as? NurseOrBabysitterService, anotherService.id == self.selectedId {
                                    anotherService.service_workshop_status_name = "Payment Online"
                                    AccountManager.shared().allServices![count] = anotherService
                                }
                                count += 1
                            }
                        }else{
                            if let i = AccountManager.shared().allWorkshops?.index(where:{$0.id == self.selectedId!}) {
                                AccountManager.shared().allWorkshops![i].service_workshop_status_name = "Payment Online"
                            }
                        }
                        self.tabBarController?.tabBar.isHidden = false
                        self.navigationController?.popToRootViewController(animated: true)
                    }else{
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
    }
    
    
    func proceedPayfort() {
    
        guard let payFortController = PayFortController(enviroment: KPayFortEnviromentSandBox) else { return }
        let uuid = payFortController.getUDID()
        let payfort = PayFortCredintials.development(udid: uuid!)
        let parametrs = payfort.parameters
        let url = payfort.mainURL
        let header = payfort.headers
        
        
        
        Alamofire.request(url, method:.post, parameters: parametrs,encoding: JSONEncoding.default, headers:header).responseJSON { response in
            switch response.result {
                
            case .success:
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    let token = JSON["sdk_token"]!
                    //let ref = JSON["merchant_reference"]!
                    print("token \(token)")
                    //print("ref \(ref)")
                    self.ShowPayfort(token: token as! String)
                }
            case .failure:
                Helper.showAlert(title: Constants.errorAlertTitle, message: response.error?.localizedDescription ?? "", controller: self, okBtnTitle: Constants.OKTItle)

                
               // completion?(nil, response.error)
            }
            }
  
    }
    

    func ShowPayfort( token sdkToken: String) {
        guard let payFortController = PayFortController(enviroment: KPayFortEnviromentSandBox) else { return }

        //let user = UserManager.shared.currentUserInfo
        let request = NSMutableDictionary()
        // Payfort api :Remember - Before sending the amount value of any transaction
        // you have to multiply the value with the currency decimal code according to ISO code 3.
        let updatedAmount: Float = Float(price * 100)

        request.setValue(updatedAmount, forKey: "amount")
        request.setValue("PURCHASE", forKey: "command")//PURCHASE - AUTHORIZATION
        request.setValue("AED", forKey: "currency")
        request.setValue(AccountManager.shared().userData?.email ?? "" , forKey: "customer_email")
        request.setValue("en", forKey: "language")
        request.setValue("NWupHCkh", forKey: "merchant_reference")
        request.setValue(sdkToken, forKey: "sdk_token")

        
        payFortController.callPayFort(withRequest: request,
                               currentViewController: self,
                               success: { (_, response) in
                                self.payedOnlide()

        }, canceled: { (request, response) in
            //                self.showError(sub: response[""])
        }, faild: { (requst, response, message) in
            //self.showError(sub: message)
            
            Helper.showAlert(title: Constants.errorAlertTitle, message: message  ?? "", controller: self, okBtnTitle: Constants.OKTItle)
        })
    }
}
