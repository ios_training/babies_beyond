//
//  WorkshopVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/26/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import ESPullToRefresh
import Toast_Swift

class WorkshopVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    let cellID = "eventCell"
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchDragInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
        createNavBarItems(titleString: "Workshops")
        // NotificationCenter observers
        observeChanges()
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self?.isRefresh = true
                    self?.allWorkshopsRequest()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.view.makeToast("Check your Internet Connection!", duration: 3.0, position: .center)
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
        
    }
    
    // MARK: ViewController life cycle
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        setUpView()

    }
    
    // MARK: NotificationCenter
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(workshopsChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Workshop.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Workshop.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    @objc func workshopsChanged(_ notification: NSNotification) {
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        guard let workshopId = notification.userInfo?[Constants.Id] as? String else {
            return
        }
        
        if let i = AccountManager.shared().allWorkshops?.index(where:{$0.id == workshopId}) {
            switch action {
            case WorkshopNotificationType.Cancelled.rawValue:
                AccountManager.shared().allWorkshops?[i].service_workshop_status_name = ""
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            case WorkshopNotificationType.ConfirmationWithoutPayment.rawValue:
                AccountManager.shared().allWorkshops?[i].service_workshop_status_name = "Confirm Without Payment"
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            default: break
            }
        }
    }
    
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
        //setUpView()
    }
    
    func updateUI() {
        if AccountManager.shared().allWorkshops == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    
    fileprivate func setUpView() {
        if let _ = AccountManager.shared().allWorkshops{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
//            userServices = workshops
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            allWorkshopsRequest()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Something went wrong, Please try again.", for: .normal)
        }
    }
    private func refresh() {
        self.allWorkshopsRequest()
    }

    func allWorkshopsRequest() {
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        let parameters = ["user_id":userID] as [String:Any]
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        UserService.getAllWorkshops(Constants.AllWorkshopsApi, params: parameters, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    self.connectionBtn?.setTitle("Something went wrong, press to try again.", for: .normal)
                }else{
                    print("mafesh neeeeeet")
                    self.tableView.es.stopPullToRefresh()
                }
            }else{
                // response
                if let responseResult = response as? AllWorkshopsResponse {
                    AccountManager.shared().allWorkshops = responseResult.data
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                }
                self.connectionBtn?.isHidden = true
            }
        })
    }


}
extension WorkshopVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allWorkShops = AccountManager.shared().allWorkshops {
            return allWorkShops.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier:cellID ) as! EventTableViewCell
        let obj = AccountManager.shared().allWorkshops![indexPath.row]
        cell.eventNameLbl.text = obj.name
        if let startDate = obj.start_date{
            cell.dateFromLbl.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
        }
        if let startTime = obj.start_date{
            cell.timeFromLbl.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
        }
        if let endDate = obj.end_date{
            cell.dateToLbl.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
        }
        if let endTime = obj.end_date{
            cell.timeToLbl.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
        }
        
        if let statusName = obj.service_workshop_status_name {
            if statusName == ""{
                //cell.statusView.isHidden = true
                cell.eventStatusLbl.isHidden = true
            }else{
                if statusName == "Pending" {
                    cell.eventStatusLbl.backgroundColor = UIColor(r: 228, g: 201, b: 192)
                    cell.eventStatusLbl.text = statusName
                }else if statusName == "Confirm Without Payment" {
                    cell.eventStatusLbl.backgroundColor = UIColor(r: 194, g: 189, b: 186)
                    cell.eventStatusLbl.text = "Ask for payment"
                }else{
                    cell.eventStatusLbl.backgroundColor = UIColor(r: 82, g: 87, b: 106)
                    cell.eventStatusLbl.text = statusName
                }
                cell.eventStatusLbl.isHidden = false
            }
        }
    
        tableView.tableFooterView = UIView()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "WorkshopInfoVC") as! WorkshopInfoVC
        VC.workshopDetails = AccountManager.shared().allWorkshops![indexPath.row]
        VC.itemIndex = indexPath.row
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let obj = AccountManager.shared().allWorkshops![indexPath.row]
        if let statusName = obj.service_workshop_status_name{
            if statusName == ""{
                return 100
            }else{
                return 140
            }
        }
        return 0
    }
}


