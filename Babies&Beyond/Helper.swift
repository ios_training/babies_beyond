//
//  Helper.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Reachability

class Helper: NSObject {

    
    static func showAlert(title:String, message: String, controller : UIViewController, okBtnTitle:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: okBtnTitle, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            // dismiss alert
        })
        alert.addAction(okButton)
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func dismissViewControllerByAlert(title:String, message: String, controller : UIViewController, okBtnTitle:String, completion:(()->())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: okBtnTitle, style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
            // dismiss alert
            completion?()
        })
        alert.addAction(okButton)
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func fullDateConverter(dateString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // This formate is input formated .
        let formateDate = dateFormatter.date(from: dateString)!
        dateFormatter.dateFormat = "dd MMM yyyy h:mm a" // Output Formated
        return dateFormatter.string(from: formateDate)
    }
    
    static func to12DateFormat(dateIn24Format:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let fromDateObj = dateFormatter.date(from: dateIn24Format)
        dateFormatter.dateFormat = "HH:mm a"
        let dateIn12FormatStr = dateFormatter.string(from: fromDateObj!)
        return dateIn12FormatStr
    }

    static func dateConverterWithFormat(dateString: String, isDate: Bool) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // This is date format which comes from the backend.
        let formateDate = dateFormatter.date(from: dateString)!
        // This is the new date format which we need
        if isDate{
            dateFormatter.dateFormat = "dd MMM yyyy"
        }else{
            dateFormatter.dateFormat = "h:mm a"
        }
        
        return dateFormatter.string(from: formateDate)
    }
    
    static func formateGroupDate(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // This is date format which comes from the backend.
        let dateObjFromString = dateFormatter.date(from: date)!
        // This is the new date format which we need
        dateFormatter.dateFormat = "dd MMM"
        return dateFormatter.string(from: dateObjFromString)
    }
    
    static func estimateFromForText(text:String, width:Int) -> CGRect {
        let size = CGSize(width: width, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)]
        
        return NSString(string: text).boundingRect(with: size, options: options, attributes: attributes, context: nil)
    }
    
    static func estimateFromForText(text:String) -> CGRect {
        let size = CGSize(width: 500, height: 20)
        let options = NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
        let attributes = [NSAttributedStringKey.font: UIFont(name: "MuseoSans-300", size: 14)]
        
        return NSString(string: text).boundingRect(with: size, options: options, attributes: attributes, context: nil)
    }
    
    static func cacheUserData(user:User_data) {
        do {
            let cachedData = try JSONEncoder.init().encode(user)
            UserDefaults.standard.set(cachedData, forKey: Constants.LoggedUserKey)
            UserDefaults.standard.synchronize()
        } catch  {
            // error in caching user object
        }
    }
    
    static func cacheStaffData(data:NurseUser_data) {
        do {
            let cachedData = try JSONEncoder.init().encode(data)
            UserDefaults.standard.set(cachedData, forKey: Constants.LoggedUserKey)
            UserDefaults.standard.synchronize()
        } catch  {
            // error in caching user object
        }
    }
}



