//
//  SigninViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/18/17.
//  Copyright © 2017 NTAM. All rights reserved.
//
import UIKit
import PKHUD

class SigninViewController: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var siginBtn:EMSpinnerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.siginBtn.cornerRadius = 5.0
        self.siginBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
    }
    
    @IBAction func signInBtnTapped(_ sender: UIButton) {
        if let inputs = validateSignInInputs() {
            checkInternetAndSignIn(email: inputs.email, password: inputs.password, notifKey: inputs.notificationKey)
        }
    }
    
    // check inputs of the sign in form
    private func validateSignInInputs() -> (email:String, password:String, notificationKey:String)? {
        guard let email = emailTxt.text, !email.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return nil
        }
        
        guard  Validations.isValidEmail(email: email) else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.InvalidEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return nil
        }
        
        guard let password = passwordTxt.text, !password.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyPassword, controller: self, okBtnTitle: Constants.OKTItle)
            return nil
        }
        
        guard let notificationToken = defaults.string(forKey: "notificationToken"), !notificationToken.isEmpty else {
            return nil
        }
        return (email,password,notificationToken)
    }
    
    private func checkInternetAndSignIn(email:String, password:String, notifKey:String) {
        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.siginBtn.animate(animation: .collapse)
            loginRequest(emailTxt: email, passwordTxt: password, notificationToken: notifKey )
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    private func cacheUserData(user:User_data) {
        do {
            let cachedData = try JSONEncoder.init().encode(user)
            UserDefaults.standard.set(cachedData, forKey: Constants.LoggedUserKey)
            UserDefaults.standard.synchronize()
        } catch  {
            // error in caching user object
        }
    }
    
    func loginRequest( emailTxt : String, passwordTxt:String, notificationToken:String ) {
        let parameters = ["email":emailTxt, "password":passwordTxt, "notification_token":notificationToken] as [String:Any]
        NurseService.login(Constants.LoginApi, params: parameters , completion: { (response, userType, error) in
            self.view.isUserInteractionEnabled = true
            if let _ = error {
                self.siginBtn.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                //if login as user
                if userType == Constants.UserTypeID{
                    // if the response isn't nil
                    if let loginResponseResult = response as? LoginResponse {
                        // if status is true
                        if let status = loginResponseResult.status {
                            if status {
                                AccountManager.shared().userData = loginResponseResult.data?.user_data
                                if let allservices = loginResponseResult.data?.home_page?.services {
                                    AccountManager.shared().allServices = allservices
                                }else{
                                    AccountManager.shared().allServices = [AnyObject]()
                                }
                                
                                if let bookedMidwifes = loginResponseResult.data?.home_page?.midwifeServices {
                                    AccountManager.shared().allServices?.append(contentsOf: bookedMidwifes as [AnyObject])
                                }
                                
                                if loginResponseResult.data?.user_data?.isActivate == AccountActivated.ActiveAccount.rawValue{
                                    Helper.cacheUserData(user: AccountManager.shared().userData!)
                                    self.moveToUserScreens()
                                    return
                                }else{
                                    AccountManager.shared().verificationCode = Int((AccountManager.shared().userData?.verificationCode)!)
                                    self.navigateToEmailVerification()
                                    return
                                }
                            }
                            
                        }
                        // if status is false
                        self.siginBtn.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NoUser, controller: self, okBtnTitle: Constants.OKTItle)
                    }else{
                        // if the response is nil
                        self.siginBtn.animate(animation: .collapse)
                    }
                }else if userType == Constants.NurseTypeID || userType == Constants.MidwifeTypeID || userType == Constants.BabysitterTypeID {
                    //if login as Nurse/babysitter/Midwife
                    // if the response isn't nil
                    if let loginResponseResult = response as? NurseLoginResponse {
                        // if status is true
                        if let status = loginResponseResult.status {
                            if status {
                                AccountManager.shared().staffData = loginResponseResult.data?.user_data
                                Helper.cacheStaffData(data: AccountManager.shared().staffData!)

                                self.moveToNurseScreens()
                                return
                            }
                        }
                        // if status is false
                        self.siginBtn.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NoUser, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    // if the response is nil
                    self.siginBtn.animate(animation: .expand)
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NoUser, controller: self, okBtnTitle: Constants.OKTItle)
                }
            }
        })
    }
}

extension SigninViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            // navigate the focus to the password textfield
            passwordTxt.becomeFirstResponder()
        }else {
            // hide the keyboard
            self.view.endEditing(true)
            if let inputs = validateSignInInputs() {
                checkInternetAndSignIn(email: inputs.email, password: inputs.password, notifKey: inputs.notificationKey)
            }
        }
        return true
    }
}
