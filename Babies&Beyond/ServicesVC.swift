//
//  ServicesVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/26/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import STPopup
import ESPullToRefresh
import Toast_Swift
import Instructions

class ServicesVC: UIViewController , CoachMarksControllerDataSource, CoachMarksControllerDelegate{
    
    let NurseOrBabysitterCellID = "serviceCell"
    let midwifeCellID = "midwifeCell"
    
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    @IBOutlet weak var addServiceActionButton: JJFloatingActionButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    
    var userServices = [AnyObject]()
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    
    let coachMarksController = CoachMarksController()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(UserDefaults.standard.bool(forKey: Constants.ShowInstructions))
        if (UserDefaults.standard.bool(forKey: Constants.ShowInstructions)) == false{
            
            self.coachMarksController.start(on: self)
            
        }else{
            
        }
        //        self.coachMarksController.start(on: self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.coachMarksController.stop(immediately: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coachMarksController.dataSource = self
        
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        tableView.register(UINib(nibName: "ServiceTableViewCell", bundle: nil), forCellReuseIdentifier: NurseOrBabysitterCellID)
        tableView.register(UINib(nibName: "MidwifeServiceTableViewCell", bundle: nil), forCellReuseIdentifier: midwifeCellID)
        setupAddServiceActionButton()
        
        // NotificationCenter observers
        observeChanges()
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    print("ana fi addPullToRefresh")
                    self?.loadAllServices()
                }
            }else{
                print("ana fi el noooooo Internet")
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.view.makeToast("Check your Internet Connection!", duration: 3.0, position: .center)
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
    }
    
    // MARK: NotificationCenter
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(servicesChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Service.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Service.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    @objc func servicesChanged(_ notification: NSNotification) {
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        guard let serviceId = notification.userInfo?[Constants.Id] as? String else {
            return
        }
        
        var count = 0
        if let qq = AccountManager.shared().allServices as? [NurseOrBabysitterService] {
            if let i = qq.index(where:{$0 as! String == serviceId}) {
                print(i)
                
            }
        }
        
        for service in AccountManager.shared().allServices! {
            //count += 1
            
            switch action {
            case ServiceNotificationType.Cancelled.rawValue:
                if let service = service as? NurseOrBabysitterService {
                    AccountManager.shared().allServices?.remove(at: count)
                    self.tableView.deleteRows(at: [IndexPath(row: count, section: 0)], with: UITableViewRowAnimation.automatic)
                }
                break
            case ServiceNotificationType.ConfirmationWithoutPayment.rawValue:
                if let anotherServiceObj = service as? NurseOrBabysitterService, anotherServiceObj.id == serviceId {
                    let price = notification.userInfo?[Constants.price] as? String
                    let name = notification.userInfo?[Constants.staffName] as? String
                    anotherServiceObj.service_workshop_status_name = "Confirm Without Payment"
                    anotherServiceObj.staff_name = name
                    anotherServiceObj.price = price
                    AccountManager.shared().allServices![count] = anotherServiceObj
                    self.tableView.reloadRows(at: [IndexPath(row: count, section: 0)], with: UITableViewRowAnimation.automatic)
                }
                break
            case MidwifeNotificationType.Cancelled.rawValue:
                if let service = service as? MidwifeService {
                    AccountManager.shared().allServices?.remove(at: count)
                    self.tableView.deleteRows(at: [IndexPath(row: count, section: 0)], with: UITableViewRowAnimation.automatic)
                }
                break
            case MidwifeNotificationType.ConfirmationWithoutPayment.rawValue:
                if let midwifeServiceObj = service as? MidwifeService, midwifeServiceObj.id == serviceId {
                    let price = notification.userInfo?[Constants.price] as? String
                    let name = notification.userInfo?[Constants.staffName] as? String
                    midwifeServiceObj.serviceStatus = "Confirm Without Payment"
                    AccountManager.shared().allServices![count] = midwifeServiceObj
                    self.tableView.reloadRows(at: [IndexPath(row: count, section: 0)], with: UITableViewRowAnimation.automatic)
                    
                }
                break
            default: break
            }
            count += 1
        }
        ///////
    }
    
    
    private func refresh() {
        self.loadAllServices()
    }
    
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        createNavBarItems(titleString: "Services")
        setUpView()
    }
    
    
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    private func setupAddServiceActionButton() {
        addServiceActionButton.buttonColor = UIColor(r: 222, g: 198, b: 189)

//        addServiceActionButton.itemTitleFont = UIFont.systemFont(ofSize: 17, weight: .medium)
//        addServiceActionButton.itemTitleColor = UIColor.white
        addServiceActionButton.addItem(title: "     Request Nurse Service     ", image: #imageLiteral(resourceName: "newNurse")) { item in
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "RequestNurseServiceViewController") as! RequestNurseServiceViewController
            destination.newServiceType = ServiceType.nurse
            self.tabBarController?.navigationController?.pushViewController(destination, animated: true)
        }
        addServiceActionButton.addItem(title: "   Request Midewife Service  ", image: #imageLiteral(resourceName: "newMidwife")) { item in
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "MidwifeListVC") as! MidwifeListVC
            self.tabBarController?.navigationController?.pushViewController(destination, animated: true)
        }
        addServiceActionButton.addItem(title: " Request BabySitter Service  ", image: #imageLiteral(resourceName: "newBabySitter")) { item in
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "UserSendRequestVC") as! UserSendRequestVC
            destination.newServiceType = ServiceType.babySitter
            self.tabBarController?.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    fileprivate func setUpView() {
        if let services = AccountManager.shared().allServices{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            userServices = services
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            loadAllServices()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    func updateUI() {
        if AccountManager.shared().allServices == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    
    private func loadAllServices() {
        guard let userEmail = AccountManager.shared().userData?.email else{
            return
        }
        
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        UserService.getServices(email: userEmail) { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    self.connectionBtn?.setTitle("Something went wrong, press to try again.", for: .normal)
                }else{
                    self.tableView.es.stopPullToRefresh()
                }
            }else{
                // response
                if let responseResult = response as? AllServicesResponse {
                    
                    if let allservices = responseResult.data?.services {
                        AccountManager.shared().allServices = allservices
                        self.userServices = AccountManager.shared().allServices!
                    }else{
                        AccountManager.shared().allServices = [AnyObject]()
                    }
                    
                    if let bookedMidwifes = responseResult.data?.midwifeServices {
                        AccountManager.shared().allServices?.append(contentsOf: bookedMidwifes as [AnyObject])
                        self.userServices = AccountManager.shared().allServices!
                    }
                    
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                    
                }
            }
        }
    }
    
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, arrowOrientation: coachMark.arrowOrientation)
        
        let leftViewText = "Tap Here to Show Your Profile"
        let rightViewText = "Tap Here to Show Notification"
        let addServiceActionButtonText = "Tap Here to Add Service"
        let tabBarText = "Here, you can show events, workshops, groups and alot of features"
        let nextButtonText = "Ok!"
        
        
        switch(index) {
        case 0:
            coachViews.bodyView.hintLabel.text = leftViewText
            coachViews.bodyView.nextLabel.text = nextButtonText
        case 1:
            coachViews.bodyView.hintLabel.text = rightViewText
            coachViews.bodyView.nextLabel.text = nextButtonText
        case 2:
            coachViews.bodyView.hintLabel.text = addServiceActionButtonText
            coachViews.bodyView.nextLabel.text = nextButtonText
        case 3:
            UserDefaults.standard.set(true, forKey: Constants.ShowInstructions)
            coachViews.bodyView.hintLabel.text = tabBarText
            coachViews.bodyView.nextLabel.text = nextButtonText
            
        default: break
        }
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkAt index: Int) -> CoachMark {
        
        switch(index) {
        case 0:
            return coachMarksController.helper.makeCoachMark(for: self.leftView)
        case 1:
            return coachMarksController.helper.makeCoachMark(for: self.rightView)
        case 2:
            return coachMarksController.helper.makeCoachMark(for: self.addServiceActionButton)
        case 3:
            UserDefaults.standard.set(true, forKey: Constants.ShowInstructions)
            
            return coachMarksController.helper.makeCoachMark(for: self.tabBarController?.tabBar)
        default:
            return coachMarksController.helper.makeCoachMark()
        }
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return 4
    }
    
}

extension ServicesVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let midwifeService = userServices[indexPath.row] as? MidwifeService {
            let cell = self.tableView.dequeueReusableCell(withIdentifier:midwifeCellID ) as! MidwifeServiceTableViewCell
            cell.midwifeNameLabel.text = midwifeService.name
            if let statusName = midwifeService.serviceStatus {
                if statusName == "Pending" {
                    cell.statusLabel.backgroundColor = UIColor(r: 228, g: 201, b: 192)
                    cell.statusLabel.text = statusName
                }else if statusName == "Confirm Without Payment" {
                    cell.statusLabel.backgroundColor = UIColor(r: 194, g: 189, b: 186)
                    cell.statusLabel.text = "Ask for payment"
                }else{
                    cell.statusLabel.backgroundColor = UIColor(r: 82, g: 87, b: 106)
                    cell.statusLabel.text = statusName
                }
            }
            tableView.tableFooterView = UIView()
            return cell
        }else {
            let serv = userServices[indexPath.row] as? NurseOrBabysitterService
            let cell = self.tableView.dequeueReusableCell(withIdentifier:NurseOrBabysitterCellID ) as! ServiceTableViewCell
            cell.serviceTypeLbl.text = serv?.service_type_name
            if let startDate = serv?.start_date{
                cell.dateFromLbl.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
            }
            if let startTime = serv?.start_date{
                cell.timeFromLbl.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
            }
            if let endDate = serv?.end_date{
                cell.dateToLbl.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
            }
            if let endTime = serv?.end_date{
                cell.timeToLbl.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
            }
            
            if let statusName = serv?.service_workshop_status_name {
                if statusName == "Pending" {
                    cell.serviceStatusLbl.backgroundColor = UIColor(r: 228, g: 201, b: 192)
                    cell.serviceStatusLbl.text = statusName
                }else if statusName == "Confirm Without Payment" {
                    cell.serviceStatusLbl.backgroundColor = UIColor(r: 194, g: 189, b: 186)
                    cell.serviceStatusLbl.text = "Ask for payment"
                }else{
                    cell.serviceStatusLbl.backgroundColor = UIColor(r: 82, g: 87, b: 106)
                    cell.serviceStatusLbl.text = statusName
                }
            }
            tableView.tableFooterView = UIView()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let midwifeService = userServices[indexPath.row] as? MidwifeService {
            // TODO: show midwife info
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "MidwifeServiceDetailsViewController") as! MidwifeServiceDetailsViewController
            destination.selectedMidwifeService = midwifeService
            destination.itemIndex = indexPath.row
            self.navigationController?.pushViewController(destination, animated: true)
        }else {
            let serv = userServices[indexPath.row] as? NurseOrBabysitterService
            let VC = storyboard?.instantiateViewController(withIdentifier: "ServiceShowPopupVC") as! ServiceInfoVC
            VC.serviceDetails = serv
            VC.itemIndex = indexPath.row
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _ = userServices[indexPath.row] as? MidwifeService {
            return 102
        }else {
            return 150
        }
    }
}

