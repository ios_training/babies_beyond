//
//  ScheduleTableViewCell.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/20/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import STPopup

class ScheduleTableViewCell: UITableViewCell{

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet weak var completeOrRateBtn: UIButton!
    var delegate:ScheduleTableViewCellDelegate?
    //var cellIndex:Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        completeOrRateBtn.layer.cornerRadius = 5.0

    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       }
    
    @IBAction func completeRequest(_ sender: UIButton) {
        self.delegate?.showRatePopup(Tag: sender.tag)

    }
}

protocol ScheduleTableViewCellDelegate{
    func showRatePopup(Tag : Int)
}



