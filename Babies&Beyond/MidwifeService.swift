//
//  MidwifeService.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/9/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeService: Codable {
    var id : String?
    var name : String?
    var email : String?
    var phone : String?
    var photo:String?
    var availableTimes : [MidwifeTime]?
    var serviceStatus:String?
    var pricePerHour:String?
    var bio:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case photo = "photo"
        case availableTimes = "time_slots"
        case pricePerHour = "price_per_hour"
        case serviceStatus = "service_workshop_status_name"
        case bio
    }
}
