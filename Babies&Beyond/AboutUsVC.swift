//
//  AboutUsVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/26/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import WebKit

class AboutUsVC: UIViewController {

    
    @IBOutlet weak var myWebView:UIWebView!
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    var showAboutUsData:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if showAboutUsData {
            self.title = "About Us"
        }else{
            self.title = "Terms & Conditions"
        }
         
        let centerY = self.view.bounds.size.height/2
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        self.view.addSubview(connectionBtn!)
        
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    func updateUI() {
        if AccountManager.shared().aboutUs == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    
    fileprivate func setUpView() {
        
        if showAboutUsData {
            if let aboutData = AccountManager.shared().aboutUs?.data{
                loadingView?.isHidden = true
                connectionBtn?.isHidden = true
                self.myWebView.loadHTMLString(aboutData, baseURL: nil)
            }else if InternetConnection.connected(){
                loadingView?.isHidden = false
                connectionBtn?.isHidden = true
                loadRequest()
            }
            else if reachability.currentReachabilityStatus == .notReachable{
                connectionBtn?.isHidden = false
            }
        }else{
            if let termsData = AccountManager.shared().termesData?.data{
                loadingView?.isHidden = true
                connectionBtn?.isHidden = true
                self.myWebView.loadHTMLString(termsData, baseURL: nil)
            }else if InternetConnection.connected(){
                loadingView?.isHidden = false
                connectionBtn?.isHidden = true
                loadRequest()
            }
            else if reachability.currentReachabilityStatus == .notReachable{
                connectionBtn?.isHidden = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func loadRequest() {
        if showAboutUsData {
            UserService.about(Constants.AboutApi, params: nil, completion: { (response, error) in
                self.loadingView?.isHidden = true
                self.connectionBtn?.isHidden = true
                if let _ = error {
                    // show alert with err message
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    // response
                    if let responseResult = response as? AboutData {
                        AccountManager.shared().aboutUs = responseResult
                        let htmlString:String! = responseResult.data
                        self.myWebView.loadHTMLString(htmlString, baseURL: nil)
                    }else{
                        return
                    }
                }
            })
        }else{
            UserService.loadTermsAndConditions(Constants.TermsApi, params: nil, completion: { (response, error) in
                self.loadingView?.isHidden = true
                self.connectionBtn?.isHidden = true
                if let _ = error {
                    // show alert with err message
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    // response
                    if let responseResult = response as? AboutData {
                        AccountManager.shared().termesData = responseResult
                        let htmlString:String! = responseResult.data
                        self.myWebView.loadHTMLString(htmlString, baseURL: nil)
                    }else{
                        return
                    }
                }
            })
        }
    }
}
