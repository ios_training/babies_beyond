//
//  NurseDoneViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class NurseDoneViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var navBar:UINavigationController?
    var userComingTasks = [ServiceInfoData]()
    var userDoneTasks = [ServiceInfoData]()
    
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        let nib = UINib.init(nibName: "NurseDoneTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "NurseDoneTableViewCell")
        observeChanges()
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    self?.loadAllSchedule()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.view.makeToast("Check your Internet Connection!", duration: 3.0, position: .center)
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
    }
    private func refresh() {
        self.loadAllSchedule()
    }
    private func observeChanges() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    @objc func connectionBtnTapped() {
        updateUI()
    }
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.tabBarController?.tabBar.isHidden = false

        setUpView()
    }
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    fileprivate func setUpView() {
        if let tasks = AccountManager.shared().staffDoneTasks{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            userDoneTasks = tasks
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            loadAllSchedule()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    func updateUI() {
        if AccountManager.shared().staffDoneTasks == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    
    //getSchedule
    private func loadAllSchedule() {
//        guard let staffID = AccountManager.shared().staffData?.id else{
//            return
//        }
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        NurseService.loadAllStaffTasks(){ (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    self.connectionBtn?.setTitle("Something went wrong, press to try again.", for: .normal)
                }else{
                    self.tableView.es.stopPullToRefresh()
                }
            }else{
                // response
                if let responseResult = response as? StaffPaidServices {
                    if let allTasks = responseResult.data {
                        AccountManager.shared().staffSchedule = allTasks
                        self.userDoneTasks.removeAll()
                        self.userComingTasks.removeAll()
                        for task in allTasks{
                            if task.is_completed!{
                                self.userDoneTasks.append(task)
                            }else{
                                self.userComingTasks.append(task)
                            }
                        }
                        if self.userComingTasks.count > 0{
                            AccountManager.shared().staffComingSchedule = self.userComingTasks
                        }
                        if self.userDoneTasks.count > 0 {
                            AccountManager.shared().staffDoneTasks = self.userDoneTasks
                        }
                    }
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                     self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()

                }else{
                    return
                }
            }
        }
    }


}

extension NurseDoneViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userDoneTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NurseDoneTableViewCell", for: indexPath)  as! NurseDoneTableViewCell
        let obj = userDoneTasks[indexPath.row]

        cell.nurseDoneName.text = obj.user_name
        cell.nusreDoneLocation.text = obj.location
        if let image = obj.photo{
            cell.nurseDoneImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "Personimage"))
        }
//        if let startDate = obj.dates{
//            cell.nurseDoneDate.text = Helper.fullDateConverter(dateString : startDate)
//        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TaskInfoVC") as! TaskInfoVC
        vc.selectedTask = userDoneTasks[indexPath.row]
        vc.isDone = true
        self.hidesBottomBarWhenPushed = true
        self.navBar?.pushViewController(vc, animated: true)
    }
}
