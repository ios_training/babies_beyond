//
//  ServiceShowPopupVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/4/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class ServiceInfoVC: UIViewController {
    
    @IBOutlet weak var serviceTypeLbl: UILabel!
    @IBOutlet weak var serviceNameLbl: UILabel!
    @IBOutlet weak var serviceNameContainerView: UIView!
    @IBOutlet weak var dateFromLbl: UILabel!
    @IBOutlet weak var timeFromLbl: UILabel!
    @IBOutlet weak var dateToLbl: UILabel!
    @IBOutlet weak var timeToLbl: UILabel!
    @IBOutlet weak var serviceStatusLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var feeLbl: UILabel!
    
    @IBOutlet weak var sendRequestContainerViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var payBtn: EMSpinnerButton!
    @IBOutlet weak var cancelBtn: EMSpinnerButton!
    
    var serviceDetails : NurseOrBabysitterService?
    var itemIndex:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Service Information"
        setupView()
    }

    func setupView() {
        self.payBtn.cornerRadius = 5.0
        self.payBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.cancelBtn.cornerRadius = 5.0
        self.cancelBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        if let serviceDetailsObj = serviceDetails{
            serviceTypeLbl.text = serviceDetailsObj.service_type_name
            
            if let startDate = serviceDetailsObj.start_date{
                dateFromLbl.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
            }
            if let startTime = serviceDetailsObj.start_date{
                timeFromLbl.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
            }
            if let endDate = serviceDetailsObj.end_date{
                dateToLbl.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
            }
            if let endTime = serviceDetailsObj.end_date{
                timeToLbl.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
            }
            
            if let service = serviceDetails {
                checkServiceStatus(selectedService: service)
            }
            if let name = serviceDetails?.staff_name, !name.isEmpty {
                serviceNameContainerView.isHidden = false
                serviceNameLbl.text = name
            }else{
                serviceNameContainerView.isHidden = true
            }
            
            locationLbl.text = serviceDetailsObj.location
            if let price = serviceDetails?.price, !price.isEmpty {
                feeLbl.text = "\(price) $"
            }else{
                feeLbl.text = "Not Determined"
            }
        }
    }
    
    fileprivate func checkServiceStatus(selectedService: NurseOrBabysitterService) {
        if let status = selectedService.service_workshop_status_name {
            if status == "Pending" {
//                self.cancelBtn.setTitle("Cancel", for: .normal)
//                serviceStatusLbl.text = "Pending"
//                self.payBtn.isHidden = true
//                
                serviceStatusLbl.text = "Ask for payment"
                self.payBtn.setTitle("Pay", for: .normal)
                self.cancelBtn.setTitle("Cancel", for: .normal)
                sendRequestContainerViewRightConstraint.constant = self.view.bounds.width/2
                
            }else if status == "Confirm Without Payment"{
                serviceStatusLbl.text = "Ask for payment"
                self.payBtn.setTitle("Pay", for: .normal)
                self.cancelBtn.setTitle("Cancel", for: .normal)
                sendRequestContainerViewRightConstraint.constant = self.view.bounds.width/2
            }else{
                serviceStatusLbl.text = status
                self.cancelBtn.isHidden = true
                self.payBtn.isHidden = true
            }
        }
    }

    @IBAction func payBtnTapped(_ sender: UIButton) {
        guard let serviceID = serviceDetails?.id else{
            return
        }
        let paymentViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectPaymentMethodViewController") as! SelectPaymentMethodViewController
        paymentViewController.selectedId = serviceID
        paymentViewController.isService = true
        paymentViewController.isMidwife = false
        self.navigationController?.pushViewController(paymentViewController, animated: true)
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        guard let serviceID = serviceDetails?.id else{
            return
        }
        if InternetConnection.connected() {
            self.cancelServiceRequest(userID: userID, serviceID: serviceID)
        }else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    

    
    func cancelServiceRequest(userID : Int, serviceID:String) {
        self.cancelBtn.animate(animation: .collapse)
        let paramters = ["user_id":userID,"service_id":serviceID] as [String : Any]
        UserService.cancelService(api: Constants.cancelAllServicesExceptMidwife, params:paramters, completion: { (response, error) in
            self.cancelBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let response = response as? CommentRateResponse {
                    if response.status!{
                        AccountManager.shared().allServices!.remove(at: self.itemIndex!)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
    }
}
