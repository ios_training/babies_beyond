//
//  SelelctSigninWayViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/13/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn

class SelelctSigninWayViewController: UIViewController , GIDSignInUIDelegate{

    @IBOutlet weak var facebookActivityLoading:UIActivityIndicatorView!
    @IBOutlet weak var googleActivityLoading:UIActivityIndicatorView!
    
    @IBOutlet weak var facebookBtn:UIButton!
    @IBOutlet weak var googleBtn:UIButton!
    @IBOutlet weak var mailBtn:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(continueLoginByGoogle(_:)), name: NSNotification.Name(rawValue: LoginType.gmail.rawValue), object: nil)
        setupViews()
    }
    
    private func setupViews(){
        self.navigationController?.isNavigationBarHidden = true
        self.facebookActivityLoading.stopAnimating()
        self.googleActivityLoading.stopAnimating()
        
        self.facebookBtn.layer.cornerRadius = 5.0
        self.facebookBtn.layer.masksToBounds = true
        self.googleBtn.layer.cornerRadius = 5.0
        self.googleBtn.layer.masksToBounds = true
        self.mailBtn.layer.cornerRadius = 5.0
        self.mailBtn.layer.masksToBounds = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: LoginType.gmail.rawValue), object: nil)
    }
    
    @objc func continueLoginByGoogle(_ notification: NSNotification) {
        if let userData = notification.userInfo as? [String:Any]{
            let userData = userData["userData"] as? (String,String,String)
            guard let email = userData?.0 , !email.isEmpty else {
                return
            }
            guard let name =  userData?.1 , !name.isEmpty else {
                return
            }
            guard let notificationToken = defaults.string(forKey: "notificationToken"), !notificationToken.isEmpty else {
                return
            }
            if InternetConnection.connected() {
                self.view.isUserInteractionEnabled = false
                self.googleActivityLoading.startAnimating()
                self.socialLoginRequest(email: email, name: name, notificationToken:notificationToken)
            }else{
                // show alert
                self.showErrorAlert(errorMessage: Constants.ConnectionAlert)
            }
        }
    }
    
    @IBAction func loginByFacebook(_ sender: UIButton) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ReadPermission.publicProfile , ReadPermission.email], viewController: self) { (loginResult) in
            self.view.isUserInteractionEnabled = true
            self.googleActivityLoading.stopAnimating()
            switch loginResult {
            case .failed(let error):
                 self.showErrorAlert(errorMessage: error.localizedDescription)
            case .cancelled:
                print("User cancelled login.")
            case .success:
                if InternetConnection.connected() {
                    self.view.isUserInteractionEnabled = false
                    self.facebookActivityLoading.startAnimating()
                    self.getFacebookAccountData()
                }else{
                    self.showErrorAlert(errorMessage: Constants.ConnectionAlert)
                }
            }
        }
    }
    
    func getFacebookAccountData() {
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start { (connection, result, error) in
            if let err = error {
                self.showErrorAlert(errorMessage: err.localizedDescription)
                return
            }
            
            if let resultDic = result as? [String:Any]{
                guard let email = resultDic["email"] as? String, !email.isEmpty else {
                    return
                }
                guard let name =  resultDic["name"] as? String, !name.isEmpty else {
                    return
                }
                guard let notificationToken = defaults.string(forKey: "notificationToken"), !notificationToken.isEmpty else {
                    return
                }
                if InternetConnection.connected() {
                    self.socialLoginRequest(email: email, name: name, notificationToken:notificationToken)
                }else{
                    // show alert
                    self.showErrorAlert(errorMessage: Constants.ConnectionAlert)
                }
            }
        }
    }
    
    func socialLoginRequest(email : String, name: String, notificationToken: String) {
        
        let parameters = ["email":email, "name":name, "notification_token":notificationToken ]  as [String:Any]
        
        UserService.loginBySocial(Constants.SocialLoginApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                // show alert with err message
                self.showErrorAlert(errorMessage: Constants.FailureAlert)
            }else{
                // response
                if let loginResponseResult = response as? LoginResponse {
                    if loginResponseResult.status!{
                        AccountManager.shared().userData = loginResponseResult.data?.user_data
                        AccountManager.shared().userData?.isLoggedFromSocial = true
                        if let nurseOrBabysitterServices = loginResponseResult.data?.home_page?.services {
                            AccountManager.shared().allServices = nurseOrBabysitterServices
                        }else{
                            AccountManager.shared().allServices = [AnyObject]()
                        }
                        if let midwifeServices = loginResponseResult.data?.home_page?.midwifeServices {
                            AccountManager.shared().allServices?.append(contentsOf: midwifeServices as [AnyObject])
                        }
                        Helper.cacheUserData(user: AccountManager.shared().userData!)
                        self.moveToUserScreens()
                    }else{
                        self.showErrorAlert(errorMessage: loginResponseResult.message!)
                    }
                }else{
                    self.showErrorAlert(errorMessage: Constants.FailureAlert)
                }
            }
        })
    }
    
    private func showErrorAlert(errorMessage:String){
        self.view.isUserInteractionEnabled = true
        self.facebookActivityLoading.stopAnimating()
        self.googleActivityLoading.stopAnimating()
        Helper.showAlert(title: Constants.errorAlertTitle, message: errorMessage, controller: self, okBtnTitle: Constants.OKTItle)
    }
    
    
    @IBAction func loginByGoogle(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func loginByNormalWay(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInSignUpPaginationController") as! SignInSignUpPaginationController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func showConditionsAndTerms(_ sender: UIButton) {
        var navBar:UINavigationController?
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        let termsVC = storyboard.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
        termsVC.showAboutUsData = false
        self.navigationController?.pushViewController(termsVC, animated: true)
    }
    
}
