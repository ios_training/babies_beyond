//
//  InternetConnection.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import Reachability

class InternetConnection: NSObject {
    
    static func connected() -> Bool {
        let reachability = Reachability()!
        if reachability.connection == .none {
            debugPrint("Network not reachable")
            return false
           
        } else {
            return true
        }
    }// end of method
    
}
