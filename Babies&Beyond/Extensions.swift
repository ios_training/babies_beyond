//
//  UIViewExt.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/26/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import Foundation
import UIKit
import FTPopOverMenu_Swift

extension UIViewController{
   // var filterDelegate:FilterDelegate?
    func createNavBarItems(titleString : String) {
        navigationItem.title = titleString
        let image1 = UIImage(named: "profilePic")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image1, style: .plain, target: self, action: #selector(handleProfileBtn))
        let image2 = UIImage(named: "notification-icon")
        
        
        //notification Button
        let notificationBtn =  UIBarButtonItem(image: image2, style: .plain, target: self, action: #selector(handleNotificationBtn))
        navigationItem.rightBarButtonItems = [notificationBtn]
    }
    @objc func handleProfileBtn() {
        let profileViewController = storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
//        self.tabBarController?.navigationController?.pushViewController(profileViewController, animated: true)
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    @objc func handleNotificationBtn() {
        let notificationViewController = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationViewController, animated: true)
    }
    
    var menuOptionNameArray : [String] {
        return ["All","Today","This Month"]
    }
    
    @objc func handleFilterBtn() {
    }
    
    func moveToNurseScreens(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "mainTabBar") as! UITabBarController
        self.present(vc, animated: true, completion: nil)
    }
    
    func moveToUserScreens(){
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        self.present(vc!, animated: true, completion: nil)
    }
    
    func navigateToEmailVerification(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmailVerificationVC") as! EmailVerificationVC
        self.present(vc, animated: true, completion: nil)
    }
    
    func moveToLoginScreen(fromLogout:Bool)  {
        var navBar:UINavigationController?
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SelelctSigninWayViewController") as! SelelctSigninWayViewController
        if fromLogout {
            navBar = UINavigationController(rootViewController: vc)
            self.present(navBar!, animated: true, completion: nil)
        }else{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension UIColor {
    convenience init(r:CGFloat, g:CGFloat, b:CGFloat){
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    func loadImageFromCacheWithURLString(urlString:String)  {
        
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            self.image = cachedImage
            return
        }
        
        //otherwise fire off a new download.
        let url = URL(string: urlString)
        URLSession(configuration: URLSessionConfiguration.default).dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                
            }else{
                DispatchQueue.main.async {
                    if let downloadedImage = UIImage(data: data!) {
                        imageCache.setObject(downloadedImage, forKey: urlString as NSString)
                        self.image = UIImage(data: data!)
                    }
                }
            }
        }).resume()
    }
}


extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}






