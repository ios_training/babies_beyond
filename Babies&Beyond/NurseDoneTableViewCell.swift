//
//  NurseDoneTableViewCell.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class NurseDoneTableViewCell: UITableViewCell {

    @IBOutlet weak var nurseDoneImage: UIImageView!
    @IBOutlet weak var nurseDoneDate: UILabel!
    @IBOutlet weak var nurseDoneName: UILabel!
    @IBOutlet weak var nusreDoneLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nurseDoneImage.layer.cornerRadius = nurseDoneImage.frame.size.height/2
        nurseDoneImage.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
