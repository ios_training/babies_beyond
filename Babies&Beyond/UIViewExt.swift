//
//  UIViewExt.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/26/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{

    func createNavBarItems(titleString : String) {
        navigationItem.title = titleString
        let image1 = UIImage(named: "profilePic")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image1, style: .plain, target: self, action: #selector(handleProfileBtn))
        
        let image2 = UIImage(named: "notification-icon")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image2, style: .plain, target: self, action: #selector(handleNotificationBtn))
    }
    @objc func handleProfileBtn() {
        let VC = storyboard?.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        self.navigationController?.pushViewController(VC, animated: true)
      
    }
    @objc func handleNotificationBtn() {
        
        let VC = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
}
