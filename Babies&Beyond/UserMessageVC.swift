//
//  UserMessageVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/25/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
//import ESPullToRefresh
import Toast_Swift

class UserMessageVC: UIViewController {
    
    let cellId = "groupCell"
    var loadingView:LoadingView?
    var groups:[Group]?
    var connectionBtn : ConnectionButton?
    var isBackFromNewGroupScreen = false
    var tabIndex:Int?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var msgSegmentedControl: UISegmentedControl!
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        self.tableView.isHidden = true

        self.tableView.register(UINib(nibName: "GroupTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        createNavBarItems(titleString: "Groups")

       observeChanges()
    }
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {

        msgSegmentedControl.selectedSegmentIndex = 0
        
        self.navigationController?.navigationBar.isHidden = false
        if isBackFromNewGroupScreen {
            isBackFromNewGroupScreen = false
            setUpView()
        }
    }
    
    fileprivate func setUpView() {
        if let groups = AccountManager.shared().allGroups{
            self.groups = groups
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            getAllGroups()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        let size: CGFloat = 13
        let font = UIFont(name: "MuseoSans-300", size: size)
        let attributes = [NSAttributedStringKey.font : font!] as [AnyHashable : Any] ;
        msgSegmentedControl.setTitleTextAttributes(attributes , for: UIControlState.normal)
    }
    
    // MARK: NotificationCenter
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(groupsChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Group.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Group.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    @objc func groupsChanged(_ notification:NSNotification){
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        guard let groupId = notification.userInfo?[Constants.Id] as? String else {
            return
        }
        
        if let i = groups?.index(where:{$0.id == groupId}) {
            let selectedGroup = groups![i]
            switch action {
            case GroupNotificationAction.GroupApproved.rawValue:
                selectedGroup.status = GroupStatus.approved.rawValue
                groups?[i] = selectedGroup
                AccountManager.shared().allGroups?[i] = selectedGroup
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            case GroupNotificationAction.GroupRejected.rawValue:
                groups?.remove(at: i)
                AccountManager.shared().allGroups?.remove(at: i)
                self.tableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            case GroupNotificationAction.GroupDeleted.rawValue:
                groups?.remove(at: i)
                AccountManager.shared().allGroups?.remove(at: i)
                self.tableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            case GroupNotificationAction.MemberJoined.rawValue:
                selectedGroup.user_status = GroupUserStatus.InGroup.rawValue
                groups?[i] = selectedGroup
                AccountManager.shared().allGroups?[i] = selectedGroup
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            case GroupNotificationAction.MemberRemoved.rawValue:
                selectedGroup.user_status = GroupUserStatus.NotInGroup.rawValue
                groups?[i] = selectedGroup
                AccountManager.shared().allGroups?[i] = selectedGroup
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            case GroupNotificationAction.MemberRejected.rawValue:
                selectedGroup.user_status = GroupUserStatus.NotInGroup.rawValue
                groups?[i] = selectedGroup
                AccountManager.shared().allGroups?[i] = selectedGroup
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            default: break
                // no action will be taken
            }
        }
    }
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    func updateUI() {
        if AccountManager.shared().allGroups == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
                connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
            }else{
                setUpView()
            }
        }
    }
    
    private func getAllGroups(){
        if InternetConnection.connected() {
            self.loadingView?.isHidden = false
            self.connectionBtn?.isHidden = true
            UserService.loadAllGroups(completion: { (response, error) in
                self.loadingView?.isHidden = true
                if let _ = error {
                        // show alert with err message
                        self.connectionBtn?.isHidden = false
                        self.connectionBtn?.setTitle("Something went wrong, press to try again.", for: .normal)
                }else{
                    // response
                    if let allGroupsResponse = response as? AllGroups {
                        if allGroupsResponse.status! {
                            AccountManager.shared().allGroups = [Group]()
                            if let allGroups = allGroupsResponse.data {
                                self.groups = self.excludePendingGroupsOfThers(groups:allGroups)
                                AccountManager.shared().allGroups?.append(contentsOf: self.groups!)
                                self.tableView.isHidden = false
                                self.tableView.reloadData()

                            }
                        }
                    }
                    self.connectionBtn?.isHidden = true
                }
            })
        }
    }
    
    @IBAction func filterGroupsSegmentedControlTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            tabIndex = 0
            if let allGroups = AccountManager.shared().allGroups {
                groups = allGroups
                self.tableView.reloadData()
            }
        }else{
            tabIndex = 1
            if let allGroups = AccountManager.shared().allGroups {
                groups = allGroups.filter({ (group:Group) -> Bool in
                    return isThisMyGroup(group: group)
                })
                self.tableView.reloadData()
            }
        }
    }
    
    private func isThisMyGroup(group:Group) -> Bool {
        if let loggedUserId = AccountManager.shared().userData?.id {
            return group.created_by == "\(loggedUserId)"
        }
        return false
    }
    

    private func excludePendingGroupsOfThers(groups:[Group]) -> [Group] {
        let result = groups.filter { (group:Group) -> Bool in
            // exclude groups when the group owner isn't me and pending at the same time.
            if group.created_by! != "\(AccountManager.shared().userData!.id!)" {
                if group.status! == GroupStatus.pending.rawValue {
                    return false
                }
            }
            return true
        }
        return result
    }
}


extension UserMessageVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let groups = self.groups {
            return groups.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellId) as! GroupTableViewCell
        let group = self.groups![indexPath.row]
        cell.setupCell(group: group)
        cell.cellIndex = indexPath
        cell.delegate = self
        cell.selectionStyle = .none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                guard let group = self.groups?[indexPath.row] else{
                    return
                }
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatLogController.selectedGroup = group
        self.navigationController?.pushViewController(chatLogController, animated: true)
        
//        guard let group = self.groups?[indexPath.row] else{
//            return
//        }
//
//        if (group.status! == GroupStatus.approved.rawValue && group.user_status! == GroupUserStatus.InGroup.rawValue) {
//            let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
//            chatLogController.selectedGroup = group
//            self.navigationController?.pushViewController(chatLogController, animated: true)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellId) as! GroupTableViewCell
        let group = self.groups![indexPath.row]
        let width = Int(UIScreen.main.bounds.size.width - 101)
        var moreSpace = 100
        if (group.status! == GroupStatus.approved.rawValue && group.user_status == GroupUserStatus.NotInGroup.rawValue){
            moreSpace = moreSpace - 20
        }
        
        let height = Helper.estimateFromForText(text: group.description!, width: width).height + CGFloat(moreSpace)
        return height
    }
}


extension UserMessageVC: MemberShipDelegate {
    
    func joinGroup(withIndex:IndexPath){
        if InternetConnection.connected() {
            guard let userID = AccountManager.shared().userData?.id else{
                return
            }
            guard let groupId = self.groups?[withIndex.row].id else{
                return
            }
            let parameters = ["user_id" : "\(userID)", "group_id": groupId] as [String : Any]
            UserService.joinGroup(Constants.JoinGroupApi, params: parameters, completion: { (response, error) in
                if let _ = error {
                    // show alert with err message
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    // show alert with success request.
                    if let result = response as? JoinGroupResponse {
                        if result.status! {
                            self.groups![withIndex.row].user_status = GroupUserStatus.Pending.rawValue
                            AccountManager.shared().allGroups?[withIndex.row].user_status = GroupUserStatus.Pending.rawValue
                            self.tableView.reloadRows(at: [withIndex], with: UITableViewRowAnimation.automatic)
                        }else{
                            Helper.showAlert(title: Constants.errorAlertTitle, message: result.message!, controller: self, okBtnTitle: Constants.OKTItle)
                        }
                    }
                }
            })
        }else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func leaveGroup(withIndex:IndexPath){
        if InternetConnection.connected() {
            guard let userID = AccountManager.shared().userData?.id else{
                return
            }
            guard let groupId = self.groups?[withIndex.row].id else{
                return
            }
            let parameters = ["user_id" : "\(userID)", "group_id": groupId] as [String : Any]
            UserService.leaveGroup(Constants.LeaveGroupApi, params: parameters, completion: { (response, error) in
                if let _ = error {
                    // show alert with err message
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    // show alert with success request.
                    if let result = response as? LeaveGroupResponse {
                        if result.status! {
                            self.groups![withIndex.row].user_status = GroupUserStatus.NotInGroup.rawValue
                            AccountManager.shared().allGroups?[withIndex.row].user_status = GroupUserStatus.NotInGroup.rawValue
                            self.tableView.reloadRows(at: [withIndex], with: UITableViewRowAnimation.automatic)
                        }else{
                            Helper.showAlert(title: Constants.errorAlertTitle, message: result.message!, controller: self, okBtnTitle: Constants.OKTItle)
                        }
                    }
                }
            })
        }else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
        
    }
    
    func cancelMembershipInGroup(withIndex:IndexPath){
        // TODO: search about canceling membership of the group
    }
}
