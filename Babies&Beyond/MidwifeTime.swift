//
//  MidwifeTime.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/24/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class MidwifeTime: Codable {
    
    var from:String?
    var to:String?
    var day:String?
    var date:String?
    
    enum CodingKeys: String, CodingKey {
        case from
        case to
        case day
        case date
    }
}
