//
//  ScheduleVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/18/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class ScheduleVC: UIViewController {
    

    @IBOutlet weak var tableView: UITableView!
//    let cellID = "ScheduleTableViewCell"
//    var userTasks = [NurseHome_page]()
//    var loadingView:LoadingView?
//    var connectionBtn : ConnectionButton?
//    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let centerY = self.view.bounds.size.height/2 - 100
//        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
//        loadingView = LoadingView(frame: frame)
//        self.view.addSubview(loadingView!)
//        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
//        connectionBtn = ConnectionButton(frame: framebtn)
//        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
//        self.view.addSubview(connectionBtn!)
//        tableView?.isHidden = true
//        loadingView?.isHidden = true
//        connectionBtn?.isHidden = true
//
//        let nib = UINib.init(nibName: "ScheduleTableViewCell", bundle: nil)
//        self.tableView.register(nib, forCellReuseIdentifier: cellID)
//        // Along with auto layout, these are the keys for enabling variable cell height
//        tableView.estimatedRowHeight = 44.0
//        tableView.rowHeight = UITableViewAutomaticDimension
//
//        // NotificationCenter observers
//        observeChanges()
//    }
//    // MARK: NotificationCenter
//    private func observeChanges() {
//
//        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
//        reachability.startNotifier()
//    }
//
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
//    }
//    @objc func connectionBtnTapped() {
//        updateUI()
//    }
//
//    override func viewDidLayoutSubviews() {
//        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
//    }
//
//
//    override func viewWillAppear(_ animated: Bool) {
////
////        if let tasks = AccountManager.shared().staffSchedule {
////            userTasks = tasks
////            tableView.reloadData()
////        }
//        setUpView()
//    }
//    @objc func reachabilityChanged() {
//        connectionBtn?.isHidden = true
//        updateUI()
//    }
//
//    fileprivate func setUpView() {
//        if let tasks = AccountManager.shared().staffSchedule{
//            loadingView?.isHidden = true
//            connectionBtn?.isHidden = true
//            tableView.isHidden = false
//            userTasks = tasks
//            tableView.reloadData()
//        }else if InternetConnection.connected(){
//            loadingView?.isHidden = false
//            connectionBtn?.isHidden = true
//            tableView.isHidden = true
//            loadAllSchedule()
//        }
//        else if reachability.currentReachabilityStatus == .notReachable{
//            connectionBtn?.isHidden = false
//            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
//        }
//    }
//
//    func updateUI() {
//        if AccountManager.shared().staffSchedule == nil{
//            if reachability.currentReachabilityStatus == .notReachable {
//                connectionBtn?.isHidden = false
//            }else{
//                setUpView()
//            }
//        }
//    }
//
//    @IBAction func logoutBtnTapped(_ sender: UIBarButtonItem) {
//        let alertController = UIAlertController(title: "Are you sure you want to Logout?", message: "", preferredStyle: .alert)
//
//        // Create the actions
//        let okAction = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default) {
//            UIAlertAction in
//            print("OK Pressed")
//            self.logoutRequest()
//        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
//            UIAlertAction in
//            print("Cancel Pressed")
//        }
//        // Add the actions
//        alertController.addAction(okAction)
//        alertController.addAction(cancelAction)
//
//        // Present the controller
//        self.present(alertController, animated: true, completion: nil)
//
//    }
//
//    func logoutRequest() {
//        guard let staffID = AccountManager.shared().staffData?.id else{
//            return
//        }
//        UserDefaults.standard.removeObject(forKey: Constants.LoggedUserKey)
//        UserDefaults.standard.synchronize()
//        AccountManager.shared().clear()
//        let parameters = ["user_id":staffID]  as [String:Any]
//        UserService.logoutRequest(Constants.LogoutApi, params: parameters, completion:nil)
//        self.moveToLoginScreen(fromLogout: true)
//    }
//
//    //getSchedule
//    private func loadAllSchedule() {
//        guard let staffID = AccountManager.shared().staffData?.id else{
//            return
//        }
//        NurseService.getSchedule(staffID: staffID) { (response, error) in
//            self.loadingView?.isHidden = true
//            if let _ = error {
//                // show alert with err message
//                self.connectionBtn?.isHidden = false
//                self.connectionBtn?.setTitle("Something went wrong, Press to try again", for: .normal)
//            }else{
//                // response
//                if let responseResult = response as? StaffScheduleResponse {
//                    if let allTasks = responseResult.data {
//                        AccountManager.shared().staffSchedule = allTasks
//                        self.userTasks = allTasks
//                    }else{
//                        AccountManager.shared().staffSchedule = [NurseHome_page]()
//                    }
//                    self.connectionBtn?.isHidden = true
//                    self.tableView?.isHidden = false
//                    self.tableView.reloadData()
//                }
//            }
//        }
//    }
//
//}
//
//extension ScheduleVC : UITableViewDelegate, UITableViewDataSource, ScheduleTableViewCellDelegate {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return userTasks.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as! ScheduleTableViewCell
//        if userTasks.count > 0 {
//            let item = userTasks[indexPath.row]
//            cell.completeOrRateBtn.tag = indexPath.row
//
//            if let endDate = item.end_date{
//                cell.dateLbl.text = Helper.fullDateConverter(dateString : endDate)
//            }
//            if let name = item.user_name{
//                cell.nameLbl.text = name
//            }
//            if let location = item.location{
//                cell.locationLbl.text = location
//            }
//
//            let isCompleted = item.is_completed
//            if isCompleted == 1 ,let rate = item.rate, !rate.isEmpty    {
//                cell.completeOrRateBtn.isHidden = false
//                cell.completeOrRateBtn.isUserInteractionEnabled = false
//                cell.completeOrRateBtn.setTitle("Completed", for: .normal)
//            }else if isCompleted == 1{
//                cell.completeOrRateBtn.isHidden = false
//                cell.completeOrRateBtn.isUserInteractionEnabled = true
//                cell.completeOrRateBtn.setTitle("Rate", for: .normal)
//            }else{
//                cell.completeOrRateBtn.isHidden = true
//            }
//
//            if let image = item.user_photo{
//                cell.img.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "Personimage"))
//            }
//        }
//
//
//        cell.delegate = self
//        tableView.tableFooterView = UIView()
//        return cell
//    }
//    func showRatePopup(Tag: Int) {
//        let VC = self.storyboard?.instantiateViewController(withIdentifier: "RateViewController") as! RateViewController
//        VC.serviceID = userTasks[Tag].id
//        self.navigationController?.pushViewController(VC, animated: true)
//    }
    
}

