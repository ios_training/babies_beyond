//
//  MainTabBarViewController.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/17/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

//import Foundation
import UIKit
import GoogleSignIn

class MainTabBarViewController: UITabBarController,UINavigationControllerDelegate {
    let cellID = "MoreTableViewCell"
    var tableview:UITableView?
    var itemIconArr = ["ic_articles","aboutIcon","contact-usIcon","terms_ic","LogoutIcon","callus","nurseServiceIcon"]
    var itemNameArr = ["Articles","About Us","Contact Us","Terms & Conditions","Logout","Call Us","Get Service Quotation"]
    
    override func viewDidLoad() {
        delegate = self
//        MainTabBarViewController.customizableViewControllers = nil;
       // MainTabBarViewController
        customizableViewControllers = nil
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
       // self.navigationController?.isNavigationBarHidden = false
       // setUpView()
    }
    
}


extension MainTabBarViewController: UITabBarControllerDelegate, UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemNameArr.count
    }
    

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        if (viewController == tabBarController.moreNavigationController && tabBarController.moreNavigationController.delegate == nil) {
            tableview = self.moreNavigationController.topViewController?.view as? UITableView
            tableview?.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
            //tableview?.separatorInset = UIEdgeInsets.zero
            tableview?.delegate = self
            tableview?.dataSource = self
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.tableview?.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as! MoreTableViewCell
        cell.itemIcon.image = UIImage(named: itemIconArr[indexPath.row])
        cell.itemName.text = itemNameArr[indexPath.row]
        tableview?.tableFooterView = UIView()
        cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        switch row {
        case 0:
            let aboutVC = storyboard?.instantiateViewController(withIdentifier: "AllArticlesViewController") as! AllArticlesViewController
            self.navigationController?.pushViewController(aboutVC, animated: true)
            break
        case 1:
            let aboutVC = storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
            aboutVC.showAboutUsData = true
            self.navigationController?.pushViewController(aboutVC, animated: true)
            break
        case 2:
            let VC = storyboard?.instantiateViewController(withIdentifier: "ContactusViewController") as! ContactusViewController
            self.navigationController?.pushViewController(VC, animated: true)
            break
         case 3:
            let termsVC = storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
            termsVC.showAboutUsData = false
            self.navigationController?.pushViewController(termsVC, animated: true)
            break
        case 4:
            let alertController = UIAlertController(title: "Are you sure you want to Logout?", message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.logoutRequest()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            break
        case 5:
            let phoneNumber = "+971044308900"
            UIApplication.shared.openURL(URL(string: "tel://" + phoneNumber)!)
            break
        case 6:
            let VC = storyboard?.instantiateViewController(withIdentifier: "GetServiceQutationViewController") as! GetServiceQutationViewController
            self.navigationController?.pushViewController(VC, animated: true)
            break
        default:
            break
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
   
    
    func logoutRequest() {
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        
        if AccountManager.shared().userData!.isLoggedFromSocial {
            GIDSignIn.sharedInstance().signOut()
        }
        UserDefaults.standard.removeObject(forKey: Constants.LoggedUserKey)
        UserDefaults.standard.synchronize()
        AccountManager.shared().clear()
        let parameters = ["user_id":userID]  as [String:Any]
        UserService.logoutRequest(Constants.LogoutApi, params: parameters, completion:nil)
        
        self.moveToLoginScreen(fromLogout: true)
    }

}
