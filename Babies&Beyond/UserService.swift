//
//  UserServise.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Reachability
class UserService: NSObject {

    static let userToken = "Bearer \((AccountManager.shared().userData?.user_token)!)"
    static let notificationToken = defaults.string(forKey: "notificationToken")
    
    
    // user registeration
    static func registerUser(_ apiName: String, params: [String : Any]? ,completion: ((Any?, Error?) -> ())?){
        
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
                case .success:
                    let jsonDecoder = JSONDecoder()
                    do {
                       // let registerResponse = try jsonDecoder.decode(RegisterResponse.self, from: response.data!)
                        let registerResponse = try jsonDecoder.decode(LoginResponse.self, from: response.data!)
                        print(registerResponse)

                        completion?(registerResponse, nil)
                    } catch let jsonErr {
                        print(jsonErr)

                        completion?(nil, jsonErr)

                    }
                case .failure:
                    print(response.error)

                    completion?(nil, response.error)
            }
            }.responseString(){ re in
                print(re)
        }
    }
    
    // login Social Media
    static func loginBySocial(_ apiName: String, params: [String : Any]? ,completion: ((Any?, Error?) -> ())?){
        
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let registerResponse = try jsonDecoder.decode(LoginResponse.self, from: response.data!)
                    print(registerResponse)
                    completion?(registerResponse, nil)
                } catch let jsonErr {
                    completion?(nil, jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
            }.responseString(){ res in
                print(res)
        }
    }
    
    static func activateAccountOrSendCode(_ apiName: String, params: [String : Any]? ,completion: ((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let activateAccountResponse = try jsonDecoder.decode(ActivateAccountResponse.self, from: response.data!)
                    completion?(activateAccountResponse, nil)
                } catch let jsonErr {
                    completion?(nil, jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func serviceRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let sendServiceRequestResponse = try jsonDecoder.decode(SendServiceRequestResponse.self, from: response.data!)
                    completion?(sendServiceRequestResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func getServices(email:String, completion:((Any?, String?) -> ())?){
        let urlString = Constants.BASE_URL + Constants.GetServiceApi;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        let params = ["email":email] as [String:Any]
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let allServicesResponseObj = try jsonDecoder.decode(AllServicesResponse.self, from: response.data!)
                    if let status = allServicesResponseObj.status {
                        if status {
                            completion?(allServicesResponseObj, nil)
                        }else {
                            if let message = allServicesResponseObj.message {
                                completion?(nil,message)
                            }else{
                                completion?(nil,Constants.FailureAlert)
                            }
                        }
                    }else{
                        if let message = allServicesResponseObj.message {
                            completion?(nil,message)
                        }else{
                            completion?(nil,Constants.FailureAlert)
                        }
                    }
                } catch let jsonErr {
                    completion?(nil,jsonErr.localizedDescription)
                }
            case .failure:
                completion?(nil, response.error?.localizedDescription)
            }
        }
    }

    static func updateProfile(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let updateProfileResponse = try jsonDecoder.decode(UpdateProfileResponse.self, from: response.data!)
                    completion?(updateProfileResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func changePassword(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let updateProfileResponse = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    completion?(updateProfileResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    // get all events
    static func getAllEvents(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let allEventsResponse = try jsonDecoder.decode(AllEventsResponse.self, from: response.data!)
                    completion?(allEventsResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
            }.responseString(){res in
                print(res)
        }
    }
    
    
    static func getAllWorkshops(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let allWorkshopsResponse = try jsonDecoder.decode(AllWorkshopsResponse.self, from: response.data!)
                    completion?(allWorkshopsResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    

    static func about(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let aboutDataResponse = try jsonDecoder.decode(AboutData.self, from: response.data!)
                    completion?(aboutDataResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    static func loadTermsAndConditions(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
       // let headers = ["Accept":"application/json","Authorization":userToken]
        Alamofire.request(url!, method:.get, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let response = try jsonDecoder.decode(AboutData.self, from: response.data!)
                    completion?(response, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func loadHistory(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let finishedServices = try jsonDecoder.decode(History.self, from: response.data!)
                    completion?(finishedServices, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func isComingEvent(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let isComingResponse = try jsonDecoder.decode(IsComingResponse.self, from: response.data!)
                    completion?(isComingResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func workshopRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let workshopReqestResponse = try jsonDecoder.decode(WorkshopReqestResponse.self, from: response.data!)
                    completion?(workshopReqestResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func logoutRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let logoutResponse = try jsonDecoder.decode(LogoutResponse.self, from: response.data!)
                    completion?(logoutResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)                
            }
        }
    }
    
    static func userNotification(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let notificationResponse = try jsonDecoder.decode(NotificationResponse.self, from: response.data!)
                    completion?(notificationResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func loadAllGroups(completion:((Any?, Error?) -> ())?){
        guard let userEmail = AccountManager.shared().userData?.email else{
            return
        }
        let userToken = "Bearer \((AccountManager.shared().userData?.user_token)!)"
        let headers = ["Accept":"application/json","Authorization":userToken]
        let params = ["email":userEmail] as [String:Any]
        let url = URL(string: "http://ntam.tech/Babies_And_Beyond/api/all_groups")
        Alamofire.request(url!, method: .post , parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let allgroupsResponse = try jsonDecoder.decode(AllGroups.self, from: response.data!)
                    if allgroupsResponse.status! {
                        completion?(allgroupsResponse, nil)
                    }else{
                        // alert
                    }
                } catch let jsonErr {
                    debugPrint("Error serializing json:", jsonErr)
                }
            case .failure(let err):
                completion?(nil,response.error)
            }
        }
    }
    
    
    static func createGroup(parameters:[String:Any], completion:((Any?, Error?) -> ())?) {
        guard let userId = AccountManager.shared().userData?.id else{
            return
        }
        let userToken = "Bearer \((AccountManager.shared().userData?.user_token)!)"
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        let url = URL(string: "http://ntam.tech/Babies_And_Beyond/api/create_group")
        Alamofire.request(url!, method: .post , parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let response = try jsonDecoder.decode(NewGroupResponse.self, from: response.data!)
                    if response.status! {
                        completion?(response, nil)
                    }else{
                        // alert
                    }
                } catch let jsonErr {
                    debugPrint("Error serializing json:", jsonErr)
                }
            case .failure:
                completion?(nil,response.error)
                debugPrint("Alamofire Failure :- ",response.error?.localizedDescription)
            }
        }
    }
    
    static func joinGroup(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let joinGroupResponse = try jsonDecoder.decode(JoinGroupResponse.self, from: response.data!)
                    completion?(joinGroupResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    static func leaveGroup(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let leaveGroupResponse = try jsonDecoder.decode(LeaveGroupResponse.self, from: response.data!)
                    completion?(leaveGroupResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    static func loadAllMidwifes(completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + "midwife/all";
        let url = URL(string: urlString);
        let headers = ["Authorization":userToken]
        
        Alamofire.request(url!, method:.get, parameters: nil,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let allMidwifeResponse = try jsonDecoder.decode(AllMidwifesResponse.self, from: response.data!)
                    completion?(allMidwifeResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func midwifeRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
//                    let midwifeRequestResponse = try jsonDecoder.decode(MidwifeRequestResponse.self, from: response.data!)
//                    print(" response:  \(midwifeRequestResponse)")
//                    completion?(midwifeRequestResponse, nil)
                } catch let jsonErr {
                    print("error: \(jsonErr) " )
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func forgetPassword(email:String,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + "forget_password";
        let url = URL(string: urlString);
//        let headers = ["Accept":"application/json","Authorization":userToken]
        let params = ["email":email] 
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let response = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    completion?(response, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func cancelWorkshop(workshopId:String, userId:Int, completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + "cancel_workshop";
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        let params = ["user_id":userId,"workshop_id":workshopId] as [String : Any]
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let response = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    completion?(response, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    static func cancelService(api:String, params:[String:Any], completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + api
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let response = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    completion?(response, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    /* pay method are being used with service or workshop
        isService true when it's service
        isService false when it's workshop
     */
    static func pay(isMidwife:Bool, isService:Bool, id:String, userId:Int, paymentMethod:Int, completion:((Any?, Error?) -> ())?) {
        var urlString = Constants.BASE_URL ;
        var headers = ["Accept":"application/json","Authorization":userToken]
        var addedParams = ["user_id":userId,"payment_method":paymentMethod] as [String : Any]
        var finalParams = [String:Any]()
        if isService{
            if isMidwife{
                headers = ["Authorization":userToken]
                addedParams = ["unique_key":id] as [String : Any]
                finalParams = ["payment_method" : paymentMethod]
                urlString = urlString + Constants.MidwifePaymentApi
            }else{
                finalParams["service_id"] = id
                urlString = urlString + "service_payment"
            }
        }else{
            finalParams["workshop_id"] = id
            urlString = urlString + "workshop_payment"
        }
        
        let url = URL(string: urlString)
        addedParams.forEach({finalParams[$0] = $1})
        Alamofire.request(url!, method:.post, parameters: finalParams,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let response = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    completion?(response, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }

    static func isMidwifeAppointmentAvailable(midwifeId:Int, fromHour:String, toHour:String, date:String, completion:((Any?, String?) -> ())?){
        let urlString = Constants.BASE_URL + "midwife/check-midwife";
        let url = URL(string: urlString)
        
        let headers = ["Accept":"application/json","Authorization":userToken]
        let params = ["midwife_id":midwifeId,"from":fromHour,"to":toHour,"date":date] as [String : Any]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let response = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    completion?(response, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr.localizedDescription)
                }
            case .failure:
                completion?(nil,Constants.FailureAlert)
            }
        }
    }
    
    static func sendMidwifeRequest(midwifeId:Int, appointments:[MidwifeTime], completion:((Any?, String?) -> ())?){
        let urlString = Constants.BASE_URL + "midwife/reserve";
        let url = URL(string: urlString)
        let reservationData = MidwifeReservationRequestBody()
        reservationData.dates = appointments
        reservationData.midwife_id = midwifeId
        do {
            let  jsonData = try JSONEncoder.init().encode(reservationData)
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            print(jsonString ?? "")
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            request.setValue("application/json", forHTTPHeaderField:"Accept")
            request.setValue(userToken, forHTTPHeaderField: "Authorization")
            request.httpBody = jsonData
            
            Alamofire.request(request).responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                        case .success:
                            let jsonDecoder = JSONDecoder()
                            do {
                                let response = try jsonDecoder.decode(MidwifeReservationResponse.self, from: response.data!)
                                completion?(response, nil)
                            } catch let jsonErr {
                                completion?(nil,jsonErr.localizedDescription)
                            }
                        case .failure:
                            completion?(nil,Constants.FailureAlert)
                    }
            }
        }catch {
            return
        }
    }
    
    static func checkMidwife(_ apiName: String, params: [String : Any]? ,completion: ((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Authorization":userToken]

        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let checkMidwifeResponse = try jsonDecoder.decode(CheckMidwifeResponse.self, from: response.data!)
                    completion?(checkMidwifeResponse, nil)
                } catch let jsonErr {
                    completion?(nil, jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    static func reserveMidwife(_ apiName: String, params: [String : Any]? ,completion: ((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        let jsonData = try? JSONSerialization.data(withJSONObject: params)


        Alamofire.request(url!, method:.post, parameters: params ,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let checkMidwifeResponse = try jsonDecoder.decode(CheckMidwifeResponse.self, from: response.data!)
                    completion?(checkMidwifeResponse, nil)
                } catch let jsonErr {
                    completion?(nil, jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    static func rateService(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        //let headers = ["Authorization" : userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let commentRateResponse = try jsonDecoder.decode(CommentRateResponse.self, from: response.data!)
                    debugPrint("Alamofire Response . resposne :- ", response)
                    completion?(commentRateResponse, nil)
                } catch let jsonErr {
                    print("Error serializing json:", jsonErr)
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
                debugPrint("Alamofire Failure :- ",response.error)
                
            }
        }
    }
    
    
    
    static func contactUsPostMessage(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let contactpostmessage = try jsonDecoder.decode(ContactPostMessageResponse.self, from: response.data!)
                    completion?(contactpostmessage, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    static func loadAllArticles(completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + "all_articles";
        let url = URL(string: urlString);
        
        Alamofire.request(url!, method:.post, parameters: nil,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let articlesResponseObj = try jsonDecoder.decode(ArticlesResponse.self, from: response.data!)
                    completion?(articlesResponseObj, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    static func getServiceQuotationUser(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]

        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let serviceQuotationResponseObj = try jsonDecoder.decode(GetServiceQuotationResponse.self, from: response.data!)
                    completion?(serviceQuotationResponseObj, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
//
//    static func sendNurseServiceRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
//        let urlString = Constants.BASE_URL + apiName
//        let url = URL(string: urlString)
//        let headers = ["Accept":"application/json","Authorization":userToken]
//
//        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
//            switch response.result {
//            case .success:
//                let jsonDecoder = JSONDecoder()
//                do {
//                    let sendNurseServiceResponseObj = try jsonDecoder.decode(NurseServiceResponse.self, from: response.data!)
//                    completion?(sendNurseServiceResponseObj, nil)
//                } catch let jsonErr {
//                    completion?(nil,jsonErr)
//                }
//            case .failure:
//                completion?(nil, response.error)
//            }
//        }
//    }

    
}



