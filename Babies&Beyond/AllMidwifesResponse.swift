//
//  AllMidwifesResponse.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/24/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

struct AllMidwifesResponse : Codable {
    
    let status : Bool?
    let message : String?
    let midwifesArr : [Midwife]?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case midwifesArr = "data"
    }
}
