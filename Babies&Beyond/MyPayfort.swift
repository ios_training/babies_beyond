//
//  MyPayfort.swift
//  Babies&Beyond
//
//  Created by Admin on 7/6/19.
//  Copyright © 2019 NTAM. All rights reserved.
//

import Foundation
import Alamofire


enum PayFortCredintials {
    case development(udid: String)
    case production(udid: String)
    
    var merchantId: String {
        switch self {
        case .development:
            return "NWupHCkh"
        default:
            return ""
        }
    }
    
    var accessCode: String {
        switch self {
        case .development:
            return "lurbWYO7xSy0rp8Fzncd"
        default:
            return ""
        }
    }
    
    
    var shaRequest: String {
        switch self {
        case .development:
            return "TESTSHAIN"
        default:
            return ""
        }
    }
    
    var currency: String { return "AED" }
    
    
    func signature(uid: String) -> String {
        return Encryption.sha256Hex(string: self.preSignature(uid)) ?? "Can't happen."
    }
    
    private func preSignature(_ uid: String) -> String {
        return self.shaRequest + "access_code=\(self.accessCode)" + "device_id=\(uid)" + "language=enmerchant_identifier=\(self.merchantId)" + "service_command=SDK_TOKEN\(self.shaRequest)"
    }
    
    
    
    func parameters(with udid: String) -> [String: Any] {
        return ["access_code": accessCode,
                "device_id": udid,
                "language": "en",
                "merchant_identifier": merchantId,
                "service_command": "SDK_TOKEN",
                "signature": signature(uid: udid)
        ]
    }
}

enum ServerPaths:String {
    case payfortSDKToken = "FortAPI/paymentApi"
}




extension PayFortCredintials {
    var method: HTTPMethod {
        return .post
    }
    
    var mainURL: URL {
        switch self {
        case .development:
            return URL(string: "https://sbpaymentservices.payfort.com/FortAPI/paymentApi")!
        default:
            return URL(string: "")!
        }
    }
    var path: ServerPaths {
        return .payfortSDKToken
    }
    
    var parameters: Parameters? {
        switch self {
        case .development(let udid):
            return parameters(with: udid)
        case .production(let udid):
            return parameters(with: udid)
        }
    }
    
    var headers: HTTPHeaders {
        var headers = HTTPHeaders()
        headers["Content-type"] = "application/x-www-form-urlencoded; charset=utf-8"
        headers["Content-type"] = "application/json"
        headers["Accept"] = "application/json"
        return headers
    }
}

struct Encryption {
    static func sha256Hex(string: String) -> String? {
        guard let messageData = string.data(using: String.Encoding.utf8) else { return nil }
        var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    static func ccSha256(data: Data) -> Data {
        var digest = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digest.withUnsafeMutableBytes { (digestBytes) in
            data.withUnsafeBytes { (stringBytes) in
                CC_SHA256(stringBytes, CC_LONG(data.count), digestBytes)
            }
        }
        return digest
    }
}
struct PayfortResponse: Codable {
    var sdkToken, responseMessage: String?
}
