//
//  ProfileNurseViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleSignIn


class ProfileNurseViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var staffProfileImage: UIImageView!
    @IBOutlet weak var staffName: UILabel!
    @IBOutlet weak var staffPhone: UILabel!
    @IBOutlet weak var staffEmail: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Account"
        staffProfileImage.layer.borderWidth = 1
        staffProfileImage.layer.masksToBounds = false
        staffProfileImage.layer.borderColor = UIColor.white.cgColor
        staffProfileImage.layer.cornerRadius = staffProfileImage.frame.height/2
        staffProfileImage.clipsToBounds = true
        logoutButton.layer.cornerRadius = 5.0
        showData()
    }

    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Are you sure you want to Logout? ", message: "", preferredStyle: .alert)
        //create the actions
        let okButton = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default) { UIAlertAction in
            self.logoOutRequest()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(okButton)
        alertController.addAction(cancelButton)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showData(){
        if let showData = AccountManager.shared().staffData{
            staffName.text = showData.name
            staffEmail.text = showData.email
            staffPhone.text = showData.phone
            if let staffImage = showData.photo {
                staffProfileImage.sd_setImage(with: URL(string:(AccountManager.shared().staffData?.photo)!), placeholderImage: UIImage(named: "Personimage"))
              }
        }
    }
    
    
    
//    override func viewWillDisappear(_ animated: Bool) {
//        var stoaryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = stoaryboard.instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
//        self.popupController()
//        //self.present(vc, animated: true, completion: nil)
//        }
//    }
    
    func logoOutRequest(){
        guard let userID = AccountManager.shared().staffData?.id else{
            return
        }
        UserDefaults.standard.removeObject(forKey: Constants.LoggedUserKey)
        UserDefaults.standard.synchronize()
        AccountManager.shared().clear()
        let parameters = ["user_id":userID]  as [String:Any]
        NurseService.logoutRequest(Constants.LogoutApi, params: parameters, completion:nil)
        
        self.moveToLoginScreen(fromLogout: true)
    }
}
