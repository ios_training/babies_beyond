//
//  AccountManager.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class AccountManager {
    
    // shared Data
    //var nurseLoginResponse:NurseLoginResponse?
    var staffSchedule:[ServiceInfoData]?
    var staffData:NurseUser_data?
    var userData: User_data?
    var userNotfication : NotificationResponse?
    var verificationCode : Int?
    
    var allServices:[AnyObject]?
    var allGroups:[Group]?
    var allEvents:[EventDetails]?
    var allWorkshops:[WorkshopsDetails]?
    var aboutUs: AboutData?
    var termesData: AboutData?
    var historyData = [FinishedService]()
    
    var staffComingSchedule:[ServiceInfoData]?
    var staffDoneTasks:[ServiceInfoData]?
   
    var inboxMessage : [MessageData]?
    var SendMessages : SendMessageResponse?
    var articlesArr:[Article]?
    
    var serviceQuotation:GetServiceQuotationResponse?
    var nurseSendServiceResponse:NurseServiceResponse?
    var nurseSendServiceDesponse:NurseServiseData?


    private static var sharedInstance = AccountManager()
    static func shared() -> AccountManager {
        return sharedInstance
    }
    
    func clear() {
        staffSchedule = nil
        staffData = nil
        //nurseLoginResponse = nil
        userData = nil
        userNotfication = nil
        allServices = nil
        allGroups = nil
        allEvents = nil
        allWorkshops = nil
        verificationCode = nil

        inboxMessage = nil
        SendMessages = nil
        staffComingSchedule = nil
        staffDoneTasks = nil
        serviceQuotation = nil
    }
    
}
