//
//  CalenadarViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/22/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import JBDatePicker

class CalenadarViewController: UIViewController {
    
    @IBOutlet weak var datePickerView: JBDatePickerView!
    
    var dateToSelect: Date!
    
    @IBOutlet weak var monthNameLbl: UILabel!
    
    var datePicker: JBDatePickerView!
    lazy var dateFormatter: DateFormatter = {
        
        var formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .medium
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false;
        createNavBarItems(titleString: "Schedule")
    }
    override func viewDidAppear(_ animated: Bool) {
        let frameForDatePicker = CGRect(x: 0, y: 20, width: view.bounds.width, height: 250)
        datePicker = JBDatePickerView(frame: frameForDatePicker)
        
        datePickerView.delegate = self
    }
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }



}
extension CalenadarViewController:  JBDatePickerViewDelegate  {
    func didSelectDay(_ dayView: JBDatePickerDayView) {

    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        monthNameLbl.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        
        if let date = dateToSelect {
            return date
        }
        else{
            return Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    var firstWeekDay: JBWeekDay {
        return .monday
        
    }
    
    //custom colors
    var colorForWeekDaysViewBackground: UIColor {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    var colorForSelectionCircleForOtherDate: UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
    }
    
    var colorForSelectionCircleForToday: UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    }
    
    var colorForWeekDaysViewText : UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
    }
    var colorForCurrentDay: UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    }
    
    var fontForWeekDaysViewText: JBFont {
        return JBFont(name: "MuseoSans-300", size: .large)
    }
    
    var fontForDayLabel: JBFont {
        return JBFont(name: "MuseoSans-300", size: .medium)
    }


}
