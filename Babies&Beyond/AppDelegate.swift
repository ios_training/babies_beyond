//
//  AppDelegate.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/14/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import FBSDKCoreKit
import GoogleSignIn

internal let kMapsAPIKey = "AIzaSyBFZRpYYIFp6mSt8R6t6lU4_skqGlFVStc"
let defaults = UserDefaults.standard

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let gcmMessageBody = "a_data"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        //Configure API key
        GMSPlacesClient.provideAPIKey(kMapsAPIKey)
        GMSServices.provideAPIKey(kMapsAPIKey)
        
        IQKeyboardManager.shared.enable = true
        UIApplication.shared.statusBarStyle = .lightContent
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationBarAppearace.barTintColor = #colorLiteral(red: 0.323059231, green: 0.3416681886, blue: 0.4140921235, alpha: 1)
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //Firebase
        FirebaseApp.configure()
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        GIDSignIn.sharedInstance().clientID = "358998449243-a6oe91ne8lvunk2nr4737srovm6mp5t4.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        return true
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])

             GIDSignIn.sharedInstance().handle(url,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return handled
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let err = error {
            print("Failed to log into Google: ", err)
            return
        }
        print("Successfully logged into Google", user)
        if let userData = user{
            guard let email = userData.profile.email, !email.isEmpty else {
                return
            }
            guard let name =  userData.profile.name, !name.isEmpty else {
                return
            }
            guard let notificationToken = defaults.string(forKey: "notificationToken"), !notificationToken.isEmpty else {
                return
            }
            if InternetConnection.connected() {
               //self.gmailLoginRequest(email: email, name: name, notificationToken:notificationToken)
               // SelelctSigninWayViewController.gmailLoginRequest(email: email, name: name, notificationToken: notificationToken)
                //socialLoginRequest(email: email, name: name, notificationToken: notificationToken)
                NotificationCenter.default.post(name:  NSNotification.Name(rawValue: LoginType.gmail.rawValue), object: user, userInfo: ["userData":(email,name,notificationToken)])
            }
            
        }
        if let userId = user.userID{
            print("userId", userId)// For client-side use only!
        }
        if let idToken = user.authentication.idToken {
            print("idToken", idToken)// Safe to send to the server
        }
        if let fullName = user.profile.name{
            print("fullName", fullName)
        }
        if let givenName = user.profile.givenName{
            print("givenName", givenName)
        }
        if let familyName = user.profile.familyName{
            print("familyName", familyName)
        }
        if let email = user.profile.email{
            print("email", email)
        }
    }
  
    
    private func getDataFromNotificationBody(data:NSDictionary)  {
        if let notificationBody = data[gcmMessageBody] as? String {
            if let dic = convertToDictionary(text: notificationBody)  {
                if let type = dic[Constants.type] {
                    switch type {
                    case NotificationType.Service.rawValue :
                        updateAllServices(data:dic)
                        break
                    case NotificationType.Event.rawValue :
                        updateAllEvents(data:dic)
                        break
                    case NotificationType.Workshop.rawValue :
                        updateAllWorkshops(data:dic)
                        break
                    case NotificationType.Group.rawValue :
                        updateAllGroups(data:dic)
                        break
                    case NotificationType.Midwife.rawValue :
                        updateAllServices(data:dic)
                        break
                    default: break
                        //
                    }
                }
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        var bgTask: UIBackgroundTaskIdentifier = 0
        bgTask = application.beginBackgroundTask(expirationHandler: {
            application.endBackgroundTask(bgTask)
            bgTask = UIBackgroundTaskInvalid
        })
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }


    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        if AccountManager.shared().userData?.isActivate == AccountActivated.ActiveAccount.rawValue {
            if let userData = AccountManager.shared().userData {
               Helper.cacheUserData(user: userData)
            }
        }
        // caching staff ()
        if let staffData = AccountManager.shared().staffData {
            Helper.cacheStaffData(data: staffData)
        }
    }
    
    // [START receive_message]
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        let userInfo = userInfo as NSDictionary
        getDataFromNotificationBody(data:userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    
//    func application(application: UIApplication,
//                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
//        Messaging.messaging().apnsToken = deviceToken
//    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    func convertToDictionary(text: String) -> [String: String]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK: Notification methods
    
    private func updateAllServices(data:[String:String]){
        if AccountManager.shared().allServices != nil {
            if AccountManager.shared().allServices!.count >= 0 {
                NotificationCenter.default.post(name:  NSNotification.Name(rawValue: NotificationType.Service.rawValue), object: data, userInfo: data)
            }
        }
    }
    
    private func updateAllEvents(data:[String:String]){
        if AccountManager.shared().allEvents != nil {
            if AccountManager.shared().allEvents!.count >= 0 {
                NotificationCenter.default.post(name:  NSNotification.Name(rawValue: NotificationType.Event.rawValue), object: data, userInfo: data)
            }
        }
    }
    
    private func updateAllWorkshops(data:[String:String]){
        if AccountManager.shared().allWorkshops != nil {
            if AccountManager.shared().allWorkshops!.count >= 0 {
                NotificationCenter.default.post(name:  NSNotification.Name(rawValue: NotificationType.Workshop.rawValue), object: data, userInfo: data)
            }
        }
    }
    
    private func updateAllGroups(data:[String:String]){
        if AccountManager.shared().allGroups != nil {
            if AccountManager.shared().allGroups!.count >= 0 {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationType.Group.rawValue), object: nil, userInfo: data)
            }
        }
    }
}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo as NSDictionary
//        getDataFromNotificationBody(data:userInfo)
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        defaults.set(fcmToken, forKey: "notificationToken")
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
