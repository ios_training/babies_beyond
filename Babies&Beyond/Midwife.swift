//
//  Midwife.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/24/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class Midwife: Codable {
    var id : Int?
    var name : String?
    var email : String?
    var phone : String?
    var photo:String?
    var availableTimes : [MidwifeTime]?
    var pricePerHour:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case photo = "photo"
        case availableTimes = "midwife_times"
        case pricePerHour = "price_per_hour"
    }
}
