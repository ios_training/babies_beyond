//
//  GroupTableViewCell.swift
//  BabiesChat
//
//  Created by M.I.Kamashany on 1/16/18.
//  Copyright © 2018 NtamTech. All rights reserved.
//

import UIKit
import SDWebImage

protocol MemberShipDelegate {
    func joinGroup(withIndex:IndexPath)
    func leaveGroup(withIndex:IndexPath)
    func cancelMembershipInGroup(withIndex:IndexPath)
}


class GroupTableViewCell: UITableViewCell {

    @IBOutlet weak var groupImageView:UIImageView!
    @IBOutlet weak var userStatusLabel:UILabel!
    @IBOutlet weak var userStatusIcon: UIImageView!
    @IBOutlet weak var groupCreationDateLabel:UILabel!
    
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var groupDescLabel: UILabel!
    @IBOutlet weak var groupOwnerLabel: UILabel!
    @IBOutlet weak var joinOrLeaveButton: UIButton!
    @IBOutlet weak var joinOrLeaveButtonImageView: UIImageView!
    
    @IBOutlet weak var groupStatusWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var joinOrLeavePopupBtn: UIButton!
    var cellIndex:IndexPath?
   // var delegate:GroupTableViewCellDelegate?
    var delegate:MemberShipDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        joinOrLeavePopupBtn.layer.cornerRadius = 5
        joinOrLeavePopupBtn.clipsToBounds = true
        
        userStatusLabel.layer.cornerRadius = 5//userStatusLabel.frame.size.height/2
        userStatusLabel.clipsToBounds = true
        
        self.groupImageView.layer.cornerRadius = 30
        self.groupImageView.layer.masksToBounds = true
        self.groupImageView.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    @IBAction func joinOrLeavePopupBtn(_ sender: UIButton) {
        joinOrLeavePopupBtn.isHidden = true
        guard let selectedGroup = AccountManager.shared().allGroups?[(self.cellIndex?.row)!] else{
            return
        }
        if selectedGroup.user_status == GroupUserStatus.Pending.rawValue{
           delegate?.cancelMembershipInGroup(withIndex: self.cellIndex!)
        }else if selectedGroup.user_status == GroupUserStatus.InGroup.rawValue{
            delegate?.leaveGroup(withIndex: self.cellIndex!)
        }else if selectedGroup.user_status == GroupUserStatus.NotInGroup.rawValue{
           delegate?.joinGroup(withIndex: self.cellIndex!)
        }
    }
    
    
    @IBAction func joinOrLeave(_ sender: UIButton) {
       // self.delegate?.showJoinOrLeaveActionBtn(btnTag: joinOrLeaveActionBtn.tag)
        if joinOrLeavePopupBtn.isHidden  {
            guard let selectedGroup = AccountManager.shared().allGroups?[(self.cellIndex?.row)!] else{
                return
            }
            joinOrLeavePopupBtn.isHidden = false
            if selectedGroup.user_status == GroupUserStatus.InGroup.rawValue {
                joinOrLeavePopupBtn.setTitle("Leave Group", for: .normal)
                UIButton.animate(withDuration: 0.5, delay: 0.3, options: UIViewAnimationOptions.curveEaseIn, animations: {
                    self.joinOrLeavePopupBtn.isHidden = false
                    self.joinOrLeavePopupBtn.alpha = 1.0
                }, completion: nil)
            }else if selectedGroup.user_status == GroupUserStatus.NotInGroup.rawValue{
                joinOrLeavePopupBtn.setTitle("Join Group", for: .normal)
                UIButton.animate(withDuration: 0.5, delay: 0.3, options: UIViewAnimationOptions.curveEaseIn, animations: {
                    self.joinOrLeavePopupBtn.isHidden = false
                    self.joinOrLeavePopupBtn.alpha = 1.0
                }, completion: nil)
            }
        }else{
            joinOrLeavePopupBtn.isHidden = true
        }
    }
    
    public func setupCell(group:Group){
        joinOrLeavePopupBtn.isHidden = true
        self.groupName.text = group.name
        self.groupDescLabel.text = group.description
        if group.status == GroupStatus.pending.rawValue {
            self.userStatusLabel.text = "    Pending Admin Approval"
            self.userStatusIcon.image = UIImage(named: "pendingAdminAproval")
            self.joinOrLeaveButton.isHidden = true
            self.joinOrLeaveButton.isEnabled = false
            self.joinOrLeaveButtonImageView.isHidden = true
        }else{
            self.userStatusLabel.text = "    Approved"
            self.userStatusIcon.image = UIImage(named: "joinedIcon")
            self.joinOrLeaveButton.isHidden = false
            self.joinOrLeaveButton.isEnabled = true
            self.joinOrLeaveButtonImageView.isHidden = false
        }
        self.groupCreationDateLabel.text = Helper.formateGroupDate(date: group.created_at!)

        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        
        self.groupOwnerLabel.text = group.created_by == "\(userID)" ? "Created by me" : "Created by \(group.created_by_name ?? "")"
    
        if let photoURLString = group.photo {
            self.groupImageView.sd_setImage(with: URL(string: photoURLString), placeholderImage: UIImage(named: "Group-img"), options: SDWebImageOptions.lowPriority, completed: nil)
        }
        
        if group.user_status == GroupUserStatus.Pending.rawValue{
            self.userStatusLabel.text = "    Pending Join"
            self.userStatusIcon.image = UIImage(named: "pendingJoinIcon")
            self.joinOrLeaveButton.isHidden = true
            self.joinOrLeaveButton.isEnabled = false
            self.joinOrLeaveButtonImageView.isHidden = true
        }else if group.user_status == GroupUserStatus.InGroup.rawValue{
            self.userStatusLabel.text = "    Joined"
            self.userStatusIcon.image = UIImage(named: "joinedIcon")
            self.joinOrLeaveButton.isHidden = false
            self.joinOrLeaveButton.isEnabled = true
            self.joinOrLeaveButtonImageView.isHidden = false
        }
        if (group.status! == GroupStatus.approved.rawValue && group.user_status == GroupUserStatus.NotInGroup.rawValue){
            self.userStatusIcon.alpha = 0
            self.userStatusLabel.alpha = 0
        }else{
            self.userStatusIcon.alpha = 1
            self.userStatusLabel.alpha = 1
        }
        self.groupStatusWidthConstraint.constant = Helper.estimateFromForText(text: self.userStatusLabel.text!).size.width + 5
    }
}



