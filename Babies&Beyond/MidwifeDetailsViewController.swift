//
//  MidwifeDetailsViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/25/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import  SDWebImage

class MidwifeDetailsViewController: UIViewController {

    // MARK: views' outlets
    @IBOutlet weak var availableTimeSlotsTableView:UITableView!
    @IBOutlet weak var midwifeNameLabel:UILabel!
    @IBOutlet weak var midwifeImageView:UIImageView!
    @IBOutlet weak var noAvailabelTimesLabel: UILabel!
    @IBOutlet weak var addRequetBtn: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    
    // MARK: private variables
    fileprivate let cellId = "timeSlotCell"
    fileprivate var days:[String:[MidwifeTime]]?
    
    // MARK: public variables
    public var selectedMidwife:Midwife?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        availableTimeSlotsTableView.register(UINib(nibName: "MidwifeTimeSlotTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        availableTimeSlotsTableView.separatorStyle = .none
        if let availabelTimeSlotsArr = selectedMidwife?.availableTimes {
            if availabelTimeSlotsArr.count > 0 {
                days = groupTimeSlotsPerDay(availableTimes: availabelTimeSlotsArr)
                self.availableTimeSlotsTableView.isHidden = false
                self.noAvailabelTimesLabel.isHidden = true
                self.addRequetBtn.isEnabled = true
            }else{
                self.availableTimeSlotsTableView.isHidden = true
                self.noAvailabelTimesLabel.isHidden = false
                self.addRequetBtn.isEnabled = false
            }
        }else{
            self.availableTimeSlotsTableView.isHidden = true
            self.noAvailabelTimesLabel.isHidden = false
            self.addRequetBtn.isEnabled = false
        }
        if let midwife = selectedMidwife{
            midwifeNameLabel.text = midwife.name
            priceLabel.text = "Price per hour: \(midwife.pricePerHour!)$"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let photoURL = selectedMidwife?.photo {
            self.midwifeImageView.sd_setImage(with: URL(string: photoURL), placeholderImage: UIImage(named: "Personimage"), options: SDWebImageOptions.lowPriority, completed: nil)
        }
    }
    
    private func groupTimeSlotsPerDay(availableTimes : [MidwifeTime]) -> [String:[MidwifeTime]] {
        let groupedDays = Dictionary(grouping: availableTimes) { (availableTime) -> String in
            return availableTime.day!
        }
        return groupedDays
    }
    
    @IBAction func backBarButtonTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addMidwifeRequest(_ sender: Any) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "MidwifeAppointmentsViewController") as! MidwifeAppointmentsViewController
        destination.selectedMidwife = selectedMidwife
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}

extension MidwifeDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let allDays = days {
            return allDays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allDays = days {
            let dayNameKey = allDays.keys.sorted()[section]
            if let midwifeTimes = allDays[dayNameKey] {
                return midwifeTimes.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MidwifeTimeSlotTableViewCell
        let dayNameKey = days!.keys.sorted()[indexPath.section]
        if let midwifeTimes = days![dayNameKey] {
            let endHourAsStringArr = midwifeTimes[indexPath.row].to!.components(separatedBy: ":")
            var endHourString = "\(endHourAsStringArr[0]):\(endHourAsStringArr[1])"

            let startHourAsStringArr = midwifeTimes[indexPath.row].from!.components(separatedBy: ":")
            var startHourString = "\(startHourAsStringArr[0]):\(startHourAsStringArr[1])"

            //convert startHour from 24 format to 12 format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            if let fromHourIn24Format = dateFormatter.date(from: startHourString) {
                dateFormatter.dateFormat = "h:mm a"
                startHourString = dateFormatter.string(from: fromHourIn24Format)
                cell.startTimeLabel.text = "From \(startHourString)"
            }else{
                cell.startTimeLabel.text = "From \(startHourString)"
            }
            
            //convert endHour from 24 format to 12 format
            dateFormatter.dateFormat = "HH:mm"
            if let endHourIn24Format = dateFormatter.date(from: endHourString) {
                dateFormatter.dateFormat = "h:mm a"
                endHourString = dateFormatter.string(from: endHourIn24Format)
                cell.endTimeLabel.text = "To \(endHourString)"
            }else{
                cell.endTimeLabel.text = " To \(endHourString)"
            }
            
        }
        cell.selectionStyle = .none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let allDays = days {
            return allDays.keys.sorted()[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let screenWidth = UIScreen.main.bounds.size.width
        let headerFrame = CGRect(x: 0, y: 0, width: screenWidth, height: 40)
        let headerView = UIView(frame: headerFrame)
        headerView.backgroundColor = UIColor(r: 82, g: 87, b: 106)
        
        let dayNameFrame = CGRect(x: 10, y: 0, width: screenWidth-10, height: 40)
        let dayNameLabel = UILabel(frame: dayNameFrame)
        let font = UIFont(name: "MuseoSans-300", size: 17)
        dayNameLabel.font = font
        dayNameLabel.textColor = UIColor.white
        
        headerView.addSubview(dayNameLabel)
        if let allDays = days {
            dayNameLabel.text = allDays.keys.sorted()[section]
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}


