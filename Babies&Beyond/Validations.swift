//
//  Validation.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 1/8/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class Validations: NSObject {
    
    static func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    //check if password characters are equal or more than 6 .
    static func isValidPassword(password:String) -> Bool {
        return password.characters.count >= 6 ? true:false
    }
}
