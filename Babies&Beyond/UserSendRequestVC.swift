//
//  UserSendRequestVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/25/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import STPopup
import PKHUD
import GooglePlacePicker


class UserSendRequestVC: UIViewController {
    
    var newServiceType:ServiceType? {
        didSet{
            if newServiceType == ServiceType.nurse {
                self.title = "Request Nurse"
                self.selectedServiceID = "1"
            }else if newServiceType == ServiceType.babySitter{
                self.title = "Request BabySitter"
                self.selectedServiceID = "2"
            }
        }
    }
    
    var selectedServiceID : String?
    var selectedStartDateTime : String?
    var selectedEndDateTime : String?
    var userData : User_data?
    var isNavigationBarHidden:Bool = true
    var minimumEndDate : Date?
    var address:String?
    var longitude:Double?
    var latitude:Double?
    
    @IBOutlet var viewContainer:UIView!
    @IBOutlet weak var startDateTimeBtn: UIButton!
    @IBOutlet weak var endDateTimeBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var sendBtn:EMSpinnerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendBtn.cornerRadius = 5.0
        self.sendBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.sendBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isNavigationBarHidden {
            self.navigationController?.isNavigationBarHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(locationLblTapped))
        //locationLbl.isUserInteractionEnabled = true
        locationLbl.addGestureRecognizer(tap)
        
        if let selectedStartDateTime = selectedStartDateTime{
            startDateTimeBtn.setTitle(selectedStartDateTime, for: .normal)
        }
        if let selectedEndDateTime = selectedEndDateTime{
            endDateTimeBtn.setTitle(selectedEndDateTime, for: .normal)
        }
    }
    
    @objc func locationLblTapped(sender:UITapGestureRecognizer) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    @IBAction func chooseStartDateTimeTapped(_ sender: UIButton) {
        isNavigationBarHidden = false
        let destination = self.storyboard!.instantiateViewController(withIdentifier: "DatePopupVC") as? DatePopupVC
        destination?.userSendRequestVC = self
        destination?.isStartDate = true
        destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
        let popupController = STPopupController.init(rootViewController: destination!)
        popupController.navigationBarHidden = true
        popupController.present(in: self)
    }
    
    @IBAction func chooseEndDateTimeTapped(_ sender: UIButton) {
        if minimumEndDate == nil{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.StartDateTimeAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }else{
            isNavigationBarHidden = false
            let destination = self.storyboard!.instantiateViewController(withIdentifier: "DatePopupVC") as? DatePopupVC
            destination?.userSendRequestVC = self
            destination?.isStartDate = false
            
            destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
            let popupController = STPopupController.init(rootViewController: destination!)
            
            popupController.navigationBarHidden = true
            popupController.present(in: self)
        }
    }
    
    @IBAction func sendBtnTapped(_ sender: UIButton){
        
        guard let serviceID = selectedServiceID, !serviceID.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ServiceNameAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let location = address,let _ = longitude, let _ = latitude, !location.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.LocationFieldAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let startDateTime = selectedStartDateTime, !startDateTime.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.StartDateTimeAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let endDateTime = selectedEndDateTime, !endDateTime.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EndDateTimeAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        if InternetConnection.connected() {
            sendServiceRequest(  startDateTime: startDateTime,endDateTime:endDateTime, location:location, serviceID:serviceID  )
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func sendServiceRequest(startDateTime: String, endDateTime:String, location:String, serviceID:String ) {
        
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        self.view.isUserInteractionEnabled = false
        self.sendBtn.animate(animation: .collapse)
        let parameters = ["user_id" : "\(userID)", "service_type_id": serviceID, "start_date": startDateTime, "end_date":endDateTime, "location":location,"longitude":longitude!,"latitude":latitude!] as [String : Any]
        UserService.serviceRequest(Constants.ServiceRequestApi, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            self.sendBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // show alert with success request.
                if let requestedService =  response as? SendServiceRequestResponse{
                    if requestedService.status! {
                        AccountManager.shared().allServices?.append(requestedService.data!)
                        //                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.SuccessServiceRequestAlert, controller: self, okBtnTitle: Constants.OKTItle)
                        self.navigationController?.popToRootViewController(animated: true)
                    }else{
                        Helper.showAlert(title: Constants.errorAlertTitle, message: (error?.localizedDescription)!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
    }
}

extension UserSendRequestVC : GMSPlacePickerViewControllerDelegate {
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.viewContainer.isHidden = false
        // self.indicatorView.isHidden = true
        address = place.formattedAddress?.components(separatedBy: ", ").joined(separator: ",")
        longitude = place.coordinate.longitude
        latitude = place.coordinate.latitude
        self.locationLbl.text = address
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

