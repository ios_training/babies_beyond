//
//  UserHistoryVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/21/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit

class UserHistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let cellID = "historyCell"
    var loadingView:LoadingView?
    var finishedServices = [FinishedService]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "History"
        tableView.dataSource = self
        tableView.delegate = self
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        tableView?.isHidden = true
        loadingView?.isHidden = true
        self.tableView.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 99
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if InternetConnection.connected() {
            loadHistoryData()
        }else{
            // show no internet connection message.
             self.finishedServices = AccountManager.shared().historyData
        }
    }
    
    private func loadHistoryData(){
        loadingView?.isHidden = false
        if let userID = AccountManager.shared().userData?.id {
            let params = ["user_id":userID]
            UserService.loadHistory(Constants.AllHistory, params: params, completion: { (response, error) in
                if let error = error {
                    Helper.showAlert(title: Constants.errorAlertTitle, message: error.localizedDescription, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    self.loadingView?.isHidden = true
                    if let historyResponse = response as? History, let historyData = historyResponse.data {
                        self.finishedServices = historyData
                        AccountManager.shared().historyData = historyData
                        if self.finishedServices.count > 0 {
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                        }
                    }
                }
            })
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return finishedServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:cellID ) as! HistoryTableViewCell
        let serv = finishedServices[indexPath.row]
        switch serv.type {
        case 0:
            cell.typeLabel.text = "Event"
            break
        case 1:
            cell.typeLabel.text = "Workshop"
            break
        case 3:
            cell.typeLabel.text = "Midwife"
            break
        case 4:
            cell.typeLabel.text = "Nurse"
            break
        case 5:
            cell.typeLabel.text = "Babysitter"
            break
        default: break
        }
        cell.nameLabel.text = serv.name
        cell.locationLabel.text = serv.location
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "SelectedHistoryItemViewController") as! SelectedHistoryItemViewController
        destination.selectedService = finishedServices[indexPath.row]
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 99
    }
}


