//
//  AllArticlesViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 4/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class AllArticlesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    let cellID = "articleCell"
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    var isBackFromArticleDetails = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let centerY =  UIScreen.main.bounds.height/2 
        var frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: frame)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    self?.isRefresh = true
                    self?.getArticles()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.view.makeToast("Check your Internet Connection!", duration: 1.0, position: .center)
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
    }
    
    // MARK: ViewController life cycle
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !isBackFromArticleDetails {
            self.navigationController?.isNavigationBarHidden = true
        }
        
    }
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    func updateUI() {
        if AccountManager.shared().allWorkshops == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    
    fileprivate func setUpView() {
        if let _ = AccountManager.shared().articlesArr {
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            getArticles()
        }else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    private func refresh() {
        self.getArticles()
    }
    
    func getArticles() {
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        UserService.loadAllArticles(completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    self.connectionBtn?.setTitle("Something went wrong, press to try again.", for: .normal)
                }else{
                    self.tableView.es.stopPullToRefresh()
                }
            }else{
                // response
                if let responseResult = response as? ArticlesResponse {
                    AccountManager.shared().articlesArr = responseResult.data
                    
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                }
                self.connectionBtn?.isHidden = true
            }
        })
    }
    
    @IBAction func back(_ sender:Any){
        isBackFromArticleDetails = false
        self.navigationController?.popViewController(animated: true)
    }
}

extension AllArticlesViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let articles = AccountManager.shared().articlesArr {
            return articles.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier:cellID ) as! ArticleTableViewCell
        let obj = AccountManager.shared().articlesArr![indexPath.row]
        cell.nameLabel.text = obj.title
        if let startDate = obj.createdAt {
            cell.dateLabel.text = Helper.fullDateConverter(dateString:startDate)
        }
        
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isBackFromArticleDetails = true
        let destination = storyboard?.instantiateViewController(withIdentifier: "ArticlesDetailsViewController") as! ArticlesDetailsViewController
        destination.selectedArticle = AccountManager.shared().articlesArr![indexPath.row]
        self.navigationController?.pushViewController(destination, animated: true)
    }

}
