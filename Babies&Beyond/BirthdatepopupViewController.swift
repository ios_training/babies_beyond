//
//  BirthdatepopupViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 6/6/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class BirthdatepopupViewController: UIViewController {

    @IBOutlet weak var birthdatePicker: UIDatePicker!
    
    //var userSendRequestVC : UserSendRequestVC?
    var nurseSendRequestVC : RequestNurseServiceViewController?

    
    var isStarBirthtDate:Bool?
    var isStartDate:Bool?
    var submitStartDate:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let currentDate: Date = Date()
        var components = DateComponents()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        components.calendar = calendar
        components.year = -200
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = 0
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        birthdatePicker.minimumDate = minDate
        birthdatePicker.maximumDate = maxDate
        print(birthdatePicker.minimumDate)
        print( birthdatePicker.maximumDate)
        
        if isStarBirthtDate == true{
            let currentDate: Date = Date()
            var components: DateComponents = DateComponents()
            var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            //components.calendar = calendar
            components.year = -200
            let minDate = Calendar.current.date(byAdding: components, to: Date())
            
            birthdatePicker.minimumDate = minDate
            birthdatePicker.minimumDate = calendar.date(byAdding: components, to: currentDate)
        }else{
            print("errorrrr")
        }
        birthdatePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
    }
    
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let selectDate = dateFormatter.string(from: sender.date)
        if isStarBirthtDate == true{
            nurseSendRequestVC?.minimumbirthDate = sender.date
            nurseSendRequestVC?.selecteddateOfbirth = "\(selectDate)"
            //nurseSendRequestVC?.dateOfBirth.setTitle("\(selectDate)", for: UIControlState.normal)
           
            
        }else{
            nurseSendRequestVC?.selectedEndDateTime = "\(selectDate)"
            print("error")
        }
        
    }
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        datePickerValueChanged(sender: birthdatePicker)
        dismiss(animated: true, completion: nil)
    }

}
