 
 
import Foundation
 
struct History : Codable {
    let status : Bool?
    let message : String?
    let data : [FinishedService]?

    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        case data = "data"
    }
}

