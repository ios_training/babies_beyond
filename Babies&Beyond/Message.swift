//
//  Message.swift
//  ChatLibraryModule
//
//  Created by M.I.Kamashany on 11/28/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

import UIKit
import Firebase

class Message: NSObject {

    var userId:NSNumber?
    var message:String?
    var timestamp:NSNumber?
    var imageWidth:NSNumber?
    var imageHeight:NSNumber?
    var imageURL:String?
    
//    func chatPartnerId() -> String? {
//        return  fromId == Auth.auth().currentUser?.uid ? toId : fromId
//    }
    
    init(dictionary:[String:Any]) {
        super.init()
        userId = dictionary["userId"] as? NSNumber
        message = dictionary["message"] as? String
        timestamp = dictionary["timeStamp"] as? NSNumber
        imageURL = dictionary["imageURL"] as? String
        imageWidth = dictionary["imgWidth"] as? NSNumber
        imageHeight = dictionary["imgHeight"] as? NSNumber
    }
}
