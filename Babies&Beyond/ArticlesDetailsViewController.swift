//
//  ArticlesDetailsViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 4/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class ArticlesDetailsViewController: UIViewController {
    
    var selectedArticle:Article?
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var bodyWebView:UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameLabel.text = selectedArticle?.title
        dateLabel.text = selectedArticle?.createdAt
        if let body = selectedArticle?.body {
            bodyWebView.loadHTMLString(body, baseURL: nil)
        }
        if let startDate = selectedArticle?.createdAt {
            dateLabel.text = Helper.fullDateConverter(dateString:startDate)
        }
    }
}
