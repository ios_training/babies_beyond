//
//  MidwifeServiceDetailsViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/18/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import  SDWebImage


class MidwifeServiceDetailsViewController: UIViewController {

    @IBOutlet weak var availableTimeSlotsTableView:UITableView!
    @IBOutlet weak var midwifeNameLabel:UILabel!
    @IBOutlet weak var midwifeImageView:UIImageView!
    @IBOutlet weak var serviceStatusLbl: UILabel!
    @IBOutlet weak var bioLbl: UILabel!
    
    @IBOutlet weak var payBtn: EMSpinnerButton!
    @IBOutlet weak var cancelBtn: EMSpinnerButton!
    
    @IBOutlet weak var sendRequestContainerViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableviewBottomConstraint: NSLayoutConstraint!
    // MARK: private variables
    fileprivate let cellId = "timeSlotCell"
    fileprivate var days:[String:[MidwifeTime]]?
    
    // MARK: public variables
    public var selectedMidwifeService:MidwifeService?
    var itemIndex:Int?
    
    override func viewDidLoad() {
       super.viewDidLoad()
        self.payBtn.cornerRadius = 5.0
        self.payBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)

        self.cancelBtn.cornerRadius = 5.0
        self.cancelBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        
       availableTimeSlotsTableView.register(UINib(nibName: "MidwifeTimeSlotTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        availableTimeSlotsTableView.separatorStyle = .none
        if let availabelTimeSlotsArr = selectedMidwifeService?.availableTimes {
                days = groupTimeSlotsPerDay(availableTimes: availabelTimeSlotsArr)
        }
        midwifeNameLabel.text = selectedMidwifeService!.name
        serviceStatusLbl.text = selectedMidwifeService!.serviceStatus
        
        bioLbl.text = selectedMidwifeService?.bio ?? "None"
        
        if let photoURL = selectedMidwifeService?.photo {
            self.midwifeImageView.sd_setImage(with: URL(string: photoURL), placeholderImage: UIImage(named: "Personimage"), options: SDWebImageOptions.lowPriority, completed: nil)
        }
        //payBtnView.isHidden = true
        checkServiceStatus(selectedService: selectedMidwifeService!)
    }
    
    fileprivate func checkServiceStatus(selectedService: MidwifeService) {
        if let status = selectedMidwifeService?.serviceStatus {
            if status == "Pending" {
                self.cancelBtn.setTitle("Cancel", for: .normal)
                serviceStatusLbl.text = "Pending"
                self.payBtn.isHidden = true
                self.sendRequestContainerViewRightConstraint.constant = 19
            }else if status == "Confirm Without Payment"{
                serviceStatusLbl.text = "Ask for payment"
                self.payBtn.setTitle("Pay \(calculatePrice())", for: .normal)
                self.cancelBtn.setTitle("Cancel", for: .normal)
                sendRequestContainerViewRightConstraint.constant = self.view.bounds.width/2
                self.payBtn.isHidden = false
            }else{
                serviceStatusLbl.text = status
                self.cancelBtn.isHidden = true
                self.payBtn.isHidden = true
                tableviewBottomConstraint.constant = 0
            }
        }
    }

    private func groupTimeSlotsPerDay(availableTimes : [MidwifeTime]) -> [String:[MidwifeTime]] {
        let groupedDays = Dictionary(grouping: availableTimes) { (availableTime) -> String in
            return availableTime.date!
        }
        return groupedDays
    }
    private func convertDateToDay(date:String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: date){
            formatter.dateFormat  = "EEEE"
            let dayName = formatter.string(from: date)
            return dayName
        }
        return nil
    }
    
    
    private func calculateTotalNumberOfHours(appointmentsArr:[MidwifeTime]) -> Float {
        var result = 0
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        for appointment in appointmentsArr {
            let fromTime = dateFormatter.date(from: appointment.from!)
            let toTime = dateFormatter.date(from: appointment.to!)
            let components = Calendar.current.dateComponents([.hour, .minute], from: fromTime!, to: toTime!)
            result = result + components.hour!
        }
        return Float(result)
    }
    
    private func calculatePrice() -> String {
        let hourPrice = (selectedMidwifeService!.pricePerHour! as NSString).floatValue
        let totalHours = calculateTotalNumberOfHours(appointmentsArr: (selectedMidwifeService?.availableTimes)!)
        return "(\(hourPrice * totalHours)$)"
    }
    

    @IBAction func payBtnTapped(_ sender: UIButton) {
        guard let serviceID = selectedMidwifeService?.id else{
            return
        }
        let paymentViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectPaymentMethodViewController") as! SelectPaymentMethodViewController
        paymentViewController.selectedId = serviceID
        paymentViewController.isService = true
        paymentViewController.isMidwife = true
        self.navigationController?.pushViewController(paymentViewController, animated: true)
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        guard let serviceID = selectedMidwifeService?.id else{
            return
        }
        if InternetConnection.connected() {
            self.cancelServiceRequest(serviceID: serviceID)
        }else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func cancelServiceRequest(serviceID:String) {
        self.cancelBtn.animate(animation: .collapse)
        let paramters = ["unique_key":serviceID] as [String : Any]
        UserService.cancelService(api: Constants.cancelMidwifeSercice, params:paramters, completion: { (response, error) in
            self.cancelBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let response = response as? CommentRateResponse {
                    if response.status!{
                        AccountManager.shared().allServices!.remove(at: self.itemIndex!)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
    }
    
}

extension MidwifeServiceDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let allDays = days {
            return allDays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allDays = days {
            let dayNameKey = allDays.keys.sorted()[section]
            if let midwifeTimes = allDays[dayNameKey] {
                return midwifeTimes.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MidwifeTimeSlotTableViewCell
        let dayNameKey = days!.keys.sorted()[indexPath.section]
        if let midwifeTimes = days![dayNameKey] {
            let endHourAsStringArr = midwifeTimes[indexPath.row].to!.components(separatedBy: ":")
            var endHourString = "\(endHourAsStringArr[0]):\(endHourAsStringArr[1])"
            
            let startHourAsStringArr = midwifeTimes[indexPath.row].from!.components(separatedBy: ":")
            var startHourString = "\(startHourAsStringArr[0]):\(startHourAsStringArr[1])"
            
            //convert startHour from 24 format to 12 format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            if let fromHourIn24Format = dateFormatter.date(from: startHourString) {
                dateFormatter.dateFormat = "h:mm a"
                startHourString = dateFormatter.string(from: fromHourIn24Format)
                cell.startTimeLabel.text = "From \(startHourString)"
            }else{
                cell.startTimeLabel.text = "From \(startHourString)"
            }
            
            //convert endHour from 24 format to 12 format
            dateFormatter.dateFormat = "HH:mm"
            if let endHourIn24Format = dateFormatter.date(from: endHourString) {
                dateFormatter.dateFormat = "h:mm a"
                endHourString = dateFormatter.string(from: endHourIn24Format)
                cell.endTimeLabel.text = "To \(endHourString)"
            }else{
                cell.endTimeLabel.text = " To \(endHourString)"
            }
        }
        
        cell.selectionStyle = .none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let allDays = days {
            return allDays.keys.sorted()[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let screenWidth = UIScreen.main.bounds.size.width
        let headerFrame = CGRect(x: 0, y: 0, width: screenWidth, height: 40)
        let headerView = UIView(frame: headerFrame)
        headerView.backgroundColor = UIColor(r: 82, g: 87, b: 106)
        
        let dayNameFrame = CGRect(x: 10, y: 0, width: screenWidth-10, height: 40)
        let dayNameLabel = UILabel(frame: dayNameFrame)
        let font = UIFont(name: "MuseoSans-300", size: 17)
        dayNameLabel.font = font
        dayNameLabel.textColor = UIColor.white
        
        headerView.addSubview(dayNameLabel)
        if let allDays = days {
            var dayString = convertDateToDay(date:  allDays.keys.sorted()[section])
            dayNameLabel.text = "\(dayString!) (\(allDays.keys.sorted()[section]))"
            
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
