//
//  EventInfoVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/14/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import Toast_Swift
class EventInfoVC: UIViewController {

    
    @IBOutlet weak var EventNameLbl: UILabel!
    @IBOutlet weak var dateFromLbl: UILabel!
    @IBOutlet weak var timeFromLbl: UILabel!
    @IBOutlet weak var dateToLbl: UILabel!
    @IBOutlet weak var timeToLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var speakerName: UILabel!
    @IBOutlet weak var speakerBio: UITextView!
    
    @IBOutlet weak var isComingBtn: EMSpinnerButton!
    var eventDetails : EventDetails?
    var itemIndex:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Event Information"
        self.isComingBtn.cornerRadius = 5.0
        self.isComingBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
       
        self.isComingBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        setupView()
    }
    
    func setupView() {
        if let eventDetailsObj = eventDetails{
            EventNameLbl.text = eventDetailsObj.name
            
            if let startDate = eventDetailsObj.start_date{
                dateFromLbl.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
            }
            if let startTime = eventDetailsObj.start_date{
                timeFromLbl.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
            }
            if let endDate = eventDetailsObj.end_date{
                dateToLbl.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
            }
            if let endTime = eventDetailsObj.end_date{
                timeToLbl.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
            }
            if let isComingEvent = eventDetailsObj.is_comming{
                if isComingEvent{
                    isComingBtn.setTitle("Cancel", for: .normal)
                }else{
                    isComingBtn.setTitle("Going", for: .normal)
                }
            }
            locationLbl.text = eventDetailsObj.location
            speakerName.text = eventDetailsObj.speaker_name
            speakerBio.text = eventDetailsObj.speaker_bio
        }
    }
    
    @IBAction func isComingBtnTapped(_ sender: UIButton) {
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        guard let eventID = eventDetails?.id else{
            return
        }

        var isComming = 0
        if let currentStatus = eventDetails?.is_comming{
            if currentStatus{
                isComming = 0
            }else{
                isComming = 1
            }
        }
        self.isComingRequest(userID: userID, eventID: String(eventID), isComming: isComming)
    }
    
    
    func isComingRequest(userID : Int,eventID:String,isComming:Int ) {
        self.view.isUserInteractionEnabled = false
        self.isComingBtn.animate(animation: .collapse)
        
        let parameters = ["user_id":userID,"is_comming":isComming, "event_id":eventID]  as [String:Any]
        
        UserService.isComingEvent(Constants.IsComingEventApi, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            self.isComingBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let isComingResponseResult = response as? IsComingResponse {
                    if (isComingResponseResult.data?.id != nil){
                        AccountManager.shared().allEvents![self.itemIndex!].is_comming = true
                        self.isComingBtn.storedTitle = "Cancel"
                        self.view.makeToast("you are going")
                    }else{
                        AccountManager.shared().allEvents![self.itemIndex!].is_comming = false

                        self.isComingBtn.storedTitle = "Going"
                        self.view.makeToast("the event cancelled")
                    }
                }
          
            }
        })
    }
    

}
