//
//  SendMessageData.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
class SendMessageData: Codable {
    let from_id : Int?
    let to_id : Int?
    let message : String?
    let updated_at : String?
    let created_at : String?
    let id : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case from_id = "from_id"
        case to_id = "to_id"
        case message = "message"
        case updated_at = "updated_at"
        case created_at = "created_at"
        case id = "id"
    }
}
