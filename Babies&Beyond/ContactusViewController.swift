//
//  ContactusViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/18/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
class ContactusViewController: UIViewController {

    @IBOutlet weak var sendbutton: EMSpinnerButton!
    @IBOutlet weak var ReasonTxt: UITextField!
    @IBOutlet weak var contactSubject: UITextField!
    @IBOutlet weak var contactMessage: KMPlaceholderTextView!
    
    var reasonsArr = ["General","Inquiry","Suggestion","Complaint","Ask Midwife","Call Me Back"]
    let reasonsPicker = UIPickerView()
    var profileLoginData = AccountManager.shared().userData
    
    let reasonPlaceHolder = "Select Your Reason"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.sendbutton.cornerRadius = 5.0
        self.sendbutton.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        self.title = "Contact Us"
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:  82/255.0, green: 87/255.0, blue: 106/255.0, alpha: 100.0/100.0)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //showData()
        ReasonTxt.inputView = reasonsPicker
        ReasonTxt.placeholder = reasonPlaceHolder
        reasonsPicker.delegate = self
        reasonsPicker.selectRow(0, inComponent:0, animated:true)
        
        self.contactMessage.layer.borderWidth = 0.0
        self.contactMessage.layer.borderColor = UIColor.lightGray.cgColor
        self.contactMessage.placeholder = "Write your message..."
        
        self.contactSubject.textColor = UIColor.black
        self.ReasonTxt.textColor = UIColor.black
    }

    @IBAction func ContactSendButton(_ sender: Any) {
        
        let email = AccountManager.shared().userData?.email ?? ""
        let name = AccountManager.shared().userData?.name ?? ""
        let userphone = AccountManager.shared().userData?.phone ?? ""
      
        guard let reason = ReasonTxt.text, !reason.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.PickerView, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let subject = contactSubject.text , !subject.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptySubject, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let message = contactMessage.text , !message.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyMessage, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        
        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.sendbutton.animate(animation: .collapse)
            PostMessage(name: name, email: email, phone: userphone, subject: subject, reason: reason, message: message)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func PostMessage(name:String,email:String,phone:String,subject:String,reason:String,message:String){
        
        let parameters = ["name":name, "email":email, "phone":phone,"subject": subject,"reason":reason ,"message":message] as [String:Any]
        UserService.contactUsPostMessage(Constants.PostMessageApi, params: parameters , completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            if let error = error {
                self.sendbutton.animate(animation: .expand)
                // show alert with error message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? ContactPostMessageResponse {
                    if result.status!{
                        self.sendbutton.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.sendButtonMessage, controller: self, okBtnTitle: Constants.OKTItle)
                        self.contactSubject.text = ""
                        self.ReasonTxt.text = ""
                        self.contactMessage.text = ""
                    }else{
                        self.sendbutton.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    return
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

}

extension ContactusViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return reasonsArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return reasonsArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        ReasonTxt.text = reasonsArr[row]
    }
}
