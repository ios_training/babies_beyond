//
//  TaskInfoVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 3/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class TaskInfoVC: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var rateServiceLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    
    @IBOutlet weak var commentBtn: EMSpinnerButton!
    
    @IBOutlet weak var DateFromLbl: UILabel!
    
    @IBOutlet weak var dateToLbl: UILabel!
    
    @IBOutlet weak var availableTimeSlotsTableView:UITableView!
    
    //@IBOutlet weak var tableView: UITableView!
    // MARK: private variables
    fileprivate let cellId = "timeSlotCell"
    fileprivate var days:[String:[MidwifeTime]]?
    
    var selectedTask:ServiceInfoData?
    var isDone:Bool?
    var isMidwife:Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Task Info"

        availableTimeSlotsTableView.register(UINib(nibName: "MidwifeTimeSlotTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        availableTimeSlotsTableView.separatorStyle = .none
        if let availabelTimeSlotsArr = selectedTask?.dates {
            if availabelTimeSlotsArr.count > 0 {
                days = groupTimeSlotsPerDay(availableTimes: availabelTimeSlotsArr)
                self.availableTimeSlotsTableView.isHidden = false
            }else{
                self.availableTimeSlotsTableView.isHidden = true
            }
        }else{
            self.availableTimeSlotsTableView.isHidden = true
        }
        
        
        
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        
//        self.commentBtn.cornerRadius = 5.0
//        self.commentBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
//        self.commentBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        //self.tabBarController?.tabBar.isHidden = true
        //setupView()
        //self.tabBarController?.navigationItem.hidesBackButton = true
        
        self.navigationItem.hidesBackButton = true
        
         self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "LeftChevron"), style: UIBarButtonItemStyle.done, target: self, action: #selector(backOrCancelBtnTapped(_:)))
        
        if isDone!{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_insert_comment_white"), style: UIBarButtonItemStyle.done, target: self, action: #selector(commentBtnTapped(_:)))
        }

        
        if let staffType = AccountManager.shared().staffData?.user_type_id{
            if staffType == AccountTypeID.MideWife.rawValue{
                isMidwife = true
            }else{
                isMidwife = false
            }
        }
        
    }
    private func groupTimeSlotsPerDay(availableTimes : [MidwifeTime]) -> [String:[MidwifeTime]] {
        let groupedDays = Dictionary(grouping: availableTimes) { (availableTime) -> String in
            return availableTime.date!
        }
        return groupedDays
    }
    @IBAction func backOrCancelBtnTapped(_ sender: UIBarButtonItem) {
 
            self.navigationController?.popViewController(animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        setupView()
    }

    @IBAction func commentBtnTapped(_ sender: UIButton) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        VC.serviceID = selectedTask?.id
        VC.selectedTask = selectedTask
        self.navigationController?.pushViewController(VC, animated: true)
    }
    func setupView()  {
        //commentBtn.isHidden = true
        if let obj = selectedTask{
            if let name = obj.user_name{
                userNameLbl.text = name
            }else{
                userNameLbl.text = "None"
            }
            if let phone = obj.phone{
                phoneLbl.text = phone
            }else{
                phoneLbl.text = "None"
            }
            if let location = obj.location{
                locationLbl.text = location
            }else{
                locationLbl.text = "None"
            }
            if let rate = obj.rate{
                switch rate {
                case 1:
                    rateServiceLbl.text = "Very Bad"
                    break
                case 2:
                    rateServiceLbl.text = "Bad"
                    break
                case 3:
                    rateServiceLbl.text = "Good"
                    break
                case 4:
                    rateServiceLbl.text = "Very Good"
                    break
                case 5:
                    rateServiceLbl.text = "Excellent"
                    break
                default:
                    rateServiceLbl.text = "None"
                    break
                }
            }
            if let comment = obj.comment{
                commentLbl.text = comment
            }else{
                commentLbl.text = "None"
            }
            
            if let serviceDates = obj.dates{
                if isMidwife!{
                    //tableView.isHidden = false
                }else{
                    //tableView.isHidden = true
                    DateFromLbl.text = Helper.fullDateConverter(dateString: serviceDates[0].from!)
                    dateToLbl.text = Helper.fullDateConverter(dateString: serviceDates[0].to!)
                }
            }
        }
    }
}


extension TaskInfoVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let allDays = days {
            return allDays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allDays = days {
            let dayNameKey = allDays.keys.sorted()[section]
            if let midwifeTimes = allDays[dayNameKey] {
                return midwifeTimes.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MidwifeTimeSlotTableViewCell
        let dayNameKey = days!.keys.sorted()[indexPath.section]
        if let midwifeTimes = days![dayNameKey] {
            let endHourAsStringArr = midwifeTimes[indexPath.row].to!.components(separatedBy: ":")
            var endHourString = "\(endHourAsStringArr[0]):\(endHourAsStringArr[1])"
            
            let startHourAsStringArr = midwifeTimes[indexPath.row].from!.components(separatedBy: ":")
            var startHourString = "\(startHourAsStringArr[0]):\(startHourAsStringArr[1])"
            
            //convert startHour from 24 format to 12 format
            let dateFormatter = DateFormatter()
            //dateFormatter.dateFormat = "HH:mm"
            dateFormatter.dateFormat = Constants.MidwifeDateFormatter//"yyyy-MM-dd HH:mm"

            if let fromHourIn24Format = dateFormatter.date(from: startHourString) {
                dateFormatter.dateFormat = "h:mm a"
                startHourString = dateFormatter.string(from: fromHourIn24Format)
                cell.startTimeLabel.text = "From \(startHourString)"
            }else{
                cell.startTimeLabel.text = "From \(startHourString)"
            }
            
            //convert endHour from 24 format to 12 format
            //dateFormatter.dateFormat = "HH:mm"
            dateFormatter.dateFormat = Constants.MidwifeDateFormatter//"yyyy-MM-dd HH:mm"

            if let endHourIn24Format = dateFormatter.date(from: endHourString) {
                dateFormatter.dateFormat = "h:mm a"
                endHourString = dateFormatter.string(from: endHourIn24Format)
                cell.endTimeLabel.text = "To \(endHourString)"
            }else{
                cell.endTimeLabel.text = " To \(endHourString)"
            }
            
        }
        cell.selectionStyle = .none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let allDays = days {
            return allDays.keys.sorted()[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let screenWidth = UIScreen.main.bounds.size.width
        let headerFrame = CGRect(x: 0, y: 0, width: screenWidth, height: 40)
        let headerView = UIView(frame: headerFrame)
        headerView.backgroundColor = UIColor(r: 82, g: 87, b: 106)
        
        let dayNameFrame = CGRect(x: 10, y: 0, width: screenWidth-10, height: 40)
        let dayNameLabel = UILabel(frame: dayNameFrame)
        let font = UIFont(name: "MuseoSans-300", size: 17)
        dayNameLabel.font = font
        dayNameLabel.textColor = UIColor.white
        
        headerView.addSubview(dayNameLabel)
        if let allDays = days {
            dayNameLabel.text = allDays.keys.sorted()[section]
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
