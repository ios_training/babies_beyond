//
//  ChatInputContainerView.swift
//  ChatLibraryModule
//
//  Created by M.I.Kamashany on 12/6/17.
//  Copyright © 2017 NtamTech. All rights reserved.
//

import UIKit

class ChatInputContainerView: UIView , UITextFieldDelegate {
    
    var chatLogController:ChatLogController? {
        didSet{
            sendButton.addTarget(chatLogController, action: #selector(ChatLogController.handleSend), for: UIControlEvents.touchUpInside)
            uploadImageView.addGestureRecognizer(UITapGestureRecognizer(target: chatLogController, action: #selector(ChatLogController.handleUploadImageViewTapped)))
        }
    }
    
    lazy var messageTextField : UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter your message..."
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()
    
    let uploadImageView: UIImageView = {
        let uploadImageView = UIImageView()
        uploadImageView.isUserInteractionEnabled = true
        uploadImageView.image = UIImage(named: "upload_image_icon")
        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
        return uploadImageView
    }()
    
    let sendButton = UIButton(type: UIButtonType.system)
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        addSubview(uploadImageView)
        //constraints
        uploadImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        uploadImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        uploadImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        uploadImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.setTitle("Send", for: UIControlState.normal)
        addSubview(sendButton)
        //constraints
        sendButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        sendButton.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        addSubview(self.messageTextField)
        //constraints
        self.messageTextField.leftAnchor.constraint(equalTo:  uploadImageView.rightAnchor, constant: 8).isActive = true
        self.messageTextField.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.messageTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        self.messageTextField.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        //separator
        let separatorline = UIView()
        separatorline.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1.0)
        separatorline.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separatorline)
        //constraints
        separatorline.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        separatorline.topAnchor.constraint(equalTo: topAnchor).isActive = true
        separatorline.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        separatorline.heightAnchor.constraint(equalToConstant: 0.5).isActive  = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        chatLogController?.handleSend()
        return true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
