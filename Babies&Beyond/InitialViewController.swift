//
//  InitialViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 1/25/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let when = DispatchTime.now() + 0.5 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // check cached user if it exists
            if let data = UserDefaults.standard.value(forKey: Constants.LoggedUserKey) as? Data {
                do {
                    let userDataObj = try JSONDecoder.init().decode(User_data.self, from: data)
                    
                     let typeId = String(userDataObj.user_type_id ?? 1)
                        if typeId == AccountTypeID.User.rawValue {
                            AccountManager.shared().userData = userDataObj
                            self.moveToUserScreens()
                        }else if typeId == AccountTypeID.Nurse.rawValue || typeId == AccountTypeID.MideWife.rawValue || typeId == AccountTypeID.BabySitter.rawValue {
                            let dataObj = try JSONDecoder.init().decode(NurseUser_data.self, from: data)
                             AccountManager.shared().staffData = dataObj
                            self.moveToNurseScreens()
                        }
                    
                }catch{
                    self.moveToLoginScreen(fromLogout: false)
                }
            }else{
                self.moveToLoginScreen(fromLogout: false)
            }
        }
    }// end of viewDidAppear method
    
}
