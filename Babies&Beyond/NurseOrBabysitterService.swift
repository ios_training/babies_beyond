//
//  NurseOrBabysitterService.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/10/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class NurseOrBabysitterService: Codable {
    var id : String?
    var user_id : String?
    var staff_id : String?
    var start_date : String?
    var end_date : String?
    var location : String?
    var price : String?
    var rate : String?
    var staff_comment : String?
    var point : String?
    var created_by : String?
    var service_workshop_status_id : String?
    var service_type_id : String?
    var created_at : String?
    var updated_at : String?
    var service_type_name : String?
    var service_workshop_status_name:String?
    var staff_name:String?
    var longitude:Double?
    var latitude:Double?
    var no_of_children : String?
    var birth_date : String?
    var additional_info : String?
    var is_complex : String?
    var nurse_type : String?
    var user_name : String?
    var user_photo : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case user_id = "user_id"
        case staff_id = "staff_id"
        case start_date = "start_date"
        case end_date = "end_date"
        case location = "location"
        case price = "price"
        case rate = "rate"
        case staff_comment = "staff_comment"
        case point = "point"
        case created_by = "created_by"
        case service_workshop_status_id = "service_workshop_status_id"
        case service_type_id = "service_type_id"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case service_type_name = "service_type_name"
        case service_workshop_status_name = "service_workshop_status_name"
        case staff_name = "staff_name"
        case longitude
        case latitude
        case no_of_children = "no_of_children"
        case birth_date = "birth_date"
        case additional_info = "additional_info"
        case is_complex = "is_complex"
        case nurse_type = "nurse_type"
        case user_name = "user_name"
        case user_photo = "user_photo"
    }
}
