//
//  MidwifeAppointmentsViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/26/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeAppointmentsViewController: UIViewController {

    // MARK: outlets
    @IBOutlet weak var appointmentsTableView:UITableView!
    @IBOutlet weak var sendRequestBtn:EMSpinnerButton!
    @IBOutlet weak var costLabel:UILabel!
    
    // MARK: private variables
    private let cellId = "appointmentCell"
    
    // MARK: public variables
    public var selectedMidwife:Midwife?
    public var appointmentsArr = [MidwifeTime]()
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        appointmentsTableView.register(UINib(nibName: "AppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        sendRequestBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        sendRequestBtn.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if appointmentsArr.count > 0 {
            appointmentsTableView.reloadData()
            self.costLabel.isHidden = false
            self.sendRequestBtn.isHidden = false
            calculatePrice()
        }else{
            self.costLabel.isHidden = true
            self.sendRequestBtn.isHidden = true
        }
    }
    
    
    private func calculateTotalNumberOfHours(appointmentsArr:[MidwifeTime]) -> Float {
        var result = 0
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        for appointment in appointmentsArr {
            let fromTime = dateFormatter.date(from: appointment.from!)
            let toTime = dateFormatter.date(from: appointment.to!)
            let components = Calendar.current.dateComponents([.hour, .minute], from: fromTime!, to: toTime!)
            result = result + components.hour!
        }
        return Float(result)
    }
    
    private func calculatePrice() {
        let hourPrice = (selectedMidwife!.pricePerHour! as NSString).floatValue
        let totalHours = calculateTotalNumberOfHours(appointmentsArr: appointmentsArr)
        self.costLabel.text = "Cost (\(hourPrice * totalHours)$)"
    }
    
    @IBAction func sendMidwifeRequest(_ sender:Any){
        if InternetConnection.connected() {
            sendRequestBtn.animate(animation: AnimationType.collapse)
            UserService.sendMidwifeRequest(midwifeId: (selectedMidwife?.id)!, appointments: appointmentsArr, completion: { (data, error) in
                self.sendRequestBtn.animate(animation: AnimationType.expand)
                if let response = data as? MidwifeReservationResponse {
                    if response.status! {
                        if let newMidwifeService = response.data {
                            AccountManager.shared().allServices?.append(newMidwifeService)
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }else{
                        // show alert
                        Helper.showAlert(title: Constants.errorAlertTitle, message: response.message!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    // show alert
                    Helper.showAlert(title: Constants.errorAlertTitle, message: error!, controller: self, okBtnTitle: Constants.OKTItle)
                }
            })
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    @IBAction func showNewMidwifeAppointmentViewController(_ sender:Any){
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "NewMidwifeAppointmentViewController") as! NewMidwifeAppointmentViewController
        destination.selectedMidwife = selectedMidwife
        destination.appointmentsArr = appointmentsArr
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func back(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
}

extension MidwifeAppointmentsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! AppointmentTableViewCell
        let appointment = appointmentsArr[indexPath.row]
        let from12Date = Helper.to12DateFormat(dateIn24Format:appointment.from!)
        cell.fromHourLabel.setTitle(from12Date, for: .normal)
        let to12Date = Helper.to12DateFormat(dateIn24Format:appointment.to!)
        cell.toHourLabel.setTitle(to12Date, for: .normal)
        cell.dayInCalenderLabel.text = appointment.date
        cell.dayNameLabel.text = getDayNameFrom(dateString: appointment.date!)
        tableView.tableFooterView = UIView()
        return cell
    }
    
    private func getDayNameFrom(dateString:String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: dateString){
            formatter.dateFormat  = "EEEE"
            return formatter.string(from: date)
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            appointmentsArr.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            if appointmentsArr.count == 0 {
                self.costLabel.isHidden = true
                self.sendRequestBtn.isHidden = true
            }else{
                self.costLabel.isHidden = false
                self.sendRequestBtn.isHidden = false
                calculatePrice()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
