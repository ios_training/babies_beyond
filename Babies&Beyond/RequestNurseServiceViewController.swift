//
//  RequestNurseServiceViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 6/9/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import STPopup
import PKHUD
import GooglePlacePicker

class RequestNurseServiceViewController: UIViewController {

    var newServiceType:ServiceType? {
        didSet{
            if newServiceType == ServiceType.nurse {
                self.title = "Request Nurse"
                self.selectedServiceID = "1"
            }else if newServiceType == ServiceType.babySitter{
                self.title = "Request BabySitter"
                self.selectedServiceID = "2"
            }
        }
    }
    
    var selectedServiceID : String?
    var selectedStartDateTime : String?
    var selecteddateOfbirth : String?
    var selectedEndDateTime : String?
    var minimumbirthDate : Date?
    
    var userData : User_data?
    var isNavigationBarHidden:Bool = true
    var minimumEndDate : Date?
    var address:String?
    var longitude:Double?
    var latitude:Double?
    var isComplex:String?
    var serviceTypeID : String?

    
    @IBOutlet var viewContainer:UIView!
    @IBOutlet weak var startDateTimeBtn: UIButton!
    @IBOutlet weak var endDateTimeBtn: UIButton!
    //@IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var sendBtn:EMSpinnerButton!
    @IBOutlet weak var selectService: UITextField!
    @IBOutlet weak var dateOfBirth: UIButton!
    @IBOutlet weak var additionalInformation: UITextView!
    @IBOutlet weak var requestIsComplex: UISwitch!
    @IBOutlet weak var numberOfChildren: UITextField!
    @IBOutlet weak var numberOfHours: UITextField!
    
    var reasonsArr = ["Newborn Care","Child Development",".Professional Babysitting","Breastfeeding & postnatal","Sleep Support","Nanny Training"]
    let reasonsPicker = UIPickerView()
    let servicePlaceHolder = "Select Your Service"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendBtn.cornerRadius = 5.0
        self.sendBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.sendBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        selectService.inputView = reasonsPicker
        selectService.placeholder = servicePlaceHolder
        reasonsPicker.delegate = self
        reasonsPicker.selectRow(0, inComponent:0, animated:true)
        //        self.submitButton.cornerRadius = 5.0
        //        self.submitButton.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        self.title = "Nurse Service"
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:  82/255.0, green: 87/255.0, blue: 106/255.0, alpha: 100.0/100.0)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
  
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isNavigationBarHidden {
            self.navigationController?.isNavigationBarHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(locationLblTapped))
        //locationLbl.isUserInteractionEnabled = true
        locationLbl.addGestureRecognizer(tap)
        
        if let selectedStartDateTime = selectedStartDateTime{
            startDateTimeBtn.setTitle(selectedStartDateTime, for: .normal)
        }
        
        if let selectedBirthDate = selecteddateOfbirth{
            dateOfBirth.setTitle(selectedBirthDate, for: .normal)
        }
        
//        if let selectedEndDateTime = selectedEndDateTime{
//            endDateTimeBtn.setTitle(selectedEndDateTime, for: .normal)
//        }
        
    }
    
    @objc func locationLblTapped(sender:UITapGestureRecognizer) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    @IBAction func chooseStartDateTimeTapped(_ sender: UIButton) {
        isNavigationBarHidden = false
        let destination = self.storyboard!.instantiateViewController(withIdentifier: "DatePopupVC") as? DatePopupVC
        destination?.nurseSendRequestVC = self
        destination?.isStartDate = true
        destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
        let popupController = STPopupController.init(rootViewController: destination!)
        popupController.navigationBarHidden = true
        popupController.present(in: self)
    }
    
    @IBAction func chooseEndDateTimeTapped(_ sender: UIButton) {
        if minimumEndDate == nil{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.StartDateTimeAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }else{
            isNavigationBarHidden = false
            let destination = self.storyboard!.instantiateViewController(withIdentifier: "DatePopupVC") as? DatePopupVC
            destination?.nurseSendRequestVC = self
            destination?.isStartDate = false
            
            destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
            let popupController = STPopupController.init(rootViewController: destination!)
            
            popupController.navigationBarHidden = true
            popupController.present(in: self)
        }
    }
    
    @IBAction func DateofBirthAction(_ sender: Any) {
        isNavigationBarHidden = false
        let destination = self.storyboard!.instantiateViewController(withIdentifier: "BirthdatepopupViewController") as? BirthdatepopupViewController
        destination?.nurseSendRequestVC = self
        destination?.isStarBirthtDate = true
        destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
        let popupController = STPopupController.init(rootViewController: destination!)
        popupController.navigationBarHidden = true
        popupController.present(in: self)
        
    }
    @IBAction func sendBtnTapped(_ sender: UIButton){
        
        guard let userID = AccountManager.shared().userData?.id else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ServiceNameAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let serviceID = selectedServiceID, !serviceID.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ServiceNameAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        //let serviceTypeID = Constants.NurseTypeID
        
        guard let startDate = selectedStartDateTime, !startDate.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseEmptyStartDate, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        
        guard let noOfHours = numberOfHours.text, !noOfHours.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseEmptyNumberOfHours, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //dateFormatter.timeZone = TimeZone(identifier: "UTC-2")
        let date1 = dateFormatter.date(from: selectedStartDateTime!)
        let section1 = date1!.adding(hours:Int(noOfHours)!)
        let enddate =  dateFormatter.string(from: section1)
        //var components = Calendar.current.dateComponents([.hour, .minute], from: date1!)
        //let hour = components.hour!
        //let minute = components.minute!
        // print(hour)
        // print(minute)
        // components.timeZone = NSTimeZone.local
   

      

        guard let location = locationLbl.text ,let long = longitude, let lat = latitude, !location.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseEmptyLocation, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let numberofChildren = numberOfChildren.text , !numberofChildren.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseEmptyNumberOfChildren, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let birthDate = selecteddateOfbirth, !birthDate.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseEmptyBirthDate, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        
        
        guard let additionalInformationText = additionalInformation.text , !additionalInformationText.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseEmptyAdditionalInformation, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        
        guard let choosenurseType = selectService.text, !choosenurseType.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseEmptyService, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        if requestIsComplex.isOn == false{
            isComplex = "0"
        }
        else{
            isComplex = "1"
        }
        
        if InternetConnection.connected() {
            sendServiceRequest(userID: "\(userID)", serviceTypeID: serviceID, startDate: startDate,endDate:enddate,location:location,numberofChildren:numberofChildren,birthDate:birthDate,additionalInformationText:additionalInformationText,choosenurseType:choosenurseType,long:long,lat:lat,isComplex:isComplex!)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func sendServiceRequest(userID:String,serviceTypeID:String,startDate:String,endDate:String,location:String,numberofChildren:String,birthDate:String,additionalInformationText:String,choosenurseType:String,long:Double,lat:Double,isComplex:String) {

//        guard let userID = AccountManager.shared().userData?.id else{
//            return
//        }
        self.view.isUserInteractionEnabled = false
        self.sendBtn.animate(animation: .collapse)
        let parameters = ["user_id" : userID, "service_type_id": serviceTypeID, "start_date": startDate, "end_date":endDate, "location":location,"longitude":longitude!,"latitude":latitude!,"no_of_children":numberofChildren,"birth_date":birthDate,"additional_info":additionalInformationText,"is_complex":isComplex,"nurse_type":choosenurseType] as [String : Any]
        UserService.serviceRequest(Constants.ServiceRequestApi, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            self.sendBtn.animate(animation: .expand)
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // show alert with success request.
                if let requestedService =  response as? SendServiceRequestResponse{
                    if requestedService.status! {
                      AccountManager.shared().allServices?.append(requestedService.data!)
                      //  Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NurseSuccessNurseServiceRequestAlert, controller: self, okBtnTitle: Constants.OKTItle)
                        self.selectService.text = ""
                        self.locationLbl.text = ""
                        self.startDateTimeBtn.setTitle("", for: .normal)
                        self.dateOfBirth.setTitle("", for: .normal)
                        self.numberOfHours.text = ""
                        self.numberOfChildren.text = ""
                        self.additionalInformation.text = ""
                        self.requestIsComplex.setOn(false, animated: true)
                        self.navigationController?.popToRootViewController(animated: true)
                        print("sa7")
                    }else{
                        Helper.showAlert(title: Constants.errorAlertTitle, message: (error?.localizedDescription)!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
    }
}

extension RequestNurseServiceViewController : GMSPlacePickerViewControllerDelegate {
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.viewContainer.isHidden = false
        // self.indicatorView.isHidden = true
        address = place.formattedAddress?.components(separatedBy: ", ").joined(separator: ",")
        longitude = place.coordinate.longitude
        latitude = place.coordinate.latitude
        self.locationLbl.text = address
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}


extension RequestNurseServiceViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return reasonsArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return reasonsArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectService.text = reasonsArr[row]
    }
}

extension Date {
    func adding(hours: Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }

}




