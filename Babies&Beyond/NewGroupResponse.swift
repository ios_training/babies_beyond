//
//  NewGroupResponse.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 1/23/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class NewGroupResponse: Codable {
    
    var status : Bool?
    var message : String?
    var data:Group?
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
}
