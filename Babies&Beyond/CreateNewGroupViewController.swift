//
//  CreateNewGroupViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 1/23/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class CreateNewGroupViewController: UIViewController {

    @IBOutlet weak var groupImageView:UIImageView!
    @IBOutlet weak var addGroupImageBtn:UIButton!
    @IBOutlet weak var groupNameTextField:UITextField!
    @IBOutlet weak var groupDescTextField:UITextField!
    @IBOutlet weak var createGroupBtn:EMSpinnerButton!
    
    var imageBase64Str:String?
    var selectedImageFromPicker:UIImage? {
        didSet{
            let imageData = UIImageJPEGRepresentation(selectedImageFromPicker!, 0.3)
            imageBase64Str = imageData?.base64EncodedString(options: .endLineWithLineFeed)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "New Group"
        self.groupImageView.layer.cornerRadius = 50
        self.groupImageView.layer.masksToBounds = true
        self.createGroupBtn.cornerRadius = 5.0
        self.createGroupBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.tabBarController?.navigationController?.isNavigationBarHidden = true
        let userMessagesVC = navigationController?.viewControllers[0] as? UserMessageVC
        userMessagesVC?.isBackFromNewGroupScreen = true
    }
    
    // MARK: Actions
    @IBAction func addGroupImageBtnTapped(_ sender:Any){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    @IBAction func createNewGroupBtnTapped(_ sender:Any){
        if let inputs = validateSignInInputs() {
            checkInternetAndCreateGroup(name: inputs.0, descr: inputs.1,groupImage: imageBase64Str)
        }        
    }
    
    // check inputs of the sign in form
    private func validateSignInInputs() -> (String,String)? {
        
        guard let name = groupNameTextField.text, !name.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.GroupNameRequired, controller: self, okBtnTitle: Constants.OKTItle)
            return nil
        }
        guard let description = groupDescTextField.text, !description.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.GroupDescriptionRequired, controller: self, okBtnTitle: Constants.OKTItle)
            return nil
        }
        return (name,description)
    }
    
    private func checkInternetAndCreateGroup(name:String, descr:String, groupImage:String?) {
        self.createGroupBtn.animate(animation: .expand)
        if InternetConnection.connected() {
            guard let userId = AccountManager.shared().userData?.id else{
                return
            }
            self.view.isUserInteractionEnabled = false
            self.createGroupBtn.animate(animation: .collapse)
            var params = ["name":name,"description":descr,"created_by":userId] as [String:Any]
            if groupImage != nil {
                params["photo"] = groupImage
            }
            UserService.createGroup(parameters:params, completion: { (response, error) in
                
                if let error = error {
                    self.createGroupBtn.animate(animation: .expand)
                    Helper.showAlert(title: Constants.errorAlertTitle, message: error.localizedDescription, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    if let newGroupResponse = response as? NewGroupResponse {
                        if newGroupResponse.status! {
                            var arrayOfGroups = [Group]()
                            arrayOfGroups.append(newGroupResponse.data!)
                            if let existedGroups = AccountManager.shared().allGroups {
                                if existedGroups.count > 0 {
                                    arrayOfGroups.append(contentsOf: existedGroups)
                                }
                            }
                            AccountManager.shared().allGroups = arrayOfGroups
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            self.createGroupBtn.animate(animation: .expand)
                            Helper.showAlert(title: Constants.errorAlertTitle, message: newGroupResponse.message!, controller: self, okBtnTitle: Constants.OKTItle)
                        }
                    }
                }
            })
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
}


extension CreateNewGroupViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImageFromPicker = editedImage
            groupImageView.image = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
            groupImageView.image = originalImage
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension CreateNewGroupViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            // navigate the focus to the password textfield
            groupDescTextField.becomeFirstResponder()
        }else {
            // hide the keyboard
            self.view.endEditing(true)
            if let inputs = validateSignInInputs() {
                checkInternetAndCreateGroup(name: inputs.0, descr: inputs.1, groupImage: imageBase64Str)
            }
        }
        return true
    }
}
