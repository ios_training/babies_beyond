//
//  Article.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 4/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class Article: Codable {
    var id:Int?
    var body:String?
    var title:String?
    var createdAt:String?
    var updatedAt:String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case body
        case title
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
