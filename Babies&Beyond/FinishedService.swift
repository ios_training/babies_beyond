

import Foundation

class FinishedService : Codable {
    
    var type:Int
    var id:String?
    var rate:String?
    var name : String?
    var comment:String?
    var location : String?
    var dates:[MidwifeTime]?
    
    enum CodingKeys: String, CodingKey {
        case type
        case id
        case rate
        case name
        case comment
        case location
        case dates
    }
}

