//
//  MidwifeListVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/24/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeListVC: UIViewController {
    
    // MARK: views' outlets
    @IBOutlet weak var mideWifeTableView:UITableView!
    
    // MARK: private variables
    private let cellId = "midwifeCell"
    private var midwifes:[Midwife]?
    fileprivate var loadingView:LoadingView?
    fileprivate var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    
    // MARK: ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        observeInternetChanges()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Service.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }

    
    
    // MARK: custom methods
    fileprivate func setupViews(){
        // navigation bar customization
        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.hidesBackButton = true
        mideWifeTableView.register(UINib(nibName: "MidwifeTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        mideWifeTableView.rowHeight = UITableViewAutomaticDimension
        mideWifeTableView.estimatedRowHeight = 54
        
        let midY = UIScreen.main.bounds.midY
        let frame = CGRect(x: 0, y: midY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let frameBtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: frameBtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        loadingView?.isHidden = false
        connectionBtn?.isHidden = true
        mideWifeTableView.isHidden = true
    }
    
    private func observeInternetChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    fileprivate func getAllMidwifes(){
        mideWifeTableView.isHidden = true
        UserService.loadAllMidwifes { (response, error) in
            self.loadingView?.isHidden = true
            if let _ = error {
                self.connectionBtn?.isHidden = false
                self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
            }else{
                if let allMidwifesResponse = response as? AllMidwifesResponse {
                    if allMidwifesResponse.status! {
                        if let midwifes = allMidwifesResponse.midwifesArr {
                            self.midwifes = midwifes
                            self.mideWifeTableView.isHidden = false
                            self.mideWifeTableView.reloadData()
                        }
                    }else{
                        self.connectionBtn?.isHidden = false
                        self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
                    }
                }
            }
        }
    }
    
    @IBAction func backOrCancelBtnTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func connectionBtnTapped() {
        reachabilityChanged()
    }
    
    @objc func reachabilityChanged(){
        if midwifes == nil {
            if InternetConnection.connected(){
                loadingView?.isHidden = false
                connectionBtn?.isHidden = true
                mideWifeTableView.isHidden = true
                getAllMidwifes()
            }
            else if reachability.currentReachabilityStatus == .notReachable {
                loadingView?.isHidden = true
                mideWifeTableView.isHidden = true
                connectionBtn?.isHidden = false
                connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
            }
        }
    }
}

extension MidwifeListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let midwifesArr = midwifes {
            return midwifesArr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MidwifeTableViewCell
        cell.nameLabel?.text = midwifes![indexPath.row].name
        if indexPath.row == 0 {
            cell.bioLabel.text = "fsdfsdfdfs  fsdfs fsd sfsdfsdf sfsdf sfsd fsdf sdfs fs fssdfsdf fsdfsdfdfs  fsdfs fsd sfsdfsdf sfsdf sfsd fsdf sdfs fs fssdfsdf"
        }else{
            cell.bioLabel.text = "fssdfsdf"
        }
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let midwife = midwifes![indexPath.row]
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "MidwifeDetailsViewController") as! MidwifeDetailsViewController
        destination.selectedMidwife = midwife
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}
