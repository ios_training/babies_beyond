//
//  RateSelectedHistoyItemViewController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/18/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import Toast_Swift


class RateSelectedHistoyItemViewController: UIViewController {
    
    var selectedService:FinishedService?
    var rateValue :Int?

    @IBOutlet weak var rateLabel:UILabel!
    @IBOutlet weak var starRatingView: SwiftyStarRatingView!
    @IBOutlet weak var submitBtn: EMSpinnerButton!
    @IBOutlet weak var cencelBtn: EMSpinnerButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        starRatingView.tintColor = UIColor(red: 255/255.0, green: 162/255.0, blue: 0, alpha: 1.0)
        starRatingView.spacing = 5.0
        starRatingView.allowsHalfStars = false
        self.submitBtn.cornerRadius = 5.0
        self.submitBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.cencelBtn.cornerRadius = 5.0
        self.cencelBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        rateLabel.text = "Please rate the service"
    }
    
    @IBAction func starRatingValueChanged(_ sender: SwiftyStarRatingView) {
        setRateOfService(rate: starRatingView.value)
        rateValue = Int(starRatingView.value)
        
    }
    
    private func setRateOfService(rate:CGFloat){
        switch rate {
        case 1.0:
            rateLabel.text = "Very Bad"
            break
        case 2.0:
            rateLabel.text = "Bad"
            break
        case 3.0:
            rateLabel.text = "Good"
            break
        case 4.0:
            rateLabel.text = "Very Good"
            break
        case 5.0:
            rateLabel.text = "Excellent"
            break
        default:
            rateLabel.text = "Please rate the service"
            break
        }
        
    }
    
    
    @IBAction func submitBtnTapped(_ sender:Any){
        
        guard let rate = rateValue, rate > 0 else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.RateValue, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let serviceID = selectedService?.id else {
            return
        }
        if InternetConnection.connected(){
            self.submitBtn.animate(animation: .collapse)
            if selectedService?.type == 3{
                rateService(apiName : Constants.RateMidwifeApi, serviceID:serviceID  , rate:rate)
            }else{
                rateService(apiName : Constants.RateServiceApi, serviceID:serviceID  , rate:rate)
            }
        }else{
            self.view.makeToast("Check your Internet Connection!", duration: 3.0, position: .center)
        }
        //self.dismiss(animated: true, completion: nil)
    }

    @IBAction func cancelBtnTapped(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }

    func rateService(apiName: String , serviceID: String , rate:Int) {
        var parameters :[String : Any]
        if selectedService?.type == 3{
            parameters = ["unique_key" : serviceID, "rate":rate] as [String : Any]
        }else{
            parameters = ["service_id" : serviceID, "rate":rate] as [String : Any]
        }
        UserService.rateService(apiName, params: parameters, completion: { (response, error) in
            if let _ = error {
                // show alert with err message
                self.submitBtn.animate(animation: .expand)
                self.view.makeToast("Check your Internet Connection!", duration: 3.0, position: .center)
            }else{
                // show alert with success request.
                if let requestResult =  response as? CommentRateResponse{
                    if requestResult.status!{
                        self.selectedService?.rate = "\(rate)"
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.submitBtn.animate(animation: .expand)
                        self.view.makeToast("Check your Internet Connection!", duration: 3.0, position: .center)
                    }
                }
            }
        })
    }
}
