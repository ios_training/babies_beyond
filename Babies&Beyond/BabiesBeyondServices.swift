//
//  BabiesBeyondServices.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Reachability

class Helper: NSObject {

    static func showAlert(title:String, message: String, controller : UIViewController, okBtnTitle:String, cancelBtnTitle:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: okBtnTitle, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            // dismiss alert
        })
        alert.addAction(okButton)
        controller.present(alert, animated: true, completion: nil)
    }
    
}
