//
//  NurseScheduleTableViewCell.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class NurseScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var nurseScheduleImage: UIImageView!
    @IBOutlet weak var nurseScheduleDate: UILabel!
    @IBOutlet weak var nurseScheduleName: UILabel!
    @IBOutlet weak var nusreScheduleLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nurseScheduleImage.layer.cornerRadius = nurseScheduleImage.frame.size.height/2
        nurseScheduleImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
