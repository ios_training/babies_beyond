//
//  EventsVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/26/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import ESPullToRefresh
import Toast_Swift

class EventsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    let cellID = "eventCell"
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
        observeChanges()
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    print("ana fi addPullToRefresh")
                    self?.allEventsRequest()
                }
            }else{
                print("ana fi el noooooo Internet")
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.view.makeToast("Check your Internet Connection!", duration: 3.0, position: .center)
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
    }
    
    
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(eventsChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Event.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Event.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    @objc func eventsChanged(_ notification: NSNotification) {
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        guard let eventId = notification.userInfo?[Constants.Id] as? String else {
            return
        }
        
        if let i = AccountManager.shared().allEvents?.index(where:{$0.id == Int(eventId) ?? 0}) {
            switch action {
            case EventNotificationType.Deleted.rawValue:
                AccountManager.shared().allEvents?.remove(at: i)
                self.tableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: UITableViewRowAnimation.automatic)
                break
            default: break
            }
        }
    }
    
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        createNavBarItems(titleString: "Events")
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        setUpView()
    }
    func updateUI() {
        if AccountManager.shared().allEvents == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    
    
    
    private func refresh() {
        self.allEventsRequest()
    }
    
    fileprivate func setUpView() {

        if let _ = AccountManager.shared().allEvents{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            allEventsRequest()
        }else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    
    func allEventsRequest() {
        guard let userEmail = AccountManager.shared().userData?.email else{
            return
        }
        let parameters = ["email":userEmail] as [String:Any]
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        UserService.getAllEvents(Constants.AllEventsApi, params: parameters, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    self.connectionBtn?.setTitle("Something went wrong, press to try again.", for: .normal)
                }else{
                    print("mafesh neeeeeet")
                    self.tableView.es.stopPullToRefresh()
                }
            }else{
                // response
                if let responseResult = response as? AllEventsResponse {
                    AccountManager.shared().allEvents = responseResult.data
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()

                }else{
                    return
                }
            }
        })
    }

}

extension EventsVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allEvents = AccountManager.shared().allEvents {
            return allEvents.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier:cellID ) as! EventTableViewCell
        let obj = AccountManager.shared().allEvents![indexPath.row]
        cell.eventNameLbl.text = obj.name
        if let startDate = obj.start_date{
            cell.dateFromLbl.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
        }
        if let startTime = obj.start_date{
            cell.timeFromLbl.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
        }
        if let endDate = obj.end_date{
            cell.dateToLbl.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
        }
        if let endTime = obj.end_date{
            cell.timeToLbl.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
        }
        if let isComingEvent = obj.is_comming{
            if isComingEvent{
                cell.eventStatusLbl.isHidden = false
                cell.eventStatusLbl.text = "Going"
            }else{
//                cell.eventStatusLbl.text = "Cancel"
                cell.eventStatusLbl.isHidden = true
            }
        }
        
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "EventInfoVC") as! EventInfoVC
        VC.eventDetails = AccountManager.shared().allEvents![indexPath.row]
        VC.itemIndex = indexPath.row
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let obj = AccountManager.shared().allEvents![indexPath.row]
        if let isComingEvent = obj.is_comming{
            if isComingEvent{
                return 140
            }else{
                return 100
            }
        }
        return 0
    }

//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        //return CGFloat.leastNormalMagnitude
//        return 20
//
//    }
}
