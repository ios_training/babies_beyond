
import Foundation
class WorkshopsDetails : Codable {

    var id : String?
    var name : String?
    var start_date : String?
    var end_date : String?
    var description : String?
    var speaker_name : String?
    var speaker_bio : String?
    var location : String?
    var price : String?
    var point : String?
    var created_by : String?
    var created_at : String?
    var updated_at : String?
    var is_comming : Bool?
//    let payment_status : String?
    var service_workshop_status_name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case start_date = "start_date"
        case end_date = "end_date"
        case description = "description"
        case speaker_name = "speaker_name"
        case speaker_bio = "speaker_bio"
        case location = "location"
        case price = "price"
        case point = "point"
        case created_by = "created_by"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case is_comming = "is_comming"
//        case payment_status = "payment_status"
        case service_workshop_status_name = "service_workshop_status_name"
    }


}
