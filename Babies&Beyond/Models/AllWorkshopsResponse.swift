

import Foundation
struct AllWorkshopsResponse : Codable {
	let status : Bool?
	let message : String?
	let data : [WorkshopsDetails]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}



}
