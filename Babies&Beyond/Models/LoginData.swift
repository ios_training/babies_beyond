

import Foundation
class LoginData : Codable{
	var user_data : User_data?
	var home_page : Home_page?
    var verification_code : Int?

	enum CodingKeys: String, CodingKey {

		case user_data
		case home_page
        case verification_code

	}
}
