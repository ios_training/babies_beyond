
import Foundation
struct NotificationData : Codable {
	let notification : String?
	let notification_time : String?
	let price : String?
	let notification_id : String?
	let is_service : String?

	enum CodingKeys: String, CodingKey {

		case notification = "notification"
		case notification_time = "notification_time"
		case price = "price"
		case notification_id = "notification_id"
		case is_service = "is_service"
	}
}
