
import Foundation
struct StaffScheduleResponse : Codable {
	let status : Bool?
	let message : String?
	let data : [NurseHome_page]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
