
import Foundation
class NurseHome_page : Codable {
    var id : String?
    var user_id : String?
    var staff_id : String?
    var start_date : String?
    var end_date : String?
    var location : String?
    var price : String?
    var rate : String?
    var staff_comment : String?
    var point : String?
    var created_by : String?
    var service_workshop_status_id : String?
    var service_type_id : String?
    var created_at : String?
    var updated_at : String?
    var user_name : String?
    var service_status_name : String?
    var service_status_id : String?
    var is_completed : Int?
    var user_photo : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case staff_id = "staff_id"
        case start_date = "start_date"
        case end_date = "end_date"
        case location = "location"
        case price = "price"
        case rate = "rate"
        case staff_comment = "staff_comment"
        case point = "point"
        case created_by = "created_by"
        case service_workshop_status_id = "service_workshop_status_id"
        case service_type_id = "service_type_id"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case user_name = "user_name"
        case service_status_name = "service_status_name"
        case service_status_id = "service_status_id"
        case is_completed = "is_completed"
        case user_photo = "user_photo"
    }


}
