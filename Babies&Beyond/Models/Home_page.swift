

import Foundation
class Home_page : Codable{
	var services : [NurseOrBabysitterService]?
    var midwifeServices:[MidwifeService]?
	var service_types : [Service_types]?


	enum CodingKeys: String, CodingKey {

		case services = "services"
		case service_types = "service_types"
        case midwifeServices = "midwife_times"

	}
}
