
import Foundation
struct JoinGroupResponse : Codable {
	let status : Bool?
	let message : String?
	let data : GroupData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}
}
