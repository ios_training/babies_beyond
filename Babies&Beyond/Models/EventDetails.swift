

import Foundation
class EventDetails : Codable {
    var id : Int?
    var name : String?
    var start_date : String?
    var end_date : String?
    var description : String?
    var speaker_name : String?
    var speaker_bio : String?
    var location : String?
    var created_by : Int?
    var created_at : String?
    var updated_at : String?
    var is_comming : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case start_date = "start_date"
        case end_date = "end_date"
        case description = "description"
        case speaker_name = "speaker_name"
        case speaker_bio = "speaker_bio"
        case location = "location"
        case created_by = "created_by"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case is_comming = "is_comming"
    }
}
