
import Foundation
struct NurseServiceResponse : Codable {
	let status : Bool?
	let message : String?
	let data : NurseServiseData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}

}
