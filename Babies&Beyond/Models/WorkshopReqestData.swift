

import Foundation
struct WorkshopReqestData : Codable {
	let id : String?
	let service_workshop_status_id : String?
	let user_id : String?
	let workshop_id : String?
	let created_at : String?
	let updated_at : String?
	let service_workshop_status_name : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case service_workshop_status_id = "service_workshop_status_id"
		case user_id = "user_id"
		case workshop_id = "workshop_id"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case service_workshop_status_name = "service_workshop_status_name"
	}
}
