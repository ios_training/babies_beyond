
import Foundation
class NurseLoginData : Codable {
	var user_data : NurseUser_data?
	var home_page : [NurseHome_page]?

	enum CodingKeys: String, CodingKey {

		case user_data = "user_data"
		case home_page = "home_page"
	}
}
