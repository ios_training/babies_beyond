
import Foundation
class NurseLoginResponse : Codable {
	var status : Bool?
	var message : String?
	var data : NurseLoginData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}
}
