

import Foundation
struct RegisterResponse : Codable {
	var status : Bool?
	var message : String?
	var data : User_data?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}

//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        status = try values.decodeIfPresent(Bool.self, forKey: .status)
//        message = try values.decodeIfPresent(String.self, forKey: .message)
//        data = try RegisterData(from: decoder)
//    }

}
