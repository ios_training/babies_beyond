
import Foundation
struct RegisterData : Codable {
	var name : String?
	var email : String?
	var phone : String?
	var user_type_id : String?
	var updated_at : String?
	var created_at : String?
	var id : Int?
	var user_token : String?
    /*
     
    
     
    
     var photo : String?
     var notification_token : String?
     var birthday : String?
     var created_by : String?
     
     */

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case email = "email"
		case phone = "phone"
		case user_type_id = "user_type_id"
		case updated_at = "updated_at"
		case created_at = "created_at"
		case id = "id"
		case user_token = "user_token"
	}

//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        name = try values.decodeIfPresent(String.self, forKey: .name)
//        email = try values.decodeIfPresent(String.self, forKey: .email)
//        phone = try values.decodeIfPresent(String.self, forKey: .phone)
//        user_type_id = try values.decodeIfPresent(String.self, forKey: .user_type_id)
//        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
//        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        user_token = try values.decodeIfPresent(String.self, forKey: .user_token)
//    }

}
