
import Foundation
struct NotificationResponse : Codable {
	let status : Bool?
	let message : String?
	let data : [NotificationData]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
