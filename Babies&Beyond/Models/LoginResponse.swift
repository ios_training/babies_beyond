
import Foundation
class LoginResponse : Codable {
	var status : Bool?
	var message : String?
	var data : LoginData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}



}
