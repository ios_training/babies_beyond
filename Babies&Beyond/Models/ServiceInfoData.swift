

import Foundation
class ServiceInfoData : Codable {
	var id : String?
	var location : String?
	var longitude : Double?
	var latitude : Double?
	var rate : Int?
	var comment : String?
	var is_completed : Bool?
	var dates : [MidwifeTime]?
	var user_name : String?
	var phone : String?
	var email : String?
	var photo : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case location = "location"
		case longitude = "longitude"
		case latitude = "latitude"
		case rate = "rate"
		case comment = "comment"
		case is_completed = "is_completed"
		case dates = "dates"
		case user_name = "user_name"
		case phone = "phone"
		case email = "email"
		case photo = "photo"
	}

}
