
import Foundation
class NurseUser_data : Codable {
	var id : Int?
	var name : String?
	var email : String?
	var phone : String?
	var photo : String?
	var notification_token : String?
	var birthday : String?
	var user_type_id : String?
	var created_by : String?
	var is_logged_in : String?
	var created_at : String?
	var updated_at : String?
	var user_token : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case phone = "phone"
		case photo = "photo"
		case notification_token = "notification_token"
		case birthday = "birthday"
		case user_type_id = "user_type_id"
		case created_by = "created_by"
		case is_logged_in = "is_logged_in"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case user_token = "user_token"
	}

	
}
