
import Foundation
struct SendServiceRequestResponse : Codable {
	var status : Bool?
	var message : String?
	var data : NurseOrBabysitterService?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}

}
