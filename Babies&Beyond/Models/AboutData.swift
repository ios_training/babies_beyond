

import Foundation

struct AboutData : Codable {
    let status : Bool?
	let message : String?
	let data : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
