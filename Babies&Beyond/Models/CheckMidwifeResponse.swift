

import Foundation
struct CheckMidwifeResponse : Codable {
	var status : Bool?
	var message : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
	}


}
