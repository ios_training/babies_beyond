
import Foundation
struct GroupData : Codable {
	let user_id : String?
	let group_id : String?
	let updated_at : String?
	let created_at : String?
	let id : Int?

	enum CodingKeys: String, CodingKey {

		case user_id = "user_id"
		case group_id = "group_id"
		case updated_at = "updated_at"
		case created_at = "created_at"
		case id = "id"
	}

}
