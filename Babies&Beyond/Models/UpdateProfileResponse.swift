
import Foundation
struct UpdateProfileResponse : Codable {
	let status : Bool?
	let message : String?
	let data : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
