

import Foundation
struct ComingEventData : Codable {
	let user_id : Int?
	let event_id : String?
	let updated_at : String?
	let created_at : String?
	let id : Int?

	enum CodingKeys: String, CodingKey {

		case user_id = "user_id"
		case event_id = "event_id"
		case updated_at = "updated_at"
		case created_at = "created_at"
		case id = "id"
	}


}
