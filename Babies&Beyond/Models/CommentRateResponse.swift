//
//  CommentRateResponse.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/19/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
class CommentRateResponse : Codable {
    var status : Bool?
    var message : String?
    
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
        
    }
    
    
    
}
