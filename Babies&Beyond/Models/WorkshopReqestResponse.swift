
import Foundation
struct WorkshopReqestResponse : Codable {
	let status : Bool?
	let message : String?
	let data : WorkshopReqestData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}



}
