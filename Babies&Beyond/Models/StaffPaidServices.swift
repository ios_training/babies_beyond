

import Foundation
class StaffPaidServices : Codable {
	var status : Bool?
	var message : String?
	var data : [ServiceInfoData]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
