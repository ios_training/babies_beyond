

import Foundation
struct IsComingResponse : Codable {
	let status : Bool?
	let message : String?
	let data : ComingEventData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}


}
