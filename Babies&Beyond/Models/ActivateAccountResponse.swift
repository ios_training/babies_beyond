

import Foundation
struct ActivateAccountResponse : Codable {
	var status : Bool?
	var message : String?
    var data : Int?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
        case data
	}


}
