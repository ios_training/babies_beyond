
import Foundation
class AllEventsResponse : Codable {
	var status : Bool?
	var message : String?
	var data : [EventDetails]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}



}
