

import Foundation
class User_data : Codable{
	var id : Int?
	var name : String?
	var email : String?
	var phone : String?
	var photo : String?
	var notification_token : String?
	var birthday : String?
	var user_type_id : Int?
	var created_by : String?
	var created_at : String?
	var updated_at : String?
	var user_token : String?
    var isActivate:Int?
    var verificationCode: String?
    var pricePerHour: String?
    var notificationTokenForWeb:String?
    var isLoggedFromSocial:Bool = false
    
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case phone = "phone"
		case photo = "photo"
		case notification_token = "notification_token"
		case birthday = "birthday"
		case user_type_id = "user_type_id"
		case created_by = "created_by"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case user_token = "user_token"
        case isActivate = "is_activate"
        case pricePerHour = "price_per_hour"
        case notificationTokenForWeb = "notification_web"
        case verificationCode = "verification_code"
	}
}
