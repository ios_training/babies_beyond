//
//  MidwifeReservationResponse.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/9/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeReservationResponse: Codable {
    
    public var status:Bool?
    public var message:String?
    public var data:MidwifeService?
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
}
