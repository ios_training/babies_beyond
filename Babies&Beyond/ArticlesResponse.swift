//
//  ArticlesResponse.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 4/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//


import UIKit

class ArticlesResponse: Codable {
    
    var status : Bool?
    var message : String?
    var data:[Article]?
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
}

