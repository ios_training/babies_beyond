//
//  NotificationVC.swift
//  Babies&Beyond
//
//  Created by NTAM on 12/25/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    let cellID = "notificationCell"
    @IBOutlet weak var tableView: UITableView!
    var loadingView:LoadingView?
    var notificationData = [NotificationData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        loadingView?.isHidden = true
        tableView.isHidden = true
        if AccountManager.shared().userNotfication != nil{
            if InternetConnection.connected() {
                self.tableView.isHidden = true
                loadingView?.isHidden = false
                notificationRequest()
            }else{
                notificationData = (AccountManager.shared().userNotfication?.data)!
                self.tableView.isHidden = false
                loadingView?.isHidden = true
                tableView.reloadData()
            }
        }else{
            self.tableView.isHidden = true
            loadingView?.isHidden = false
            notificationRequest()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func notificationRequest() {
        guard let userID = AccountManager.shared().userData?.id else{
            return
        }
        let parameters = ["user_id":userID]  as [String:Any]
        UserService.userNotification(Constants.UserNotificationApi, params: parameters, completion: { (response, error) in
            self.loadingView?.isHidden = true
            if let _ = error {
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let responseResult = response as? NotificationResponse {
                    AccountManager.shared().userNotfication = responseResult
                    self.notificationData.removeAll()
                    for notification in responseResult.data!{
                        self.notificationData.append(notification)
                    }
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }else{
                    return
                }
            }
        })
    }
}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellID) as! NotificationTableViewCell
        let obj = notificationData[indexPath.row]
        
        if let notificationTitle = obj.notification {
            if notificationTitle == "you payed successfully"{
                if let price = obj.price {
                    cell.titleLabel.text = "You paid \(price) successfully."
                }
            }else{
                cell.titleLabel.text = notificationTitle
            }
        }
        
        if let notificationTime = obj.notification_time{
            cell.descriptionLabel.text = Helper.dateConverterWithFormat(dateString : notificationTime, isDate: true)
        }
        
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
}

