//
//  MidwifeServiceTableViewCell.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 3/10/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var midwifeNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        containerView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
