//
//  CommentViewController.swift
//  Babies&Beyond
//
//  Created by NTAM on 3/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class CommentViewController: UIViewController {

    @IBOutlet weak var commentTxt: KMPlaceholderTextView!
    @IBOutlet weak var submitBtn: EMSpinnerButton!
    
    var serviceID : String?
    var selectedTask:ServiceInfoData?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Comment"
        
        self.submitBtn.cornerRadius = 5.0
        self.submitBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.submitBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        
        self.commentTxt.layer.borderWidth = 0.5
        self.commentTxt.layer.borderColor = UIColor.lightGray.cgColor
        self.commentTxt.placeholder = "Write your comment..."
    }
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        guard let userID = AccountManager.shared().staffData?.id else {
            return
        }
        
        guard let comment = commentTxt.text, !comment.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyComment, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.submitBtn.animate(animation: .collapse)
            rateRequest(userID: userID,serviceID:serviceID!, comment:comment)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func rateRequest(userID: Int,serviceID:String, comment:String) {
        let parameters = ["staff_id" : "\(userID)", "service_id": serviceID, "comment": comment] as [String : Any]
        NurseService.rateCommentRequest(Constants.StaffRateCommentApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                // show alert with err message
                self.submitBtn.animate(animation: .expand)
                
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // show alert with success request.
                if let requestResult =  response as? CommentRateResponse{
                    if requestResult.status!{
                        self.selectedTask?.comment = self.commentTxt.text
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.submitBtn.animate(animation: .expand)
                        
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
    }
}
