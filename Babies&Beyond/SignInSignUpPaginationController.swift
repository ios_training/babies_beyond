//
//  SignInSignUpPaginationController.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 12/24/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit

class SignInSignUpPaginationController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate {
    
    
    @IBOutlet private weak var signInBtn: UIButton!
    @IBOutlet private weak var signUpBtn: UIButton!
    
    @IBOutlet private weak var viewLine: UIView!
    @IBOutlet private weak var constantViewLeft: NSLayoutConstraint!
    
    var signInVC:SigninViewController?
    var signUpVC:SignupViewController?
    
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentPage = 0
        createPageViewController()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    //MARK: - CreatePagination
    
    private func createPageViewController() {
        
        pageController = UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: 254, width: self.view.frame.size.width, height: self.view.frame.size.height-254)
        }
        
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil)
        signInVC = homeStoryboard.instantiateViewController(withIdentifier: "SigninViewController") as? SigninViewController
        signUpVC = homeStoryboard.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController
        
        arrVC = [signInVC!, signUpVC!]
        
        // it's important method which detect all the view controllers that you paginate over them
        pageController.setViewControllers([signInVC!], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        
        self.addChildViewController(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParentViewController: self)
    }
    
    
    //MARK: - Custom Methods
    
    private func selectedButton(btn: UIButton) {
        
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.layer.opacity = 1.0
        
        constantViewLeft.constant = btn.frame.origin.x
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func unSelectedButton(btn: UIButton) {
        btn.setTitleColor(UIColor.white, for: UIControlState.normal)
        btn.layer.opacity = 0.5
    }
    
    
    private func indexOfViewController(viewController: UIViewController) -> Int {
        if(arrVC .contains(viewController)) {
            return arrVC.index(of: viewController)!
        }
        
        return -1
    }
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexOfViewController(viewController: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexOfViewController(viewController: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            resetTabBarForTag(tag: currentPage!)
        }
    }
    
    
    //MARK: - Set Top bar after selecting Option from Top Tabbar
    
    private func resetTabBarForTag(tag: Int) {
        
        var sender: UIButton!
        
        if(tag == 0) {
            sender = signInBtn
        }
        else if(tag == 1) {
            sender = signUpBtn
        }
        
        currentPage = tag
        unSelectedButton(btn: signInBtn)
        unSelectedButton(btn: signUpBtn)
        
        selectedButton(btn: sender)
        
    }
    
    @IBAction private func btnOptionClicked(btn: UIButton) {
        
        if btn.tag == 1 {
            pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: {(Bool) -> Void in
            })
        }else{
            pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: {(Bool) -> Void in
            })
        }
        
        
        resetTabBarForTag(tag: btn.tag-1)
    }
    
}


