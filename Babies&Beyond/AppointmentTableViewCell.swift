//
//  AppointmentTableViewCell.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/27/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {

    
    @IBOutlet weak var dayNameLabel: UILabel!
    @IBOutlet weak var dayInCalenderLabel: UILabel!
    @IBOutlet weak var toHourLabel: UIButton!
    @IBOutlet weak var fromHourLabel: UIButton!
    @IBOutlet weak var startDateBtnContainerView: UIView!
    @IBOutlet weak var endDateBtnContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        toHourLabel.isUserInteractionEnabled = false
        fromHourLabel.isUserInteractionEnabled = false
        startDateBtnContainerView.layer.borderColor = UIColor.lightGray.cgColor
        startDateBtnContainerView.layer.borderWidth = 0.5
        endDateBtnContainerView.layer.borderColor = UIColor.lightGray.cgColor
        endDateBtnContainerView.layer.borderWidth = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
