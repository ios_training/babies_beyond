//
//  SendMessageViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class SendMessageViewController: UIViewController,UITextViewDelegate {
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendButton: EMSpinnerButton!
    
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    var message:String?
    
    var allMessages:[MessageData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.sendButton.cornerRadius = 5.0
        self.sendButton.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        sendButton.layer.cornerRadius = 5.0
//       // messageTextField.text = "Write Your Message."
//      //  messageTextField.textColor = UIColor(red:  204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 100.0/100.0)
        messageTextView.layer.cornerRadius = 5.0
        messageTextView.layer.borderWidth = 1.0
        messageTextView.layer.borderColor = UIColor(red:  169.0/255.0, green: 169.0/255.0, blue: 169.0/255.0, alpha: 100.0/100.0).cgColor
        messageTextView.text = "Write Your Message."
        messageTextView.textColor = UIColor.lightGray
        
        
    }
    

    @IBAction func sendButtonAction(_ sender: Any) {
        
        guard let Message = messageTextView.text , !Message.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyMessage, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.sendButton.animate(animation: .collapse)
            assignService(message: Message)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if messageTextView.textColor == UIColor.lightGray {
            messageTextView.text = nil
            messageTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if messageTextView.text.isEmpty {
            messageTextView.text = "Write Your Message."
            messageTextView.textColor = UIColor.lightGray
        }
    }
    func assignService (message:String){
        
        let parameters = ["message":message] as [String:Any]
        NurseService.SendInboxMessagesRequest(Constants.SendInboxMessage, params: parameters, completion: { (response, error) in
            if let _ = error {
                self.sendButton.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? SendMessageResponse {
                    if result.status!{
                        print("Success")
                        self.sendButton.animate(animation: .expand)
                        if let newMessage = result.data {
                            var newMessagesArr = [MessageData]()
                            newMessagesArr.append(newMessage)
                            if let oldMessages = self.allMessages {
                                newMessagesArr.append(contentsOf: oldMessages)
                            }
                            let count = self.navigationController?.viewControllers.count ?? 0
                            if let previousVC = self.navigationController?.viewControllers[count-2] as? MessagesViewController {
                                previousVC.AllStaffMessages = newMessagesArr
                                AccountManager.shared().inboxMessage = previousVC.AllStaffMessages
                            }
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }else{
                        self.sendButton.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    return
                }
                
            }
        })
    }
}
