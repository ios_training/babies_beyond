//
//  Constants.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation


struct Constants {
    
    //IDs of Users
    static let UserTypeID = "1"
    static let NurseTypeID = "3"
    static let BabysitterTypeID = "4"
    static let MidwifeTypeID = "5"
    
    // API Constants
    static let BASE_URL = "http://34.228.233.35/Babies_And_Beyond/api/"
    static let LoginApi = "login"
    static let RegisterApi = "register"
    static let ServiceRequestApi = "send_service_request"
    static let GetServiceApi = "services"
    static let UpdateProfileApi = "update_profile"
    static let ChangePasswordApi = "change_password"
    static let AllEventsApi = "all_events"
    static let AllWorkshopsApi = "all_workshops"
    static let AboutApi = "about"
    static let TermsApi = "terms-and-conditions/get"
    static let AllHistory = "user/history"
    static let IsComingEventApi = "comming_or_not_comming_to_event"
    static let WorkshopRequestApi = "send_workshop_request"
    static let LogoutApi = "logout"
    static let UserNotificationApi = "user_notifications"
    static let StaffRateCommentApi = "staff_rate_and_comment"
    static let AllMidwivesApi = "all_midwives"
    static let MidwifeRequestApi = "send_midwife_request"
    static let JoinGroupApi = "join_group"
    static let LeaveGroupApi = "leave_group"
    static let StaffScheduleApi = "staff_schedule_and_tasks"
    static let ActiveAccountApi = "active_account"
    static let ResendVerificationCodeApi = "resend_verification_code"
    static let CheckMidwifeApi = "midwife/check-midwife"
    static let ReserveMidwifeApi = "midwife/reserve"
    static let SocialLoginApi = "social_login"
    static let RateMidwifeApi = "midwife/rate-service"
    static let RateServiceApi = "user_service_rate"
    static let cancelMidwifeSercice = "midwife/cancel-reservation"
    static let cancelAllServicesExceptMidwife = "cancel_service"
    static let PostMessageApi = "contact-us/post-message"
    static let MidwifePaymentApi = "midwife/confirm-with-payment"
    static let StaffPaidServicesApi = "staff/paid-services"
    static let GetStaffMessages = "staff/messages"
    static let SendInboxMessage = "staff/send-inbox-message"
    static let GetServiceQuotationApi = "get_service_quotation"
    static let SendNurseServiceRequestApi = "send_service_request"

    
    //End of Api Constants
    
    // Alerts Constants:
    static let OKTItle  =  "Ok"
    static let errorAlertTitle = ""
    
    // 1- Internet connection messages in alerts
    static let ConnectionAlert  =  "Please check your internet connection!"
    static let FailureAlert  =  "Something went wrong, Please try again"
    static let FailureAlertiPhone5  =  "Something went wrong, \nPlease try again"
    
    
    // 2- Login & Register messages in alerts
    static let loginFailedMessage = "Incorrent email or password."
    static let EmptyEmail  =  "Please enter your email."
    static let InvalidEmail  =  "Your email is wrong."
    static let EmptyPassword  =  "Please enter your password."
    static let EmptyCurrentPassword  =  "Please enter your current password."
    static let EmptyNewPassword  =  "Please enter your new password."
    static let newPasswordAndConfirmation = "New Password doesn't match confirm new password."
    static let oldPasswordSimilarNewPassword = "Your new password is similar to your current password. Please try another one."
    static let PasswordCharactersLessThan6Digits = "Password must be 6 digits or characters or more."
    static let PasswordsNotMatched  =  "Password Confirmation doesn't match the password ."
    static let EmptyPhoneNumber  =  "Please enter your phone number."
    static let EmptyName  =  "Please enter your name."
    static let NoUser = "Email or password is wrong. check them again."
    
    // 3- Send Service Request messages in alerts
    static let ServiceNameAlert  =  "Please Choose a Service"
    static let StartDateTimeAlert  =  "Please Choose Start Date & Time"
    static let EndDateTimeAlert  =  "Please Choose End Date & Time"
    static let LocationFieldAlert  =  "Please Pickup Your Location"
    static let SuccessServiceRequestAlert  =  "You Request sent Successfully, wait the confirmation"
    
    // 4- Update Profile messages in alerts
    static let ProfileUpdateSuccessAlert  =  "Your Profile Updated Successfully"
    static let ChangePasswordSuccessAlert  =  "Your Password Updated Successfully"
    static let WrongCurrentPassword  =  "The entered current Password is wrong. "
    
    // 5- Comment/Rate
    static let EmptyComment  =  "Please enter your comment."
    static let CommentRateSuccessAlert  =  "Your Rate Submitted Successfully"
    static let RateValue  =  "Please enter rate."
    
    
    // 6- Create Group
    static let GroupImageRequired  =  "Please enter group image"
    static let GroupNameRequired  =  "Please enter group name"
    static let GroupDescriptionRequired  =  "Please enter group description"
    
    // 7- caching
    static let LoggedUserKey = "loggedUser"
    
    // 8- cloud messages keys
    static let Id = "id"
    static let Action = "action"
    static let type = "type"
    static let price = "service_price"
    static let staffName = "staff_name"
    
    // 9- payment keys
    static let selectedPaymentIcon = #imageLiteral(resourceName: "selectedPaymentIcon")
    static let notSelectedPAymentIcon = #imageLiteral(resourceName: "notSelectedPaymentIcon")
    static let cachOnDeliveryTitle = "Cash on delivery"
    static let onlinePaymentTitle = "Credit or Debit card"
    static let paymentsImages = [#imageLiteral(resourceName: "money-icon"),#imageLiteral(resourceName: "creditCard-icon")]
    
    // 10- EmailVerificationVC
    static let EmptyEmailVerificationAlert = "Enter verification code"
    static let enteredCodeAndSentCode = "Wrong verification code"
    static let CodeSentSuccessfully = "Verification code has been sent to your email address. Please check it now"
    
    // 11- NewMidwifeAppointmentViewController
    static let EmptyAppointmentDate = "Please enter the appointment date first."
    static let OverlappedAppointmentDate = "Your Appointment is overlapped with another Appointment"
    
    // 12- Contact Us Post Message
    static let EmptySubject = "Please enter Your Subject."
    static let EmptyMessage = "Please enter Your Message."
    static let PickerView = "Please choose your reason."
    static let title = ""
    static let sendButtonMessage = "Your Message sent Successfully."
    
    // 12- Get Service Quotation Message
    static let EmptyService = "Please choose your service."
    static let EmptyNumberOfDays = "Please enter Your Number Of Days Per Month."
    static let EmptyNumberOfHours = "Please enter Your Number Of Hours aDay."
    static let EmptyLocation = "Please enter Your Location."
    static let EmptySpecificRequest = "Please enter Your Specific Requests OR Concerns."
    static let submitButtonMessage = "Your Service sent Successfully."

    
    // 12- Nurse Request Message
    static let NurseEmptyService = "Please choose your service."
    static let NurseEmptyNumberOfChildren = "Please enter Your Number Of Your Children."
    static let NurseEmptyNumberOfHours = "Please enter Your Number Of Hours aDay."
    static let NurseEmptyLocation = "Please enter Your Location."
    static let NurseEmptyAdditionalInformation = "Please enter Your Additional Information."
    static let NurseEmptyBirthDate = "Please choose your Birth Date."
    static let NurseEmptyStartDate = "Please choose your Start Date."
    static let NurseSuccessNurseServiceRequestAlert  =  "You Request sent Successfully, wait the confirmation"


    //MidWife From / To Formate
    static let MidwifeDateFormatter = "yyyy-MM-dd HH:mm"

   //
    static let ShowInstructions = "ShowInstructions"

}
