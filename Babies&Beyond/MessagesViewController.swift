//
//  MessagesViewController.swift
//  Babies&Beyond
//
//  Created by NTAM Tech on 3/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController {

   
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier = "MessagesTableViewCell"
    
    // MARK:- Variables
    var AllStaffMessages = [MessageData]()
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Messages"
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchDragInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        let nib = UINib.init(nibName: "MessagesTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MessagesTableViewCell")
        
        observeChanges()
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    self?.showMessages()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.tabBarController?.view.makeToast("Check your Internet Connection!")
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AllStaffMessages.count > 0 {
            tableView.reloadData()
        }
        setUpView()
    }
    
    
    //func if internet connection lost suddenly and to update data if internet come sudden
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    func updateUI(){
        if AccountManager.shared().inboxMessage == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
                
            }
        }
    }
    
    
    fileprivate func setUpView() {
        if let messages = AccountManager.shared().inboxMessage{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            self.AllStaffMessages = messages
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            showMessages()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    private func refresh() {
        self.showMessages()
    }

    func showMessages(){
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        NurseService.GetStaffInboxMessagesRequest(Constants.GetStaffMessages, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    let result = UIScreen.main.bounds.size
                    if result.height <= 568{
                        self.connectionBtn?.setTitle(Constants.FailureAlertiPhone5, for: .normal)
                    }else{
                        self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
                    }
                    
                }else{
                    self.tableView.es.stopPullToRefresh()
                    self.tabBarController?.view.makeToast("Check your Internet Connection!")
                }
            }else{
                // response
                if let result = response as? MessageResponse, let status = result.status, status == true {
                    if let messagesArr = result.data {
                        self.AllStaffMessages = messagesArr
                        AccountManager.shared().inboxMessage = self.AllStaffMessages
                    }
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                }
            }
        })
    }
    
    @IBAction func addSendMessageAction(_ sender: Any) {
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "SendMessageViewController") as! SendMessageViewController
        destination.allMessages = self.AllStaffMessages
        self.navigationController?.pushViewController(destination, animated: true)
        
        
    }
}

extension MessagesViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllStaffMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesTableViewCell", for: indexPath)  as! MessagesTableViewCell
        let obj = AllStaffMessages[indexPath.row]
        cell.messageDate.text =  "\(Helper.fullDateConverter(dateString: obj.created_at!)!)"
        
        if let messages = obj.message{
            cell.theMessage.text = messages
        }
        if obj.from_id! == (AccountManager.shared().staffData?.id)!{
            cell.messageFromMe.text = "From Me"
        }else{
            cell.messageFromMe.text = "From Admin"
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 150
//    }
}

