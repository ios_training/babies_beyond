//
//  SignupViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/19/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import PKHUD

class SignupViewController: UIViewController {
    
    @IBOutlet private weak var scrollview:UIScrollView!
    @IBOutlet private weak var containerView:UIView!
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var signUpBtn:EMSpinnerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signUpBtn.cornerRadius = 5.0
        self.signUpBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scrollview.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        DispatchQueue.main.async {
            self.scrollview.contentSize = CGSize(width: 0, height: self.containerView.bounds.size.height)
        }
    }
    
    func registerRequest( emailTxt : String, passwordTxt:String ,confirmPasswordTxt: String, phoneTxt: String, nameTxt: String, notificationToken: String) {
        let parameters = ["email":emailTxt, "password":passwordTxt, "phone":phoneTxt, "name":nameTxt, "notification_token":notificationToken ]  as [String:Any]
        
        UserService.registerUser(Constants.RegisterApi, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            if let _ = error {
                self.signUpBtn.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let registerResponseResult = response as? LoginResponse {
                    if registerResponseResult.status!{
                        AccountManager.shared().userData = registerResponseResult.data?.user_data
                        AccountManager.shared().allServices = [AnyObject]()
                        AccountManager.shared().verificationCode = registerResponseResult.data?.verification_code
                        AccountManager.shared().userData?.isActivate = AccountActivated.DeactiveAccount.rawValue
                        self.navigateToEmailVerification()
                    }else{
                        self.signUpBtn.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: registerResponseResult.message!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    self.signUpBtn.animate(animation: .expand)
                }
            }
        })
    }
    
    
//    private func navigateToEmailVerification(){
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EmailVerificationVC") as! EmailVerificationVC
//        self.present(vc, animated: true, completion: nil)
//    }
    
    private func navigateToUserScreens(){
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func signUpBtnTapped(_ sender: UIButton) {
        
        guard let name = nameTxt.text, !name.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyName, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let phone = phoneTxt.text, !phone.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyPhoneNumber, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let email = emailTxt.text, !email.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard  Validations.isValidEmail(email: email) else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.InvalidEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let password = passwordTxt.text, !password.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyPassword, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        //        guard  Validation.isValidPassword(password: password) else {
        //            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.PasswordCharactersLessThan6Digits, controller: self, okBtnTitle: Constants.OKTItle)
        //            return
        //        }
        
        guard let confirmPassword = confirmPasswordTxt.text else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.PasswordsNotMatched, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let notificationToken = defaults.string(forKey: "notificationToken"), !notificationToken.isEmpty else {
            return
        }
        
        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.signUpBtn.animate(animation: .collapse)
            registerRequest(emailTxt: email, passwordTxt: password, confirmPasswordTxt: confirmPassword, phoneTxt: phone, nameTxt: name, notificationToken:notificationToken)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
}

