//
//  PaymentMethodTableViewCell.swift
//  Babies&Beyond
//
//  Created by M.I.Kamashany on 2/19/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    
    @IBOutlet weak var paymentImageView: UIImageView!
    @IBOutlet weak var paymentTitleLabel: UILabel!
    @IBOutlet weak var selectionStatusImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
